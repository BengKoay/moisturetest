'use strict';
const fs = require('fs-extra');
const piConfig = require('./piConfig.js').piConfig;
const appProperties = require('./helper/config/properties');

const ensureDirectory = async () => {
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.modbusConnectionPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.resetPowerMeterConnectionPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.resetSmartloggerConnectionPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.powerMeterReadingPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.processedNotificationPoolPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.publishingNotificationPoolPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.publishedNotificationPoolPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.smartLoggerReadingPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.inverterReadingDataPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.controlSolarOutputPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.processedDataPoolPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.publishingDataPoolPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.publishingErrorDataPoolPath
	);
	await fs.ensureDirSync(
		appProperties.localFileStorageLocation.ensureDirectories
			.publishedDataPoolPath
	);

	main();
};

const main = () => {
	// Core
	require('./core/appCore');
	// Ping
	require('./core/appCorePing');
	// // WatchDog
	// require('./core/appCoreWatchdog');
	// Data Combine
	require('./core/appCoreDataCombine');
	// Moisture code
	require('./core/appCoreMoisture');

	// if (!!piConfig.moscoClientConfig && !!piConfig.moscoClientConfig.server) {
	//   require('./core/appCoreMQTTPublisher');
	// }

	// if (!!piConfig.moscoServerConfig && !!piConfig.moscoServerConfig.port) {
	//   require('./core/appCoreMQTTServer');
	// }

	// if (
	//   piConfig.dataReadingType.includes(appProperties.meterTypeReading.powerMeter)
	// ) {
	//   require('./core/appCorePowerMeter');
	// }

	// if (
	//   piConfig.dataReadingType.includes(
	//     appProperties.meterTypeReading.smartLogger
	//   )
	// ) {
	//   require('./core/appCoreSmartLogger');
	// }

	if (
		[
			appProperties.publishMethod.mqtt,
			appProperties.publishMethod.rest,
		].includes(piConfig.publishDataMethod)
	) {
		require('./core/appCoreDataPublisher');
	}
};

ensureDirectory();
