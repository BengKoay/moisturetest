'use strict';
const cron = require('node-cron');
const exec = require('child_process').exec;
const logger = require('../helper/loggerHelper').getLogger('log');
const appProperties = require('../helper/config/properties');
const ReadingFileHelper = require('../helper/DataPublisherHelper/base');

const _readingFileHelper = new ReadingFileHelper();
const main = async () => {
  startJob();
};

const startJob = async () => {
  cron.schedule(
    '0 */10 * * * *',
    async () => {
      // Every 10 minute
      _readingFileHelper.moveUnpublishedReadingBackFileToReadingPath();
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone,
    }
  );

  cron.schedule(
    '10 30 23 * * *',
    async () => {
      // Every 23:30:10 Everyday
      // Reading
      _readingFileHelper.removePublishedHistoricalData(
        appProperties.localFileStorageLocation.ensureDirectories
          .publishedDataPoolPath
      );

      // Notification
      _readingFileHelper.removePublishedHistoricalData(
        appProperties.localFileStorageLocation.ensureDirectories
          .publishedNotificationPoolPath
      );
    },
    {
      scheduled: true,
      timezone: appProperties.timezone,
    }
  );

  cron.schedule(
    '1 0 0 * * *',
    async () => {
      // Every day  00:00:01 reboot Pi
      logger.info('Daily reboot 12am');
      exec('sudo reboot', (error, stdout, stderr) => {
        if (stdout) {
          logger.info('Successful Daily reboot 12am');
        }
      });
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone,
    }
  );
};

main();
