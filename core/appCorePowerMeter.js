'use strict';
const cron = require('node-cron');
const moment = require('moment-timezone');
const logger = require('../helper/loggerHelper').getLogger('log');
const energyMeterHelper = require('../helper/modbusHelper/energyMeterHelper');
const appProperties = require('../helper/config/properties');
const localCache = require('../helper/localCache');

moment.tz.setDefault(appProperties.momentTimeZone);

let _energyMeterHelper = null;
let task1 = false;

const main = async () => {
  _energyMeterHelper = new energyMeterHelper();
  await Promise.all([await _energyMeterHelper.sleep(1000)]);
  startJob();
};

const startJob = async () => {
  let task = cron.schedule(
    '*/5 * * * * *',
    async () => {
      // Every 5 Seconds
      if (
        !!_energyMeterHelper &&
        (await _energyMeterHelper.getConnectionStatus())
      ) {
        task1 = true;
        _energyMeterHelper.readData();
      } else {
        localCache.DPMConnectionFailureCount++;
        if (localCache.DPMConnectionFailureCount > 10) {
          if (moment().second() <= 40 && moment().second() >= 5) {
            logger.info(
              `Failed to reconnect Digital Power Meter for ${localCache.DPMConnectionFailureCount} time, Restarting Process!!!`
            );
            return process.exit(0);
          }
        }
        task1 = false;
        logger.info(
          `Reconnecting Digital Power Meter... ${localCache.DPMConnectionFailureCount}`
        );
        task.stop();
        main();
      }
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone,
    }
  );
};

main();
