'use strict';
const logger = require('../helper/loggerHelper').getLogger('log');
const cron = require('node-cron');
const appProperties = require('../helper/config/properties');
const dataCombineHelper = require('../helper/dataCombineHelper');

const _dataCombineHelper = new dataCombineHelper();

const main = async () => {
  startJob();
};

const startJob = async () => {
  cron.schedule(
    '3,8,13,18,23,28,33,38,43,48,53,58 * * * * *',
    // '58 * * * * *',
    async () => {
      // Every 5 seconds
      _dataCombineHelper.process();
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone
    }
  );
};

main();
