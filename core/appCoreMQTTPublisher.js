'use strict';
const cron = require('node-cron');
const fs = require('fs-extra');
const MQTTClientHelper = require('../helper/MQTTClientHelper');
const logger = require('../helper/loggerHelper').getLogger('log');
const appProperties = require('../helper/config/properties');
const piConfig = require('../piConfig.js').piConfig;

let _mqttClientHelper = null;
const main = async () => {
  _mqttClientHelper = new MQTTClientHelper();
  await Promise.all([await _mqttClientHelper.sleep(1000)]);
  logger.info('MQTT Publisher is Ready');
  startJob();
};

const startJob = async () => {
  let task = cron.schedule(
    '*/5 * * * * *',
    async () => {
      // Every 5 Seconds
      if (!_mqttClientHelper.getConnectionStatus()) {
        logger.info('Reconnecting MQTT Publisher...');
        task.stop();
        return main();
      }

      // POWER METER DATA
      if (
        piConfig.dataReadingType.includes(
          appProperties.meterTypeReading.powerMeter
        )
      ) {
        if (
          await fs.exists(
            `${appProperties.localFileStorageLocation.metersReading}`
          )
        ) {
          let allMeterReading = [];
          try {
            allMeterReading = JSON.parse(
              await fs.readFileSync(
                `${appProperties.localFileStorageLocation.metersReading}`,
                'utf8'
              )
            );
          } catch (e) {}

          if (allMeterReading.length > 0) {
            await _mqttClientHelper.publishData(
              `${appProperties.mosca.channel.powerMeter}/${
                piConfig.moscoClientConfig.username
              }`,
              allMeterReading
            );
          }
        }
      }
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone
    }
  );
};

main();
