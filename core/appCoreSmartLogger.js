'use strict';
const cron = require('node-cron');
const moment = require('moment-timezone');
const logger = require('../helper/loggerHelper').getLogger('log');
const smartloggerHelper = require('../helper/modbusHelper/smartLoggerHelper');
const appProperties = require('../helper/config/properties');
const localCache = require('../helper/localCache');

moment.tz.setDefault(appProperties.momentTimeZone);

let _smartloggerHelper = null;
let task1 = false;

const main = async () => {
  _smartloggerHelper = new smartloggerHelper();
  await Promise.all([await _smartloggerHelper.sleep(1000)]);
  startJob();
};

const startJob = async () => {
  let task = cron.schedule(
    '*/5 * * * * *',
    async () => {
      // Every 5 Seconds
      if (
        !!_smartloggerHelper &&
        (await _smartloggerHelper.getConnectionStatus())
      ) {
        task1 = true;
        _smartloggerHelper.readData();
      } else {
        localCache.smartLoggerConnectionFailureCount++;
        if (localCache.smartLoggerConnectionFailureCount > 10) {
          if (moment().second() <= 40 && moment().second() >= 5) {
            logger.info(
              `Failed to reconnect SmartLogger for ${localCache.smartLoggerConnectionFailureCount} time, Restarting Process!!!`
            );
            return process.exit(0);
          }
        }
        task1 = false;
        logger.info(
          `Reconnecting SmartLogger... ${localCache.smartLoggerConnectionFailureCount}`
        );
        task.stop();
        main();
      }
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone,
    }
  );

  cron.schedule(
    '*/5 * * * * *',
    // '5 * * * * *',
    async () => {
      // Every 1 minute on 5 seconds
      if (task1) {
        _smartloggerHelper.controlSolar();
      }
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone,
    }
  );
};

main();
