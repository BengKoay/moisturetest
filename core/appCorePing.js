'use strict';
const cron = require('node-cron');
const PingHelper = require('../helper/PingHelper');
const appProperties = require('../helper/config/properties');

const _pingHelper = new PingHelper();

const main = async () => {
  startJob();
};

const startJob = async () => {
  cron.schedule(
    '0 */5 * * * *',
    async () => {
      // Every 5 minute
      _pingHelper.process();
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone
    }
  );
};

main();
