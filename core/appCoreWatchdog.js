'use strict';
const cron = require('node-cron');
const watchdogHelper = require('../helper/watchdogHelper');
const appProperties = require('../helper/config/properties');

const _watchdogHelper = new watchdogHelper();

const main = async () => {
  startJob();
};

const startJob = async () => {
  cron.schedule(
    '*/5 * * * * *',
    async () => {
      // Every 5 seconds
      _watchdogHelper.process();
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone,
    }
  );
};

main();
