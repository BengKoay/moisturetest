'use strict';
const cron = require('node-cron');
const moment = require('moment-timezone');
const logger = require('../helper/loggerHelper').getLogger('log');
const moistureHelper = require('../helper/moisture');
const appProperties = require('../helper/config/properties');
const localCache = require('../helper/localCache');

const axios = require('axios');
// const deviceIP = 'http://192.168.0.107';

moment.tz.setDefault(appProperties.momentTimeZone);

let _moistureHelper = null;

const main = async () => {
	_moistureHelper = new moistureHelper();
	await Promise.all([await _moistureHelper.sleep(1000)]);
	startJob();
};

const startJob = async () =>
	cron.schedule('*/5 * * * * *', async () => {
		if (!!_moistureHelper) _moistureHelper.readData();
	});

main();
