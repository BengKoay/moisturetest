'use strict';

const mosca = require('mosca');
const fs = require('fs-extra');
const logger = require('../helper/loggerHelper').getLogger('log');
const appProperties = require('../helper/config/properties');
const piConfig = require('../piConfig.js').piConfig;

// Accepts the connection if the username and password are valid
const moscaAuthenticate = (client, username, password, callback) => {
  const authorized =
    username === piConfig.moscoServerConfig.username &&
    password.toString() === piConfig.moscoServerConfig.password;
  if (authorized) {
    client.user = username;
  }
  callback(null, authorized);
};

// In this case the client authorized as alice can publish to /users/alice taking
// the username from the topic and verifing it is the same of the authorized user
const moscaAuthorizePublish = (client, topic, payload, callback) => {
  callback(null, client.user == topic.split('/')[1]);
};

// In this case the client authorized as alice can subscribe to /users/alice taking
// the username from the topic and verifing it is the same of the authorized user
const moscaAuthorizeSubscribe = (client, topic, callback) => {
  callback(null, client.user == topic.split('/')[1]);
};

const main = async () => {
  try {
    const moscaServer = new mosca.Server({
      port: piConfig.moscoServerConfig.port
    });
    moscaServer.on('ready', () => {
      moscaServer.authenticate = moscaAuthenticate;
      moscaServer.authorizePublish = moscaAuthorizePublish;
      moscaServer.authorizeSubscribe = moscaAuthorizeSubscribe;
      logger.info('Mosca Service is Ready!');
    });

    moscaServer.on('published', (packet, client) => {
      // Power Meter Data
      if (packet.topic.includes(appProperties.mosca.channel.powerMeter)) {
        fs.writeFile(
          `${appProperties.localFileStorageLocation.metersReading}`,
          packet.payload.toString()
        );
      } else {
        logger.info('Packet', JSON.stringify(packet));
      }

      // // SmartLogger Data
      // if (packet.topic === appProperties.mosca.channel.smartLogger) {
      //   fs.writeFile(
      //     `${appProperties.localStoragePath}/${
      //       appProperties.fileName.awsSmartLoggerdata
      //     }`,
      //     payloadData
      //   );
      // }
    });
  } catch (err) {
    logger.error('Service Error', err);
  }
};

main();
