'use strict';
const cron = require('node-cron');
const DataPublisherHelper = require('../helper/DataPublisherHelper');
const appProperties = require('../helper/config/properties');
const piConfig = require('../piConfig.js').piConfig;
const localCache = require('../helper/localCache');

let client = null;
let task1 = false;

const main = async () => {
  switch (piConfig.publishDataMethod) {
    case appProperties.publishMethod.mqtt:
      client = new DataPublisherHelper.AWS_IOT_HELPER();
      break;
    case appProperties.publishMethod.rest:
      client = await new DataPublisherHelper.AWS_API_GATEWAY_HELPER();
      break;
    default:
  }

  await Promise.all([
    await new Promise((resolve) => setTimeout(resolve, 5000)),
  ]);

  startJob();
};

const startJob = async () => {
  if (client.isConnectionReady()) {
    await client.processReportDeviceOnline();
  }

  cron.schedule(
    '0 * * * * *',
    async () => {
      // Every 1 minute on 00 seconds
      if (client.isConnectionReady()) {
        await client.process();
      } else {
        await main();
      }
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone,
    }
  );

  // PubSubLatestReading
  cron.schedule(
    '*/5 * * * * *',
    async () => {
      // Every 5 seconds
      if (
        client.isConnectionReady() &&
        piConfig.publishDataMethod === appProperties.publishMethod.mqtt
      ) {
        await client.processPubSubLatestReading();
      }
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone,
    }
  );

  cron.schedule(
    // '0 0 * * * *', // Every 1 hour on 00 minute and 00 seconds
    '0 */10 * * * *', // Every 10 minute and 00 seconds
    async () => {
      if (client.isConnectionReady()) {
        task1 = true;
        await client.processReportDeviceOnline();
      } else {
        if (moment().second() <= 40 && moment().second() >= 5) {
          logger.info(`Failed to reconnect AWS Server, Restarting Process!!!`);
          return process.exit(0);
        }
        task1 = false;
        logger.info(`Waiting to Reconnect AWS Server........................`);
        main();
      }
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone,
    }
  );

  cron.schedule(
    '0 */10 9-19 * * *',
    async () => {
      // Every 10 minute within 9AM to 7PM
      if (
        client.isConnectionReady() &&
        piConfig.publishDataMethod === appProperties.publishMethod.rest
      ) {
        await client.getRemoteCommand();
      }
    },
    {
      scheduled: true,
      timezone: appProperties.momentTimeZone,
    }
  );
};

main();
