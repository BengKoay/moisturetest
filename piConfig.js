module.exports.piConfig = {
	// typeOfService: 1,
	// meterConfig: {
	// 	baudRate: null,
	// 	parity: 'none',
	// 	usbConnection: false,
	// 	meters: [],
	// },
	// smartloggerConfig: [],
	// panelEfficiency: 16,
	// dataReadingType: [],
	publishDataMethod: 'rest',
	// maxPVoutput: 673.285,
	// solarCappingPercentage: 0.95,
	// companyId: '079fc95f-5c0f-492c-8699-369e446f7cd7',
	// locationId: '627b05f6-b678-45c8-ae8a-9e246c62e4e8',
	// lotId: '846e9eef-b8eb-4075-8f00-03d313c2512d',
	// deviceId: 'b0985f5d-a846-4bf5-bd6d-35a344c5576b',
	// awsIoTCredentials: {
	// 	clientCert:
	// 		'-----BEGIN CERTIFICATE-----\nMIIDWjCCAkKgAwIBAgIVALSO41KD9ZDvLmZBQDQ7AlFbDgcYMA0GCSqGSIb3DQEB\nCwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t\nIEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0yMTA0MjgwNjIw\nMTNaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh\ndGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCxhCvCBV6Q3k/ps8Yj\nE614tH15yTkbxMzpLI6KERQAp2RiygGNTWtsQsgDTURhC3fwydSU/N7ElXd0JFuk\ndPojDD5aPjfwchT8SmiOMTHo9Fz+JoVBWQtcQARFL5FcZOEoXONyJgLjHF+z4vBG\nlgl1Dapv7Ww7U3Sq+dEHPBAj5/NpNCHKcGy5I6khs+ogdoduH9HyFp84EtS1/pFh\nmj9aFmGHLgjbjhdHb4JEhXeQ0CLKz7+VdTz0LessEZq/bPJFBYMT2Z4Fbu2iy9op\nRuulfNE3B+VOtd2fxFrG9ERJDJ+A9LfJwSEreZTMHlDgL/vWqL0EGEvYp1ntVK0A\ngf2VAgMBAAGjYDBeMB8GA1UdIwQYMBaAFPflbHed4eU9aX4dk/HVX4hBqY3EMB0G\nA1UdDgQWBBQZB70+Qrwj5pS/xRndRW0w2+tcXjAMBgNVHRMBAf8EAjAAMA4GA1Ud\nDwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAHyuY61PbtSdRkP5gPblBY3nD\n+iuj5mAdyKQyCId3xAfywRU/G3MPZ9Yozi06HHNoVkV/7utPbRmtw5DOQp6Cwftr\nn/xX6aVYV5FGztlABerVtk9S/t6bD8dfsO0rD5IGwwnHiQlt7Z6Yj+8B5zZl++fJ\nf27wu8QDiXt2sRTQwMotRKPBBpbK77wC85uJx11cU0WVZnImS8xjts62jrB3KEV7\ndCtZHhdJ75UXQqCIthTxhvxezptnElJ8FFkxw87qXQec8QvOlEiGKgmPKAH2MsNg\nSQZ7cDugPn1kVAW4f5RVZXuIJOTX4XREQSL6AkmNBrsMPxiE4wlKT4TCSKGxVA==\n-----END CERTIFICATE-----\n',
	// 	privateKey:
	// 		'-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAsYQrwgVekN5P6bPGIxOteLR9eck5G8TM6SyOihEUAKdkYsoB\njU1rbELIA01EYQt38MnUlPzexJV3dCRbpHT6Iww+Wj438HIU/EpojjEx6PRc/iaF\nQVkLXEAERS+RXGThKFzjciYC4xxfs+LwRpYJdQ2qb+1sO1N0qvnRBzwQI+fzaTQh\nynBsuSOpIbPqIHaHbh/R8hafOBLUtf6RYZo/WhZhhy4I244XR2+CRIV3kNAiys+/\nlXU89C3rLBGav2zyRQWDE9meBW7tosvaKUbrpXzRNwflTrXdn8RaxvRESQyfgPS3\nycEhK3mUzB5Q4C/71qi9BBhL2KdZ7VStAIH9lQIDAQABAoIBAQCIuK+rI8+N7GvI\nJ7o3UfSNvxXVMvJVPOJFxReTp0uspUBgBg3j8vkFzPmX8W70/4xFXG3qwkghSK1U\nOpyKsb3mOIvdnYYgHv2iwvx8Z891CKCG7sw7ZIWna3begqCGdPwlxkU3qacXnMbp\nGaTN+MkX6oqW4lo15zU4amhgFhaoT7fxaI8LnkPzURyKU6HJi1SlTqc2eMFuHk2t\n3kYOJvTItXTIEycPoVQJG2zFF66nb2h8ktfDX5kMou7XIanmsVpZ2Nq6/bEoTZHp\n1NYX5tDukrmEXvR5fuMyPtJnijv3p0ehT8EZ1wzp5PV4AfgCFZztlaJ5/7t0OPxE\nwADNVQmRAoGBAOrkuZkbSpeJDCSjUaYNLHbmqvZjwm2kjPcx+LUlAKdOD7pawH9x\nBck9nAiSfWmN0xaU/t0RElRYHEwBudXtLhNmIUweS1eb39H/s5LKH/DHcS0Ao8Hn\npSMcV37e4nzI/zuKRVq07unTXBIJFRxPTx0pD8WFUZt+Y2zEhpA82hPfAoGBAMF3\nmAyMRJwxqzoaxkJitOy4NZm9WqAGEF1iCumAjkyu1kgUy0Q5S1L/DnXHBq0v2kGQ\nlTL74Rc/esqhM3hrSyOLuCYPn0uDtxo4VNXiGXHC/ApXVatiGSoV3avGKrbgLOEc\nPTeqDPP3ertwP4NhUVhiE6lU7vNxBmTSpTn4eT0LAoGAcO3OHUH1tqz43ZR3aLHk\nsrVrdFG55CS8cQvhWpWTntv0y89dxOd9tKaeIbDsApdg6PZxIa9XdJ133ULnKWaC\nRGlRkNrvtgsGrFuW5fevK393df9cu5i22A6EMkbN9NhdfS6Y8WcThe9L0I/1SksG\n6ioXnwrdlThKbLx/RVE8N1kCgYBsjhN17vrMfZ4rMzzNoR8NuGrzAemo4t4p18px\nrN2rr2qy9V/8vv1B67WMZombgb9AjMT635Q0fD0FF6COz/1QS6SfTqK+N4D1KGiJ\nvp0yVQshLlH2oUunbq98I+xxwqXMBhFfe35bIiDurZQlvuRq6FVJ7UtKs7+3sO5e\n8cntcwKBgQDPnJCgVaBguJh8SVM2vapJIj4lZmAVe3kmsNps+cw+JtVnj0qdnq//\nobecYAAIGSLin5dTjHfGvgopWpHTxr568itEJAPQyVJcMIZdlz59WIkQvCl+WOWm\nDdXcabaJDIzZDuIvckNLoxeMaN0K4AfHisHxMM27MHo2EZ4qzVS1/Q==\n-----END RSA PRIVATE KEY-----\n',
	// },
};
