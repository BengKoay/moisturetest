'use strict';
const ModbusRTU = require('modbus-serial');
const fs = require('fs-extra');
const moment = require('moment-timezone');
const bignum = require('bignum');

const Base = require('../base');
const piConfig = require('../../../piConfig.js').piConfig;
const logger = require('../../loggerHelper').getLogger('log');
const appProperties = require('../../config/properties');
let localCache = require('../../localCache');
moment.tz.setDefault(appProperties.momentTimeZone);

class EnergyMeterHelper extends Base {
  constructor() {
    super();
    const modbusHelper = new ModbusRTU();
    if (!!piConfig.meterConfig && piConfig.meterConfig.meters.length > 0) {
      const { usbConnection, baudRate, parity } = piConfig.meterConfig;
      let connectionType = '/dev/ttyAMA0';
      if (usbConnection === true) {
        connectionType = '/dev/ttyUSB0';
      }
      if (!!piConfig.meterConfig.ip)
        modbusHelper.connectTCP(
          piConfig.meterConfig.ip,
          { port: 502 },
          (error, success) => {
            if (error) {
              logger.info(
                `Power meter with baud rate of ${baudRate} is initialization unsuccessful`
              );
              this.meterConnectedStatus = false;
              return;
            }
            this.meterConnectedStatus = true;
            logger.info(
              `Power meter with baud rate of ${baudRate} is initialization successful`
            );
          }
        );
      else
        modbusHelper.connectRTUBuffered(
          connectionType,
          { baudRate, parity: parity },
          (error, success) => {
            if (error) {
              logger.info(
                `Power meter with baud rate of ${baudRate} is initialization unsuccessful`
              );
              this.meterConnectedStatus = false;
              return;
            }
            this.meterConnectedStatus = true;
            logger.info(
              `Power meter with baud rate of ${baudRate} is initialization successful`
            );
          }
        );
    }
    modbusHelper.setTimeout(500);
    this.modbusHelper = modbusHelper;
    return this;
  }

  async getConnectionStatus() {
    let connectionStatus = this.meterConnectedStatus;
    const pendingDataFileList = await fs.readdirSync(
      appProperties.localFileStorageLocation.ensureDirectories
        .resetPowerMeterConnectionPath
    );
    for (const pendingDataFile of pendingDataFileList) {
      if (
        !fs
          .statSync(
            `${appProperties.localFileStorageLocation.ensureDirectories.resetPowerMeterConnectionPath}/${pendingDataFile}`
          )
          .isDirectory()
      ) {
        await fs.unlinkSync(
          `${appProperties.localFileStorageLocation.ensureDirectories.resetPowerMeterConnectionPath}/${pendingDataFile}`
        );
        connectionStatus = false;
      }
    }

    return connectionStatus;
  }

  async readData() {
    if (
      !!piConfig.meterConfig &&
      piConfig.meterConfig.meters.length > 0 &&
      !!this.modbusHelper
    ) {
      let meterReadingList = [];
      for (const meter of piConfig.meterConfig.meters) {
        let meterReading = await _getEachMeterInfo(this.modbusHelper, meter);
        meterReadingList.push(meterReading);

        await this.sleep(500);
      }
      if (!!meterReadingList) {
        localCache.energyMeterReading = meterReadingList;
      }
    }
  }
}

const _getEachMeterInfo = async (modbusHelper, meter) => {
  const { brand: powerMeterBrand, id: meterId } = meter;

  if (!localCache.energyMeterReading) {
    localCache.energyMeterReading = [
      {
        activeEnergy: 0,
        negativeActiveEnergy: 0,
        apparentEnergy: 0,
        negativeReactiveEnergy: 0,
        reactiveEnergy: 0,
        activePower: 0,
        apparentPower: 0,
        reactivePower: 0,
        powerFactor: 0,
        frequency: 0,
        current1: 0,
        current2: 0,
        current3: 0,
        voltage12: 0,
        voltage23: 0,
        voltage31: 0,
        voltage1: 0,
        voltage2: 0,
        voltage3: 0,
        power1: 0,
        power2: 0,
        power3: 0,
        reactivePower1: 0,
        reactivePower2: 0,
        reactivePower3: 0,
        powerMeterBrand,
        ref: meterId,
        lastUpdate: appProperties.powerMeterErrorTimestamp,
      },
    ];
  }

  let powerMeterValue = await _getMeterInfoByMeterBrand(modbusHelper, meter);
  if (powerMeterValue) {
    let {
      activePower,
      negativeActiveEnergy,
      activeEnergy,
      apparentEnergy,
      negativeReactiveEnergy,
      reactiveEnergy,
      apparentPower,
      reactivePower,
      powerFactor,
      frequency,
      current1,
      current2,
      current3,
      voltage12,
      voltage23,
      voltage31,
      voltage1,
      voltage2,
      voltage3,
      power1,
      power2,
      power3,
      reactivePower1,
      reactivePower2,
      reactivePower3,
    } = powerMeterValue;

    // Power and Energy
    if (activePower > appProperties.maxTnbPower) {
      activePower = 0;
    }

    // let allMeterReading = [];
    // allMeterReading = localCache.energyMeterReading;

    // allMeterReading = allMeterReading.filter((obj) => {
    //   return obj.ref !== meterId;
    // });

    let currentReading = {
      utilitiesActivePower: activePower,
      activePower,
      negativeActiveEnergy,
      activeEnergy,
      apparentEnergy,
      negativeReactiveEnergy,
      reactiveEnergy,
      apparentPower,
      reactivePower,
      powerFactor,
      frequency,
      current1,
      current2,
      current3,
      voltage12,
      voltage23,
      voltage31,
      voltage1,
      voltage2,
      voltage3,
      power1,
      power2,
      power3,
      reactivePower1,
      reactivePower2,
      reactivePower3,
      powerMeterBrand,
      ref: meterId,
      lastUpdate: moment().unix(),
    };
    // Data for AWS

    return currentReading;
  } else {
    logger.error(`Unable to get power meter value on meter id of ${meterId}`);
    localCache.errorDPM = e;
    console.log('localCache.errorDPM', localCache.errorDPM);
  }
};

const _getMeterInfoByMeterBrand = async (modbusHelper, meter) => {
  const { brand, id: meterId } = meter;

  // if (!!localCache.energyMeterReading) {
  // }
  let previousReading = localCache.energyMeterReading;

  // let previousReading = await fs.readJsonSync(
  //   `${appProperties.localFileStorageLocation.metersReading}`,
  //   'utf8'
  // );

  let energyParameters = null;
  let reactiveEnergyParameters = null;
  let apparentEnergyParameters = null;
  let negativeActiveEnergyParameters = null;
  let negativeReactiveEnergyParameters = null;
  let result = {
    activeEnergy: previousReading[0].activeEnergy,
    negativeActiveEnergy: previousReading[0].negativeActiveEnergy,
    // activeEnergy: 0,
    activePower: 0,
    apparentEnergy: previousReading[0].apparentEnergy,
    negativeReactiveEnergy: previousReading[0].negativeReactiveEnergy,
    reactiveEnergy: previousReading[0].reactiveEnergy,
    apparentPower: 0,
    reactivePower: 0,
    powerFactor: 0,
    frequency: 0,
    current1: 0,
    current2: 0,
    current3: 0,
    voltage12: 0,
    voltage23: 0,
    voltage31: 0,
    voltage1: 0,
    voltage2: 0,
    voltage3: 0,
    power1: 0,
    power2: 0,
    power3: 0,
    reactivePower1: 0,
    reactivePower2: 0,
    reactivePower3: 0,
  };

  try {
    await modbusHelper.setID(meterId);
    if (brand === 1) {
      // Mikro DPM 380
      energyParameters = await modbusHelper.readHoldingRegisters(4000, 80);
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.negativeActiveEnergy = energyParameters.buffer.readUInt32BE(0);
      result.activeEnergy = energyParameters.buffer.readUInt32BE(4);
      result.apparentEnergy = energyParameters.buffer.readUInt32BE(12);
      result.negativeReactiveEnergy = energyParameters.buffer.readUInt32BE(16);
      result.reactiveEnergy = energyParameters.buffer.readUInt32BE(20);
      result.activePower = energyParameters.buffer.readInt32BE(24) / 1000;
      result.apparentPower = energyParameters.buffer.readInt32BE(28) / 1000;
      result.reactivePower = energyParameters.buffer.readInt32BE(32) / 1000;
      result.powerFactor = energyParameters.buffer.readUInt16BE(36) / 1000;
      result.frequency = energyParameters.buffer.readUInt16BE(38) / 100;
      result.current1 = energyParameters.buffer.readUInt32BE(40) / 1000;
      result.current2 = energyParameters.buffer.readUInt32BE(44) / 1000;
      result.current3 = energyParameters.buffer.readUInt32BE(48) / 1000;
      result.voltage12 = energyParameters.buffer.readUInt32BE(56) / 10;
      result.voltage23 = energyParameters.buffer.readUInt32BE(60) / 10;
      result.voltage31 = energyParameters.buffer.readUInt32BE(64) / 10;
      result.voltage1 = energyParameters.buffer.readUInt32BE(68) / 10;
      result.voltage2 = energyParameters.buffer.readUInt32BE(72) / 10;
      result.voltage3 = energyParameters.buffer.readUInt32BE(76) / 10;
      result.power1 = energyParameters.buffer.readInt32BE(80);
      result.power2 = energyParameters.buffer.readInt32BE(84);
      result.power3 = energyParameters.buffer.readInt32BE(88);
      result.reactivePower1 = energyParameters.buffer.readInt32BE(104);
      result.reactivePower2 = energyParameters.buffer.readInt32BE(108);
      result.reactivePower3 = energyParameters.buffer.readInt32BE(112);
    } else if (brand === 2) {
      // Schneider PM710
      energyParameters = await modbusHelper.readHoldingRegisters(3999, 9);
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.activeEnergy = energyParameters.buffer.readInt32BE(0) / 10;
      result.apparentEnergy = energyParameters.buffer.readInt32BE(4) / 10;
      result.activePower = energyParameters.data[6];
      result.reactivePower = energyParameters.data[8];
      result.powerFactor = energyParameters.data[9];
      result.frequency = energyParameters.data[13];
      result.current1 = energyParameters.data[20];
      result.current2 = energyParameters.data[21];
      result.current3 = energyParameters.data[22];
      result.voltage12 = energyParameters.data[30];
      result.voltage23 = energyParameters.data[31];
      result.voltage31 = energyParameters.data[32];
      result.voltage1 = energyParameters.data[33];
      result.voltage2 = energyParameters.data[34];
      result.voltage3 = energyParameters.data[35];
      result.power1 = energyParameters.data[36];
      result.power2 = energyParameters.data[37];
      result.power3 = energyParameters.data[38];
      result.reactivePower1 = energyParameters.data[42];
      result.reactivePower2 = energyParameters.data[43];
      result.reactivePower3 = energyParameters.data[44];
    } else if (brand === 3) {
      // Schneider PM5350
      energyParameters = await modbusHelper.readHoldingRegisters(3203, 4);
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.activeEnergy = bignum.toNumber(
        await bignum.fromBuffer(energyParameters.buffer)
      );

      negativeActiveEnergyParameters = await modbusHelper.readHoldingRegisters(
        3207,
        4
      );
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.negativeActiveEnergy = bignum.toNumber(
        await bignum.fromBuffer(negativeActiveEnergyParameters.buffer)
      );

      apparentEnergyParameters = await modbusHelper.readHoldingRegisters(
        3235,
        4
      );
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.apparentEnergy = bignum.toNumber(
        await bignum.fromBuffer(apparentEnergyParameters.buffer)
      );

      reactiveEnergyParameters = await modbusHelper.readHoldingRegisters(
        3219,
        4
      );
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.reactiveEnergy = bignum.toNumber(
        await bignum.fromBuffer(reactiveEnergyParameters.buffer)
      );

      negativeReactiveEnergyParameters =
        await modbusHelper.readHoldingRegisters(3223, 4);
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.negativeReactiveEnergy = bignum.toNumber(
        await bignum.fromBuffer(negativeReactiveEnergyParameters.buffer)
      );

      result.activePower = (
        await modbusHelper.readHoldingRegisters(3059, 4)
      ).buffer.readFloatBE(0);
      result.apparentPower = (
        await modbusHelper.readHoldingRegisters(3075, 4)
      ).buffer.readFloatBE(0);

      result.reactivePower = (
        await modbusHelper.readHoldingRegisters(3067, 4)
      ).buffer.readFloatBE(0);
      result.frequency = (
        await modbusHelper.readHoldingRegisters(3109, 4)
      ).buffer.readFloatBE(0);
      result.powerFactor = (
        await modbusHelper.readHoldingRegisters(3083, 4)
      ).buffer.readFloatBE(0);
      result.frequency = (
        await modbusHelper.readHoldingRegisters(3109, 4)
      ).buffer.readFloatBE(0);
      result.current1 = (
        await modbusHelper.readHoldingRegisters(2999, 4)
      ).buffer.readFloatBE(0);
      result.current2 = (
        await modbusHelper.readHoldingRegisters(3001, 4)
      ).buffer.readFloatBE(0);
      result.current3 = (
        await modbusHelper.readHoldingRegisters(3003, 4)
      ).buffer.readFloatBE(0);
      result.voltage12 = (
        await modbusHelper.readHoldingRegisters(3019, 4)
      ).buffer.readFloatBE(0);
      result.voltage23 = (
        await modbusHelper.readHoldingRegisters(3021, 4)
      ).buffer.readFloatBE(0);
      result.voltage31 = (
        await modbusHelper.readHoldingRegisters(3023, 4)
      ).buffer.readFloatBE(0);
      result.voltage1 = (
        await modbusHelper.readHoldingRegisters(3027, 4)
      ).buffer.readFloatBE(0);
      result.voltage2 = (
        await modbusHelper.readHoldingRegisters(3029, 4)
      ).buffer.readFloatBE(0);
      result.voltage3 = (
        await modbusHelper.readHoldingRegisters(3031, 4)
      ).buffer.readFloatBE(0);
      result.power1 = (
        await modbusHelper.readHoldingRegisters(3053, 4)
      ).buffer.readFloatBE(0);
      result.power2 = (
        await modbusHelper.readHoldingRegisters(3055, 4)
      ).buffer.readFloatBE(0);
      result.power3 = (
        await modbusHelper.readHoldingRegisters(3057, 4)
      ).buffer.readFloatBE(0);
      result.reactivePower1 = (
        await modbusHelper.readHoldingRegisters(3061, 4)
      ).buffer.readFloatBE(0);
      result.reactivePower2 = (
        await modbusHelper.readHoldingRegisters(3063, 4)
      ).buffer.readFloatBE(0);
      result.reactivePower3 = (
        await modbusHelper.readHoldingRegisters(3065, 4)
      ).buffer.readFloatBE(0);
    } else if (brand === 4) {
      // Schneider Power Logic PM5500
      result.activePower = (
        await modbusHelper.readHoldingRegisters(3059, 2)
      ).buffer.readFloatBE(0);
      energyParameters = await modbusHelper.readHoldingRegisters(3203, 4);
      result.activeEnergy =
        bignum.toNumber(await bignum.fromBuffer(energyParameters.buffer)) /
        1000;
      result.powerFactor = 0;
      // let voltageParameter = await modbusHelper.readHoldingRegisters(3019, 4);
      // voltageTNB = voltageParameter.buffer.readFloatBE(0);
    } else if (brand === 5) {
      // Socomec Multis L50
      energyParameters = await modbusHelper.readHoldingRegisters(768, 98);

      await new Promise((resolve) => setTimeout(resolve, 10));
      negativeActiveEnergyParameters = await modbusHelper.readHoldingRegisters(
        50780,
        10
      );

      await new Promise((resolve) => setTimeout(resolve, 10));
      result.negativeActiveEnergy =
        negativeActiveEnergyParameters.buffer.readUInt32BE(12); // 50786
      result.activeEnergy = energyParameters.buffer.readUInt32BE(176); // 856
      result.apparentEnergy =
        negativeActiveEnergyParameters.buffer.readUInt32BE(8); // 50784
      result.negativeReactiveEnergy =
        negativeActiveEnergyParameters.buffer.readUInt32BE(16); // 50788
      result.reactiveEnergy = energyParameters.buffer.readUInt32BE(180); // 858
      result.activePower = energyParameters.buffer.readInt32BE(44) / 100; // 790
      result.reactivePower = energyParameters.buffer.readInt32BE(48) / 100; // 792
      result.apparentPower = energyParameters.buffer.readInt32BE(52) / 100; // 794
      result.powerFactor = energyParameters.buffer.readInt32BE(56) / 1000; // 796
      result.frequency = energyParameters.buffer.readUInt32BE(40) / 100; // 788
      result.current1 = energyParameters.buffer.readUInt32BE(0) / 1000; // 768
      result.current2 = energyParameters.buffer.readUInt32BE(4) / 1000; // 770
      result.current3 = energyParameters.buffer.readUInt32BE(8) / 1000; // 772
      result.voltage12 = energyParameters.buffer.readUInt32BE(16) / 100; // 776
      result.voltage23 = energyParameters.buffer.readUInt32BE(20) / 100; // 778
      result.voltage31 = energyParameters.buffer.readUInt32BE(24) / 100; // 780
      result.voltage1 = energyParameters.buffer.readUInt32BE(28) / 100; // 782
      result.voltage2 = energyParameters.buffer.readUInt32BE(32) / 100; // 784
      result.voltage3 = energyParameters.buffer.readUInt32BE(36) / 100; // 786
      result.power1 = energyParameters.buffer.readInt32BE(60) / 100; // 798
      result.power2 = energyParameters.buffer.readInt32BE(64) / 100; // 800
      result.power3 = energyParameters.buffer.readInt32BE(68) / 100; // 802
      result.reactivePower1 = energyParameters.buffer.readInt32BE(72) / 100; // 804
      result.reactivePower2 = energyParameters.buffer.readInt32BE(76) / 100; // 806
      result.reactivePower3 = energyParameters.buffer.readInt32BE(80) / 100; // 808

      // energyParameters = await modbusHelper.readHoldingRegisters(768, 90);
      // result.activeEnergy = energyParameters.buffer.readUInt32BE(176);
      // result.activePower = energyParameters.buffer.readUInt32BE(44) / 100;
    } else if (brand === 6) {
      // Selec MFM384, this DPM has no negative power capability, reactive energy (no Power Factor). Please don't use this in future if NEM.

      energyParameters = await modbusHelper.readInputRegisters('01', 60);
      result.activeEnergy = energyParameters.buffer.readFloatBE(116);
      result.activePower = energyParameters.buffer.readFloatBE(84);
      result.reactivePower = energyParameters.buffer.readFloatBE(88);
      result.powerFactor = energyParameters.buffer.readFloatBE(108);
      result.frequency = energyParameters.buffer.readFloatBE(112);
      result.current1 = energyParameters.buffer.readFloatBE(32);
      result.current2 = energyParameters.buffer.readFloatBE(36);
      result.current3 = energyParameters.buffer.readFloatBE(40);
      result.voltage12 = energyParameters.buffer.readFloatBE(16);
      result.voltage23 = energyParameters.buffer.readFloatBE(20);
      result.voltage31 = energyParameters.buffer.readFloatBE(24);
      result.voltage1 = energyParameters.buffer.readFloatBE(0);
      result.voltage2 = energyParameters.buffer.readFloatBE(4);
      result.voltage3 = energyParameters.buffer.readFloatBE(8);
      result.power1 = energyParameters.buffer.readFloatBE(48);
      result.power2 = energyParameters.buffer.readFloatBE(52);
      result.power3 = energyParameters.buffer.readFloatBE(56);
      result.reactivePower1 = energyParameters.buffer.readFloatBE(60);
      result.reactivePower2 = energyParameters.buffer.readFloatBE(64);
      result.reactivePower3 = energyParameters.buffer.readFloatBE(68);
    } else if (brand === 7) {
      // Customize for IKEA only
      energyParameters = await modbusHelper.readHoldingRegisters(1, 46);

      let activeEnergy1TNB1 = energyParameters.buffer.readFloatBE(20);
      let activeEnergy2TNB1 = energyParameters.buffer.readFloatBE(22);
      let activeEnergy3TNB1 = energyParameters.buffer.readFloatBE(24);

      let activeEnergy1TNB2 = energyParameters.buffer.readFloatBE(64);
      let activeEnergy2TNB2 = energyParameters.buffer.readFloatBE(66);
      let activeEnergy3TNB2 = energyParameters.buffer.readFloatBE(68);

      let intakeTNB1 =
        activeEnergy1TNB1 + activeEnergy2TNB1 + activeEnergy3TNB1;
      let intakeTNB2 =
        activeEnergy1TNB2 + activeEnergy2TNB2 + activeEnergy3TNB2;
      result.activePower = intakeTNB1 + intakeTNB2;
      result.powerFactor = 0;
    } else if (brand === 8) {
      // Schneider PM500
      result.activeEnergy = (
        await modbusHelper.readHoldingRegisters(856, 2)
      ).buffer.readInt32BE(0);
      result.activePower =
        (await modbusHelper.readHoldingRegisters(790, 2)).buffer.readInt32BE(
          0
        ) / 100;
    } else if (brand === 9) {
      result.current1 = (
        await modbusHelper.readHoldingRegisters(2999, 2)
      ).buffer.readFloatBE();
      result.current2 = (
        await modbusHelper.readHoldingRegisters(3001, 2)
      ).buffer.readFloatBE();
      result.current3 = (
        await modbusHelper.readHoldingRegisters(3003, 2)
      ).buffer.readFloatBE();
      result.voltage1 = (
        await modbusHelper.readHoldingRegisters(3027, 2)
      ).buffer.readFloatBE();
      result.voltage2 = (
        await modbusHelper.readHoldingRegisters(3029, 2)
      ).buffer.readFloatBE();
      result.voltage3 = (
        await modbusHelper.readHoldingRegisters(3031, 2)
      ).buffer.readFloatBE();
      result.powerFactor = (
        await modbusHelper.readHoldingRegisters(3083, 2)
      ).buffer.readFloatBE();
      let currentCalculate = (
        await modbusHelper.readHoldingRegisters(3009, 2)
      ).buffer.readFloatBE();
      let voltageCalculate = (
        await modbusHelper.readHoldingRegisters(3035, 2)
      ).buffer.readFloatBE();

      let intakeTNBCalculate =
        (currentCalculate * 3 * result.powerFactor * voltageCalculate) / 1000;
      result.activePower = intakeTNBCalculate;
    } else if (brand === 10) {
      // SMA Energy Meter
      energyParameters = await modbusHelper.readHoldingRegisters(30865, 2);
      result.activePower = energyParameters.buffer.readInt32BE(0) / 1000;
      result.activeEnergy =
        (await modbusHelper.readHoldingRegisters(30581, 2)).buffer.readUInt32BE(
          0
        ) / 1000;
      result.powerFactor = 1; //energyParameters.buffer.readUInt16BE(36) / 1000;
    } else if (brand == 11) {
      energyParameters = await modbusHelper.readHoldingRegisters(19026, 40);
      result.activeEnergy = energyParameters.buffer.readFloatBE(68) / 1000;
      result.activePower = energyParameters.buffer.readFloatBE(0) / 1000;
      result.powerFactor = (
        await modbusHelper.readHoldingRegisters(1373, 2)
      ).buffer
        .readFloatBE(0)
        .toFixed(3);
    } else if (brand === 12) {
      // Mikro DPM 680
      energyParameters = await modbusHelper.readHoldingRegisters(4000, 80);
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.negativeActiveEnergy =
        energyParameters.buffer.readUInt32BE(0) / 1000;
      result.activeEnergy = energyParameters.buffer.readUInt32BE(4) / 1000;
      result.apparentEnergy = energyParameters.buffer.readUInt32BE(12);
      result.negativeReactiveEnergy = energyParameters.buffer.readUInt32BE(16);
      result.reactiveEnergy = energyParameters.buffer.readUInt32BE(20);
      result.activePower = energyParameters.buffer.readInt32BE(24) / 1000;
      result.apparentPower = energyParameters.buffer.readInt32BE(28) / 1000;
      result.reactivePower = energyParameters.buffer.readInt32BE(32) / 1000;
      var PowerFactorDPM = energyParameters.buffer.readUInt16BE(36) / 1000;

      const invalidNumberRangeA = (key, value) => {
        if (['PowerFactorDPM'].includes(key)) {
          if (value > 1) {
            return 1;
          }
        }

        return value;
      };

      result.powerFactor = invalidNumberRangeA(
        'PowerFactorDPM',
        PowerFactorDPM
      );

      result.frequency = energyParameters.buffer.readUInt16BE(38) / 100;
      result.current1 = energyParameters.buffer.readUInt32BE(40) / 1000;
      result.current2 = energyParameters.buffer.readUInt32BE(44) / 1000;
      result.current3 = energyParameters.buffer.readUInt32BE(48) / 1000;
      result.voltage12 = energyParameters.buffer.readUInt32BE(56) / 10;
      result.voltage23 = energyParameters.buffer.readUInt32BE(60) / 10;
      result.voltage31 = energyParameters.buffer.readUInt32BE(64) / 10;
      result.voltage1 = energyParameters.buffer.readUInt32BE(68) / 10;
      result.voltage2 = energyParameters.buffer.readUInt32BE(72) / 10;
      result.voltage3 = energyParameters.buffer.readUInt32BE(76) / 10;
      result.power1 = energyParameters.buffer.readInt32BE(80);
      result.power2 = energyParameters.buffer.readInt32BE(84);
      result.power3 = energyParameters.buffer.readInt32BE(88);
      result.reactivePower1 = energyParameters.buffer.readInt32BE(104);
      result.reactivePower2 = energyParameters.buffer.readInt32BE(108);
      result.reactivePower3 = energyParameters.buffer.readInt32BE(112);
    } else if (brand === 13) {
      // Acuvim II
      result.activeEnergy = (
        await modbusHelper.readHoldingRegisters(16456, 2)
      ).buffer.readFloatBE(0);
      result.negativeActiveEnergy = (
        await modbusHelper.readHoldingRegisters(26458, 2)
      ).buffer.readFloatBE(0);
      result.activePower = (
        await modbusHelper.readHoldingRegisters(16418, 2)
      ).buffer.readFloatBE(0);
      result.powerFactor = (
        await modbusHelper.readHoldingRegisters(16442, 2)
      ).buffer.readFloatBE(0);
      result.apparentEnergy = (
        await modbusHelper.readHoldingRegisters(16472, 2)
      ).buffer.readFloatBE(0);
      result.negativeReactiveEnergy = (
        await modbusHelper.readHoldingRegisters(16462, 2)
      ).buffer.readFloatBE(0);
      result.ReactiveEnergy = (
        await modbusHelper.readHoldingRegisters(16468, 2)
      ).buffer.readFloatBE(0);
      result.apparentPower = (
        await modbusHelper.readHoldingRegisters(16434, 2)
      ).buffer.readFloatBE(0);
      result.reactivePower = (
        await modbusHelper.readHoldingRegisters(16426, 2)
      ).buffer.readFloatBE(0);
      result.frequency = (
        await modbusHelper.readHoldingRegisters(16384, 2)
      ).buffer.readFloatBE(0);
      result.current1 = (
        await modbusHelper.readHoldingRegisters(16402, 2)
      ).buffer.readFloatBE(0);
      result.current2 = (
        await modbusHelper.readHoldingRegisters(16404, 2)
      ).buffer.readFloatBE(0);
      result.current3 = (
        await modbusHelper.readHoldingRegisters(16406, 2)
      ).buffer.readFloatBE(0);
      result.voltage12 = (
        await modbusHelper.readHoldingRegisters(16394, 2)
      ).buffer.readFloatBE(0);
      result.voltage23 = (
        await modbusHelper.readHoldingRegisters(16396, 2)
      ).buffer.readFloatBE(0);
      result.voltage32 = (
        await modbusHelper.readHoldingRegisters(16398, 2)
      ).buffer.readFloatBE(0);
      result.voltage1 = (
        await modbusHelper.readHoldingRegisters(16386, 2)
      ).buffer.readFloatBE(0);
      result.voltage2 = (
        await modbusHelper.readHoldingRegisters(16388, 2)
      ).buffer.readFloatBE(0);
      result.voltage3 = (
        await modbusHelper.readHoldingRegisters(16390, 2)
      ).buffer.readFloatBE(0);
      result.power1 = (
        await modbusHelper.readHoldingRegisters(16412, 2)
      ).buffer.readFloatBE(0);
      result.power2 = (
        await modbusHelper.readHoldingRegisters(16414, 2)
      ).buffer.readFloatBE(0);
      result.power3 = (
        await modbusHelper.readHoldingRegisters(16416, 2)
      ).buffer.readFloatBE(0);
      result.reactivePower1 = (
        await modbusHelper.readHoldingRegisters(16420, 2)
      ).buffer.readFloatBE(0);
      result.reactivePower2 = (
        await modbusHelper.readHoldingRegisters(16422, 2)
      ).buffer.readFloatBE(0);
      result.reactivePower3 = (
        await modbusHelper.readHoldingRegisters(16424, 2)
      ).buffer.readFloatBE(0);
      // energyParameters = await modbusHelper.readHoldingRegisters(4048, 80);
      // await new Promise((resolve) => setTimeout(resolve, 10));
      // result.negativeActiveEnergy =
      //   energyParameters.buffer.readUInt32BE(0) / 1000;
      // result.activeEnergy = energyParameters.buffer.readUInt32BE(4) / 1000;
      // result.apparentEnergy = energyParameters.buffer.readUInt32BE(12);
      // result.negativeReactiveEnergy = energyParameters.buffer.readUInt32BE(16);
      // result.reactiveEnergy = energyParameters.buffer.readUInt32BE(20);
      // result.activePower = energyParameters.buffer.readInt32BE(24) / 1000;
      // result.apparentPower = energyParameters.buffer.readInt32BE(28) / 1000;
      // result.reactivePower = energyParameters.buffer.readInt32BE(32) / 1000;
      // result.powerFactor = energyParameters.buffer.readUInt16BE(36) / 1000;
      // result.frequency = energyParameters.buffer.readUInt16BE(38) / 100;
      // result.current1 = energyParameters.buffer.readUInt32BE(40) / 1000;
      // result.current2 = energyParameters.buffer.readUInt32BE(44) / 1000;
      // result.current3 = energyParameters.buffer.readUInt32BE(48) / 1000;
      // result.voltage12 = energyParameters.buffer.readUInt32BE(56) / 10;
      // result.voltage23 = energyParameters.buffer.readUInt32BE(60) / 10;
      // result.voltage31 = energyParameters.buffer.readUInt32BE(64) / 10;
      // result.voltage1 = energyParameters.buffer.readUInt32BE(68) / 10;
      // result.voltage2 = energyParameters.buffer.readUInt32BE(72) / 10;
      // result.voltage3 = energyParameters.buffer.readUInt32BE(76) / 10;
      // result.power1 = energyParameters.buffer.readInt32BE(80);
      // result.power2 = energyParameters.buffer.readInt32BE(84);
      // result.power3 = energyParameters.buffer.readInt32BE(88);
      // result.reactivePower1 = energyParameters.buffer.readInt32BE(104);
      // result.reactivePower2 = energyParameters.buffer.readInt32BE(108);
      // result.reactivePower3 = energyParameters.buffer.readInt32BE(112);
    } else if (brand === 14) {
      // Circutor CVM-C10

      result.activeEnergy = (
        await modbusHelper.readHoldingRegisters(220, 2)
      ).buffer.readUInt32BE(0);
      result.negativeActiveEnergy = (
        await modbusHelper.readHoldingRegisters(240, 2)
      ).buffer.readUInt32BE(0);
      result.activePower =
        (await modbusHelper.readHoldingRegisters(48, 2)).buffer.readInt32BE(0) /
        1000;

      var PowerFactorDPM =
        (await modbusHelper.readHoldingRegisters(56, 2)).buffer.readInt32BE(0) /
        100;

      const invalidNumberRangeA = (key, value) => {
        if (['PowerFactorDPM'].includes(key)) {
          if (value > 1) {
            return 1;
          }
        }

        return value;
      };

      result.powerFactor = invalidNumberRangeA(
        'PowerFactorDPM',
        PowerFactorDPM
      );
      result.apparentEnergy = (
        await modbusHelper.readHoldingRegisters(232, 2)
      ).buffer.readUInt32BE(0);
      result.negativeReactiveEnergy = (
        await modbusHelper.readHoldingRegisters(244, 2)
      ).buffer.readUInt32BE(0);
      let inductiveReactiveEnergy = (
        await modbusHelper.readHoldingRegisters(224, 2)
      ).buffer.readUInt32BE(0);
      let capacitiveReactiveEnergy = (
        await modbusHelper.readHoldingRegisters(228, 2)
      ).buffer.readUInt32BE(0);
      result.reactiveEnergy =
        inductiveReactiveEnergy + capacitiveReactiveEnergy;
      result.apparentPower =
        (await modbusHelper.readHoldingRegisters(54, 2)).buffer.readUInt32BE(
          0
        ) / 1000;

      let inductiveReactivePower = (
        await modbusHelper.readHoldingRegisters(50, 2)
      ).buffer.readInt32BE(0);
      let capacitiveReactivePower = (
        await modbusHelper.readHoldingRegisters(52, 2)
      ).buffer.readInt32BE(0);
      result.reactivePower =
        (inductiveReactivePower + capacitiveReactivePower) / 1000;
      result.frequency =
        (await modbusHelper.readHoldingRegisters(60, 2)).buffer.readUInt32BE(
          0
        ) / 100;
      result.current1 =
        (await modbusHelper.readHoldingRegisters(2, 2)).buffer.readUInt32BE(0) /
        1000;
      result.current2 =
        (await modbusHelper.readHoldingRegisters(18, 2)).buffer.readUInt32BE(
          0
        ) / 1000;
      result.current3 =
        (await modbusHelper.readHoldingRegisters(34, 2)).buffer.readUInt32BE(
          0
        ) / 1000;
      result.voltage12 =
        (await modbusHelper.readHoldingRegisters(62, 2)).buffer.readUInt32BE(
          0
        ) / 10;
      result.voltage23 =
        (await modbusHelper.readHoldingRegisters(64, 2)).buffer.readUInt32BE(
          0
        ) / 10;
      result.voltage31 =
        (await modbusHelper.readHoldingRegisters(66, 2)).buffer.readUInt32BE(
          0
        ) / 10;
      result.voltage1 =
        (await modbusHelper.readHoldingRegisters(0, 2)).buffer.readUInt32BE(0) /
        10;
      result.voltage2 =
        (await modbusHelper.readHoldingRegisters(16, 2)).buffer.readUInt32BE(
          0
        ) / 10;
      result.voltage3 =
        (await modbusHelper.readHoldingRegisters(32, 2)).buffer.readUInt32BE(
          0
        ) / 10;
      result.power1 =
        (await modbusHelper.readHoldingRegisters(4, 2)).buffer.readInt32BE(0) /
        1000;
      result.power2 =
        (await modbusHelper.readHoldingRegisters(20, 2)).buffer.readInt32BE(0) /
        1000;
      result.power3 =
        (await modbusHelper.readHoldingRegisters(36, 2)).buffer.readInt32BE(0) /
        1000;
      result.reactivePower1 =
        (await modbusHelper.readHoldingRegisters(6, 2)).buffer.readInt32BE(0) /
        1000;
      result.reactivePower2 =
        (await modbusHelper.readHoldingRegisters(22, 2)).buffer.readInt32BE(0) /
        1000;
      result.reactivePower3 =
        (await modbusHelper.readHoldingRegisters(38, 2)).buffer.readInt32BE(0) /
        1000;
      // console.log('result , ', result);
    } else if (brand === 15) {
      // Schneider PM5100
      energyParameters = await modbusHelper.readHoldingRegisters(3203, 4);
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.activeEnergy =
        bignum.toNumber(await bignum.fromBuffer(energyParameters.buffer)) /
        1000;

      negativeActiveEnergyParameters = await modbusHelper.readHoldingRegisters(
        3207,
        4
      );
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.negativeActiveEnergy =
        bignum.toNumber(
          await bignum.fromBuffer(negativeActiveEnergyParameters.buffer)
        ) / 1000;

      apparentEnergyParameters = await modbusHelper.readHoldingRegisters(
        3235,
        4
      );
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.apparentEnergy =
        bignum.toNumber(
          await bignum.fromBuffer(apparentEnergyParameters.buffer)
        ) / 1000;

      reactiveEnergyParameters = await modbusHelper.readHoldingRegisters(
        3219,
        4
      );
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.reactiveEnergy =
        bignum.toNumber(
          await bignum.fromBuffer(reactiveEnergyParameters.buffer)
        ) / 1000;

      negativeReactiveEnergyParameters =
        await modbusHelper.readHoldingRegisters(3223, 4);
      await new Promise((resolve) => setTimeout(resolve, 10));
      result.negativeReactiveEnergy =
        bignum.toNumber(
          await bignum.fromBuffer(negativeReactiveEnergyParameters.buffer)
        ) / 1000;

      result.activePower = (
        await modbusHelper.readHoldingRegisters(3059, 4)
      ).buffer.readFloatBE(0);
      result.apparentPower = (
        await modbusHelper.readHoldingRegisters(3075, 4)
      ).buffer.readFloatBE(0);

      result.reactivePower = (
        await modbusHelper.readHoldingRegisters(3067, 4)
      ).buffer.readFloatBE(0);
      result.frequency = (
        await modbusHelper.readHoldingRegisters(3109, 4)
      ).buffer.readFloatBE(0);
      result.powerFactor = (
        await modbusHelper.readHoldingRegisters(3083, 4)
      ).buffer.readFloatBE(0);
      result.frequency = (
        await modbusHelper.readHoldingRegisters(3109, 4)
      ).buffer.readFloatBE(0);
      result.current1 = (
        await modbusHelper.readHoldingRegisters(2999, 4)
      ).buffer.readFloatBE(0);
      result.current2 = (
        await modbusHelper.readHoldingRegisters(3001, 4)
      ).buffer.readFloatBE(0);
      result.current3 = (
        await modbusHelper.readHoldingRegisters(3003, 4)
      ).buffer.readFloatBE(0);
      result.voltage12 = (
        await modbusHelper.readHoldingRegisters(3019, 4)
      ).buffer.readFloatBE(0);
      result.voltage23 = (
        await modbusHelper.readHoldingRegisters(3021, 4)
      ).buffer.readFloatBE(0);
      result.voltage31 = (
        await modbusHelper.readHoldingRegisters(3023, 4)
      ).buffer.readFloatBE(0);
      result.voltage1 = (
        await modbusHelper.readHoldingRegisters(3027, 4)
      ).buffer.readFloatBE(0);
      result.voltage2 = (
        await modbusHelper.readHoldingRegisters(3029, 4)
      ).buffer.readFloatBE(0);
      result.voltage3 = (
        await modbusHelper.readHoldingRegisters(3031, 4)
      ).buffer.readFloatBE(0);
      result.power1 = (
        await modbusHelper.readHoldingRegisters(3053, 4)
      ).buffer.readFloatBE(0);
      result.power2 = (
        await modbusHelper.readHoldingRegisters(3055, 4)
      ).buffer.readFloatBE(0);
      result.power3 = (
        await modbusHelper.readHoldingRegisters(3057, 4)
      ).buffer.readFloatBE(0);
      result.reactivePower1 = (
        await modbusHelper.readHoldingRegisters(3061, 4)
      ).buffer.readFloatBE(0);
      result.reactivePower2 = (
        await modbusHelper.readHoldingRegisters(3063, 4)
      ).buffer.readFloatBE(0);
      result.reactivePower3 = (
        await modbusHelper.readHoldingRegisters(3065, 4)
      ).buffer.readFloatBE(0);
    }

    if (
      !!piConfig.selectedSystemStructure &&
      piConfig.selectedSystemStructure === 2
    ) {
      let solarActivePower = 0;
      for (const smartloggerconfig of piConfig.smartloggerConfig) {
        try {
          solarActivePower +=
            localCache.smartLoggerReading[`smartlogger_${smartloggerconfig.id}`]
              .activePower;
        } catch (e) {}
      }
      logger.info('This system is using structure 2.');
      result.activePower = result.activePower - solarActivePower;
    }
  } catch (e) {
    logger.error('Digital Power Meter Error ref ', meterId, e); // show error in full error trace stack

    let error = {
      dpmID: '',
      errorName: '',
    };

    error.dpmID = meterId;
    error.errorName = e.name;

    localCache.errorDPM = error; // pass info to dataCombineHelper
    console.log('localCache.errorDPM', localCache.errorDPM);
  }

  return result;
};

module.exports = EnergyMeterHelper;
