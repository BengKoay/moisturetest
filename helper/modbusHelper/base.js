class Base {
  constructor() {
    return this;
  }

  async sleep(ms) {
    new Promise((resolve) => setTimeout(resolve, ms));
  }
}

module.exports = Base;
