const HUAWEI_INVERTER = require('./huawei/index');
const SMA_INVERTER = require('./sma/index');
const SUNGROW_INVERTER = require('./sungrow/index');

module.exports = {
  HUAWEI_INVERTER,
  SMA_INVERTER,
  SUNGROW_INVERTER
};
