const Base = require('../base');
const piConfig = require('../../../../../piConfig').piConfig;
let localCache = require('../../../../localCache');

const invalidNumberRange = (key, value) => {
  if (
    [
      'voltageA',
      'voltageB',
      'voltageC',
      'currentA',
      'currentB',
      'currentC',
      'frequency',
      'powerFactor'
    ].includes(key)
  ) {
    if (value >= 4294967 || value <= -4294967) {
      return 0;
    }
  }
  if (
    ['mppt', 'temperature', 'acPower', 'reactivePower', 'dcPower'].includes(key)
  ) {
    if (value >= 2147483 || value <= -2147483) {
      return 0;
    }
  }

  return value;
};
class SMA_INVERTER_CLUSTER_CONTROLLER extends Base {
  constructor(modbusHelper, args) {
    super(modbusHelper, args);
  }

  async readData() {
    try {
      let { id } = this.args;
      let payload = {
        ref: id
      };
      await this.modbusHelper.setID(id);

      let serialNumber = await this.modbusHelper.readHoldingRegisters(30057, 2);
      serialNumber = String(serialNumber.buffer.readUInt32BE(0));
      let inverter_data = await this.modbusHelper.readHoldingRegisters(
        30769,
        50
      );
      let inverter_data2 = await this.modbusHelper.readHoldingRegisters(
        30201,
        2
      );
      let inverter_data3 = await this.modbusHelper.readHoldingRegisters(
        30949,
        50
      );
      // let inverter_data4 = await modbusHelper.readHoldingRegisters(30531, 10);

      let refSerialNumber = '';
      if (!!serialNumber) {
        refSerialNumber = serialNumber.substring(
          serialNumber.length - 6,
          serialNumber.length
        );
      }

      payload.refSn = refSerialNumber;
      payload.voltageA = invalidNumberRange(
        'voltageA',
        inverter_data.buffer.readUInt32BE(28) / 100
      );
      payload.voltageB = invalidNumberRange(
        'voltageB',
        inverter_data.buffer.readUInt32BE(32) / 100
      );
      payload.voltageC = invalidNumberRange(
        'voltageC',
        inverter_data.buffer.readUInt32BE(36) / 100
      );
      payload.currentA = invalidNumberRange(
        'currentA',
        inverter_data.buffer.readUInt32BE(56) / 1000
      );
      payload.currentB = invalidNumberRange(
        'currentB',
        inverter_data.buffer.readUInt32BE(60) / 1000
      );
      payload.currentC = invalidNumberRange(
        'currentC',
        inverter_data.buffer.readUInt32BE(64) / 1000
      );
      payload.frequency = invalidNumberRange(
        'frequency',
        inverter_data.buffer.readUInt32BE(68) / 100
      );
      payload.powerFactor = invalidNumberRange(
        'powerFactor',
        inverter_data3.buffer.readUInt32BE(0) / 1000
      );
      payload.effInverter = 90;
      payload.temperature = invalidNumberRange(
        'temperature',
        inverter_data3.buffer.readInt32BE(8)
      );
      payload.status = inverter_data2.buffer.readUInt32BE(0);
      payload.peakPower = null;
      payload.acPower = invalidNumberRange(
        'acPower',
        inverter_data.buffer.readInt32BE(12) / 1000
      );
      payload.reactivePower = invalidNumberRange(
        'reactivePower',
        inverter_data.buffer.readInt32BE(72)
      );
      payload.dcPower = invalidNumberRange(
        'dcPower',
        inverter_data.buffer.readInt32BE(8) / 1000
      );
      payload.energyHourly = null;
      payload.energyDaily = null; //inverter_data4.buffer.readInt32BE(20);
      payload.energyMonthly = null;
      payload.energyTotal = null; //inverter_data4.buffer.readInt32BE(0);
      payload.startup = null;
      payload.shutdown = null;
      payload.serialNumber = serialNumber;
      payload.dcCapacity = null;

      let slConfig = piConfig.smartloggerConfig.filter((obj) => {
        return obj.id === this.smartLoggerIndex;
      })[0];

      let inverterObj = slConfig.inverterConfig.filter((obj) => {
        return obj.id === id;
      })[0];

      let noOfMppt0 = 0;
      let noOfMppt1 = 0;
      let noOfMppt2 = 0;
      let noOfMppt3 = 0;

      if (!!inverterObj && !!inverterObj.noOfMppt) {
        noOfMppt0 = inverterObj.noOfMppt[0] || 0;
        noOfMppt1 = inverterObj.noOfMppt[1] || 0;
        noOfMppt2 = inverterObj.noOfMppt[2] || 0;
        noOfMppt3 = inverterObj.noOfMppt[3] || 0;
      }

      payload.mpptPower = [
        {
          ref: 1,
          noOfMppt: noOfMppt0,
          power: inverter_data.buffer.readInt32BE(8) / 1000,
          voltage: {
            '1': invalidNumberRange(
              'mppt',
              inverter_data.buffer.readInt32BE(4) / 100
            )
            // '2': invalidNumberRange('mppt', null)
          },
          current: {
            '1': invalidNumberRange(
              'mppt',
              inverter_data.buffer.readInt32BE(0) / 1000
            )
            // '2': invalidNumberRange('mppt', null)
          }
        },
        {
          ref: 2,
          noOfMppt: noOfMppt1,
          power: inverter_data3.buffer.readInt32BE(24) / 1000,
          voltage: {
            '1': invalidNumberRange(
              'mppt',
              inverter_data3.buffer.readInt32BE(20) / 100
            )
            // '2': invalidNumberRange('mppt', null)
          },
          current: {
            '1': invalidNumberRange(
              'mppt',
              inverter_data3.buffer.readInt32BE(16) / 1000
            )
            // '2': invalidNumberRange('mppt', null)
          }
        },
        {
          ref: 3,
          noOfMppt: noOfMppt2,
          power: inverter_data3.buffer.readInt32BE(36) / 1000,
          voltage: {
            '1': invalidNumberRange(
              'mppt',
              inverter_data3.buffer.readInt32BE(32) / 100
            )
            // '2': invalidNumberRange('mppt', null)
          },
          current: {
            '1': invalidNumberRange(
              'mppt',
              inverter_data3.buffer.readInt32BE(28) / 1000
            )
            // '2': invalidNumberRange('mppt', null)
          }
        },
        {
          ref: 4,
          noOfMppt: noOfMppt3,
          power: inverter_data3.buffer.readInt32BE(48) / 1000,
          voltage: {
            '1': invalidNumberRange(
              'mppt',
              inverter_data3.buffer.readInt32BE(44) / 100
            )
            // '2': invalidNumberRange('mppt', null)
          },
          current: {
            '1': invalidNumberRange(
              'mppt',
              inverter_data3.buffer.readInt32BE(40) / 1000
            )
            // '2': invalidNumberRange('mppt', null)
          }
        }
      ];

      // payload.mpptPower = {
      //   1: null,
      //   2: null,
      //   3: null
      // };
      // payload.pcVoltage = {
      //   1: inverter_data.buffer.readInt32BE(4) / 100,
      //   2: inverter_data3.buffer.readInt32BE(20) / 100,
      //   3: inverter_data3.buffer.readInt32BE(32) / 100,
      //   4: inverter_data3.buffer.readInt32BE(44) / 100
      // };
      // payload.pvCurrent = {
      //   1: inverter_data.buffer.readInt32BE(0) / 1000,
      //   2: inverter_data3.buffer.readInt32BE(16) / 1000,
      //   3: inverter_data3.buffer.readInt32BE(28) / 1000,
      //   4: inverter_data3.buffer.readInt32BE(40) / 1000
      // };
      payload.lastUpdate = this.moment().unix();

      if (!localCache.inverterReading) {
        localCache.inverterReading = {};
      }

      localCache.inverterReading[`${id}`] = payload;

      // await this.writeDataToLocal(
      //   `${this.appProperties.localFileStorageLocation.ensureDirectories.inverterReadingDataPath}/${id}.json`,
      //   payload
      // );
    } catch (e) {
      this.logger.error(
        `SMA_INVERTER_CLUSTER_CONTROLLER Error ====> get inverter data error on inverter number of ${this.args.id}`,
        e
      );
    }
  }
}

module.exports = SMA_INVERTER_CLUSTER_CONTROLLER;
