const Base = require('../base');
const piConfig = require('../../../../../piConfig').piConfig;
let localCache = require('../../../../localCache');
class SMA_INVERTER_MANAGER extends Base {
  constructor(modbusHelper, args, smartLoggerIndex) {
    super(modbusHelper, args, smartLoggerIndex);
  }

  async readData() {
    try {
      let { id } = this.args;
      let payload = {
        ref: id,
      };

      await this.modbusHelper.setID(id);
      let inverter_data = await this.modbusHelper.readHoldingRegisters(
        40188,
        50
      );

      let sNumber = await this.modbusHelper.readHoldingRegisters(40052, 16);
      let serialNumber = sNumber.buffer.toString('utf8');
      let refSerialNumber = serialNumber.substring(
        serialNumber.length - 26,
        serialNumber.length - 14
      );

      payload.refSn = refSerialNumber;
      payload.acPower = inverter_data.buffer.readInt16BE(22) / 100; //40200
      payload.dcPower = inverter_data.buffer.readInt16BE(56) / 100; //40217
      payload.voltageA = inverter_data.buffer.readUInt16BE(14) / 10; //40196
      payload.voltageB = inverter_data.buffer.readUInt16BE(16) / 10; //40197
      payload.voltageC = inverter_data.buffer.readUInt16BE(18) / 10; //40198
      payload.currentA = inverter_data.buffer.readUInt16BE(0) / 10; //40189
      payload.currentB = inverter_data.buffer.readUInt16BE(2) / 10; //40190
      payload.currentC = inverter_data.buffer.readUInt16BE(4) / 10; //40191
      payload.frequency = inverter_data.buffer.readUInt16BE(26) / 1000; //40202
      payload.powerFactor = inverter_data.buffer.readInt16BE(38) / 1000; //40208
      payload.effInverter = ((payload.acPower / payload.dcPower) * 100).toFixed(
        2
      );
      payload.temperature = inverter_data.buffer.readInt16BE(60); //40219
      payload.status = null;
      payload.peakPower = null;
      payload.reactivePower = inverter_data.buffer.readInt16BE(34) / 10000; //40206
      payload.energyHourly = null;
      payload.energyDaily = null;
      payload.energyMonthly = null;
      payload.energyTotal = inverter_data.buffer.readUInt32BE(42) / 1000; //40210
      payload.startup = null;
      payload.shutdown = null;
      payload.serialNumber = sNumber.buffer.toString('utf8');
      payload.dcCapacity = null;

      payload.panelEfficiency = piConfig.panelEfficiency;

      let slConfig = piConfig.smartloggerConfig.filter((obj) => {
        return obj.id === this.smartLoggerIndex;
      })[0];

      let inverterObj = slConfig.inverterConfig.filter((obj) => {
        return obj.id === id;
      })[0];

      let noOfMppt0 = 0;

      if (!!inverterObj && !!inverterObj.noOfMppt) {
        noOfMppt0 = inverterObj.noOfMppt[0] || 0;
      }

      payload.mpptPower = [
        {
          ref: 1,
          noOfMppt: noOfMppt0,
          power: inverter_data.buffer.readInt16BE(56) / 100, //40217
          voltage: {
            1: inverter_data.buffer.readUInt16BE(52) / 10, //40215
          },
          current: {
            1: inverter_data.buffer.readUInt16BE(48) / 100, //40213
          },
        },
      ];
      payload.lastUpdate = this.moment().unix();

      if (!localCache.inverterReading) {
        localCache.inverterReading = {};
      }

      localCache.inverterReading[`${id}`] = payload;

      // await this.writeDataToLocal(
      //   `${this.appProperties.localFileStorageLocation.ensureDirectories.inverterReadingDataPath}/${id}.json`,
      //   payload
      // );
    } catch (e) {
      this.logger.error(
        `SMA_INVERTER_MANAGER Error ====> get inverter data error on inverter number of ${this.args.id}`,
        e
      );
    }
  }
}

module.exports = SMA_INVERTER_MANAGER;
