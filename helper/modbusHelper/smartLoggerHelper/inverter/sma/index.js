const SMA_INVERTER_CLUSTER_CONTROLLER = require('./sma_inverter_cluster_controller');
const SMA_INVERTER_MANAGER = require('./sma_inverter_manager');

module.exports = {
  SMA_INVERTER_CLUSTER_CONTROLLER,
  SMA_INVERTER_MANAGER
};
