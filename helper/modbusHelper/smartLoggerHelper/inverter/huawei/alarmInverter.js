const localCache = require('../../../../localCache');

const alarm = async (type, inverterId, model, number) => {
  for (let bit = 0; bit < 16; bit++) {
    if (Math.pow(2, bit) === number) {
      await errorCheck(type, inverterId, model, bit);
      return bit;
    }
  }
  return 0;
};
const errorCheck = async (type, inverterId, model, bit) => {
  // severity value 1 = warning, 2 = minor, 3 = major
  let error = {
    inverterID: '',
    alarmName: '',
    alarmID: '',
    severity: '',
    code: '',
  };
  // inverter 12KTL, 20KTL, 36KTL
  if (['12KTL', '36KTL'].includes(model)) {
    // alarm 1
    if (type === 1) {
      switch (bit) {
        case 1:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 1';
          error.alarmID = '106';
          error.severity = '1';

          break;
        case 2:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 2';
          error.alarmID = '107';
          error.severity = '1';

          break;
        case 3:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 3';
          error.alarmID = '108';
          error.severity = '1';

          break;
        case 4:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 4';
          error.alarmID = '109';
          error.severity = '1';

          break;
        case 5:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 5';
          error.alarmID = '110';
          error.severity = '1';

          break;
        case 6:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 6';
          error.alarmID = '111';
          error.severity = '1';

          break;
        case 10:
          error.inverterID = inverterId;
          error.alarmName = ' Software Ver. Unmatch';
          error.alarmID = '504';
          error.severity = '2';

          break;
        case 12:
          error.inverterID = inverterId;
          error.alarmName = ' Upgrade Failed';
          error.alarmID = '505';
          error.severity = '3';

          break;
        case 13:
          error.inverterID = inverterId;
          error.alarmName = ' Flash Fault';
          error.alarmID = '61440';
          error.severity = '2';

          break;
        case 14:
          error.inverterID = inverterId;
          error.alarmName = ' License Expired';
          error.alarmID = '506';
          error.severity = '1';

          break;
        default:
      }
    }
    // alarm 2
    if (type === 2) {
      switch (bit) {
        case 1:
          error.inverterID = inverterId;
          error.alarmName = ' Software Ver. Unmatch';
          error.alarmID = '504';
          error.severity = '2';

          break;
        case 2:
          error.inverterID = inverterId;
          error.alarmName = ' Software Ver. Unmatch';
          error.alarmID = '504';
          error.severity = '2';

          break;
        case 3:
          error.inverterID = inverterId;
          error.alarmName = ' System Fault';
          error.alarmID = '400';
          error.severity = '3';

          break;
        case 4:
          error.inverterID = inverterId;
          error.alarmName = ' System Fault';
          error.alarmID = '400';
          error.severity = '3';

          break;
        case 5:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal DC Circuit';
          error.alarmID = '200';
          error.severity = '3';

          break;
        case 6:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Inv. Circuit';
          error.alarmID = '202';
          error.severity = '3';

          break;
        case 7:
          error.inverterID = inverterId;
          error.alarmName = ' Abn. Residual Curr';
          error.alarmID = '318';
          error.severity = '3';

          break;
        case 8:
          error.inverterID = inverterId;
          error.alarmName = ' Overtemperature';
          error.alarmID = '321';
          error.severity = '3';

          break;
        case 10:
          error.inverterID = inverterId;
          error.alarmName = ' System Fault';
          error.alarmID = '400';
          error.severity = '3';

          break;
        case 11:
          error.inverterID = inverterId;
          error.alarmName = ' Fan Fault';
          error.alarmID = '320';
          error.severity = '2';

          break;
        case 12:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal SPI Comm';
          error.alarmID = '322';
          error.severity = '3';

          break;
        case 14:
          error.inverterID = inverterId;
          error.alarmName = ' System Fault';
          error.alarmID = '400';
          error.severity = '3';

          break;
        default:
      }
    }
    // alarm 3
    if (type === 3) {
      switch (bit) {
        case 0:
          error.inverterID = inverterId;
          error.alarmName = ' Low Insulation Resistance';
          error.alarmID = '313';
          error.severity = '3';

          break;
        case 1:
          error.inverterID = inverterId;
          error.alarmName = ' AFCI Self-Check Failure';
          error.alarmID = '411';
          error.severity = '3';

          break;
        case 2:
          error.inverterID = inverterId;
          error.alarmName = ' DC Arc Fault';
          error.alarmID = '412';
          error.severity = '3';

          break;
        case 3:
          error.inverterID = inverterId;
          error.alarmName = ' AFCI Self-Check Failure';
          error.alarmID = '411';
          error.severity = '3';

          break;
        case 4:
          error.inverterID = inverterId;
          error.alarmName = ' AFCI Self-Check Failure';
          error.alarmID = '411';
          error.severity = '3';

          break;
        case 5:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Inv. Circuit';
          error.alarmID = '202';
          error.severity = '3';

          break;
        case 7:
          error.inverterID = inverterId;
          error.alarmName = ' System Fault';
          error.alarmID = '400';
          error.severity = '3';

          break;
        case 8:
          error.inverterID = inverterId;
          error.alarmName = ' System Fault';
          error.alarmID = '400';
          error.severity = '3';

          break;
        case 9:
          error.inverterID = inverterId;
          error.alarmName = ' String 3 Reversed';
          error.alarmID = '122';
          error.severity = '3';

          break;
        case 12:
          error.inverterID = inverterId;
          error.alarmName = ' DC Arc Fault';
          error.alarmID = '412';
          error.severity = '3';

          break;
        case 13:
          error.inverterID = inverterId;
          error.alarmName = ' DC Arc Fault';
          error.alarmID = '412';
          error.severity = '3';

          break;
        case 14:
          error.inverterID = inverterId;
          error.alarmName = ' DC Arc Fault';
          error.alarmID = '412';
          error.severity = '3';

          break;
        case 15:
          error.inverterID = inverterId;
          error.alarmName = ' System Fault';
          error.alarmID = '400';
          error.severity = '3';

          break;
        default:
      }
    }
    // alarm 4
    if (type === 4) {
      switch (bit) {
        case 2:
          error.inverterID = inverterId;
          error.alarmName = ' String 1 Reversed';
          error.alarmID = '120';
          error.severity = '1';

          break;
        case 3:
          error.inverterID = inverterId;
          error.alarmName = ' String 2 Reversed';
          error.alarmID = '121';
          error.severity = '1';

          break;
        case 4:
          error.inverterID = inverterId;
          error.alarmName = ' String 7 Reversed';
          error.alarmID = '126';
          error.severity = '3';

          break;
        case 5:
          error.inverterID = inverterId;
          error.alarmName = ' String 7 Reversed';
          error.alarmID = '126';
          error.severity = '1';

          break;
        case 6:
          error.inverterID = inverterId;
          error.alarmName = ' String 8 Reversed';
          error.alarmID = '127';
          error.severity = '3';

          break;
        case 7:
          error.inverterID = inverterId;
          error.alarmName = ' String 8 Reversed';
          error.alarmID = '127';
          error.severity = '1';

          break;
        case 8:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal PV String Connection';
          error.alarmID = '413';
          error.severity = '3';

          break;
        case 9:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal PV String Connection';
          error.alarmID = '413';
          error.severity = '3';

          break;
        case 10:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal PV String Connection';
          error.alarmID = '413';
          error.severity = '3';

          break;
        case 11:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal PV String Connection';
          error.alarmID = '413';
          error.severity = '3';

          break;
        case 12:
          error.inverterID = inverterId;
          error.alarmName = ' String 3 Reversed';
          error.alarmID = '122';
          error.severity = '1';

          break;
        case 13:
          error.inverterID = inverterId;
          error.alarmName = ' String 4 Reversed';
          error.alarmID = '123';
          error.severity = '1';

          break;
        case 14:
          error.inverterID = inverterId;
          error.alarmName = ' String 5 Reversed';
          error.alarmID = '124';
          error.severity = '1';

          break;
        case 15:
          error.inverterID = inverterId;
          error.alarmName = ' String 6 Reversed';
          error.alarmID = '125';
          error.severity = '1';

          break;
        default:
      }
    }
    // alarm 5
    if (type === 5) {
      switch (bit) {
        case 1:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal DC Circuit';
          error.alarmID = '200';
          error.severity = '3';

          break;
        case 2:
          error.inverterID = inverterId;
          error.alarmName = ' Abn. Auxiliary Power';
          error.alarmID = '410';
          error.severity = '3';

          break;
        case 4:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal DC Circuit';
          error.alarmID = '200';
          error.severity = '3';

          break;
        case 5:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal DC Circuit';
          error.alarmID = '200';
          error.severity = '3';

          break;
        case 6:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal DC Circuit';
          error.alarmID = '200';
          error.severity = '3';

          break;
        case 7:
          error.inverterID = inverterId;
          error.alarmName = ' BST Inductor Cable Connection Abnormal';
          error.alarmID = '414';
          error.severity = '3';

          break;
        case 8:
          error.inverterID = inverterId;
          error.alarmName = ' BST Inductor Cable Connection Abnormal';
          error.alarmID = '414';
          error.severity = '3';

          break;
        case 9:
          error.inverterID = inverterId;
          error.alarmName = ' BST Inductor Cable Connection Abnormal';
          error.alarmID = '414';
          error.severity = '3';

          break;
        case 10:
          error.inverterID = inverterId;
          error.alarmName = ' BST Inductor Cable Connection Abnormal';
          error.alarmID = '414';
          error.severity = '3';

          break;
        default:
      }
    }
    // alarm 6
    if (type === 6) {
      switch (bit) {
        case 6:
          error.inverterID = inverterId;
          error.alarmName = ' System Fault';
          error.alarmID = '400';
          error.severity = '3';

          break;
        case 10:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Inv. Circuit';
          error.alarmID = '202';
          error.severity = '3';

          break;
        case 12:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Inv. Circuit';
          error.alarmID = '202';
          error.severity = '3';

          break;
        default:
      }
    }
    // alarm 7
    if (type === 7) {
      switch (bit) {
        case 1:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Inv. Circuit';
          error.alarmID = '202';
          error.severity = '3';

          break;
        case 5:
          error.inverterID = inverterId;
          error.alarmName = ' System Fault';
          error.alarmID = '400';
          error.severity = '3';

          break;
        default:
      }
    }
    // alarm 8
    if (type === 8) {
      switch (bit) {
        case 0:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Voltage';
          error.alarmID = '301';
          error.severity = '3';

          break;
        case 3:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Voltage';
          error.alarmID = '301';
          error.severity = '3';

          break;
        case 6:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Frequency';
          error.alarmID = '305';
          error.severity = '3';

          break;
        case 7:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Frequency';
          error.alarmID = '305';
          error.severity = '3';

          break;
        case 8:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Voltage';
          error.alarmID = '301';
          error.severity = '3';

          break;
        case 9:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Voltage';
          error.alarmID = '301';
          error.severity = '3';

          break;
        case 10:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grounding';
          error.alarmID = '326';
          error.severity = '3';
          break;
        case 11:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Voltage';
          error.alarmID = '301';
          error.severity = '3';

          break;
        case 12:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Frequency';
          error.alarmID = '305';
          error.severity = '3';

          break;
        default:
      }
    }
    // alarm 9
    if (type === 9) {
      switch (bit) {
        case 0:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Voltage';
          error.alarmID = '301';
          error.severity = '3';

          break;
        case 1:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Voltage';
          error.alarmID = '301';
          error.severity = '3';

          break;
        case 2:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Voltage';
          error.alarmID = '301';
          error.severity = '3';

          break;
        case 8:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal Grid Voltage';
          error.alarmID = '301';
          error.severity = '3';

          break;
        default:
      }
    }
    // alarm 16
    if (type === 16) {
      switch (bit) {
        case 0:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 1';
          error.alarmID = '106';
          error.severity = '1';

          break;
        case 1:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 2';
          error.alarmID = '107';
          error.severity = '1';

          break;
        case 2:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 3';
          error.alarmID = '108';
          error.severity = '1';

          break;
        case 3:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 4';
          error.alarmID = '109';
          error.severity = '1';

          break;
        case 4:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 5';
          error.alarmID = '110';
          error.severity = '1';

          break;
        case 5:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 6';
          error.alarmID = '111';
          error.severity = '1';

          break;
        case 6:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 7';
          error.alarmID = '112';
          error.severity = '1';

          break;
        case 7:
          error.inverterID = inverterId;
          error.alarmName = ' Abnormal String 8';
          error.alarmID = '113';
          error.severity = '1';

          break;
        default:
      }
    }
  }
  // inverter 60KTL
  if (['12KTL_2', '17KTL', '20KTL', '60KTL', '100KTL'].includes(model)) {
    // alarm 1
    if (type === 1) {
      switch (bit) {
        case 0:
          error.inverterID = inverterId;
          error.alarmName = 'High String Input Voltage';
          error.alarmID = '2001';
          error.severity = '3';

          break;
        case 1:
          error.inverterID = inverterId;
          error.alarmName = ' DC Arc Fault';
          error.alarmID = '2002';
          error.severity = '3';

          break;
        case 2:
          error.inverterID = inverterId;
          error.alarmName = ' String Reverse Connection';
          error.alarmID = '2011';
          error.severity = '3';

          break;
        case 3:
          error.inverterID = inverterId;
          error.alarmName = 'String Current Backfeed';
          error.alarmID = '2012';
          error.severity = '1';

          break;
        case 4:
          error.inverterID = inverterId;
          error.alarmName = 'Abnormal String Power';
          error.alarmID = '2013';
          error.severity = '1';

          break;
        case 5:
          error.inverterID = inverterId;
          error.alarmName = 'AFCI Self-Check Fail';
          error.alarmID = '2021';
          error.severity = '3';

          break;
        case 6:
          error.inverterID = inverterId;
          error.alarmName = 'Phase Wire Short-Circuited to PE';
          error.alarmID = '2031';
          error.severity = '3';

          break;
        case 7:
          error.inverterID = inverterId;
          error.alarmName = 'Grid Loss';
          error.alarmID = '2032';
          error.severity = '3';

          break;
        case 8:
          error.inverterID = inverterId;
          error.alarmName = ' Grid Undervoltage';
          error.alarmID = '2033';
          error.severity = '3';

          break;
        case 9:
          error.inverterID = inverterId;
          error.alarmName = ' Grid Overvoltage';
          error.alarmID = '2034';
          error.severity = '3';

          break;
        case 10:
          error.inverterID = inverterId;
          error.alarmName = ' Grid Volt. Imbalance';
          error.alarmID = '2035';
          error.severity = '3';

          break;
        case 11:
          error.inverterID = inverterId;
          error.alarmName = ' Grid Overfrequency';
          error.alarmID = '2036';
          error.severity = '3';

          break;
        case 12:
          error.inverterID = inverterId;
          error.alarmName = ' Grid Underfrequency';
          error.alarmID = '2037';
          error.severity = '3';

          break;
        case 13:
          error.inverterID = inverterId;
          error.alarmName = 'Unstable Grid Frequency';
          error.alarmID = '2038';
          error.severity = '3';

          break;
        case 14:
          error.inverterID = inverterId;
          error.alarmName = ' Output Overcurrent';
          error.alarmID = '2039';
          error.severity = '3';

          break;
        case 15:
          error.inverterID = inverterId;
          error.alarmName = 'Output DC Component Overhigh';
          error.alarmID = '2040';
          error.severity = '3';

          break;
      }
    }
    // alarm 2
    if (type === 2) {
      switch (bit) {
        case 0:
          error.inverterID = inverterId;
          error.alarmName = 'Abnormal Residual Current';
          error.alarmID = '2051';
          error.severity = '3';

          break;
        case 1:
          error.inverterID = inverterId;
          error.alarmName = 'Abnormal Grounding';
          error.alarmID = '2061';
          error.severity = '3';

          break;
        case 2:
          error.inverterID = inverterId;
          error.alarmName = 'Low Insulation Resistance';
          error.alarmID = '2062';
          error.severity = '3';

          break;
        case 3:
          error.inverterID = inverterId;
          error.alarmName = 'Overtemperature';
          error.alarmID = '2063';
          error.severity = '2';

          break;
        case 4:
          error.inverterID = inverterId;
          error.alarmName = 'Device Fault';
          error.alarmID = '2064';
          error.severity = '3';

          break;
        case 5:
          error.inverterID = inverterId;
          error.alarmName = 'Upgrade Failed or Version Mismatch';
          error.alarmID = '2065';
          error.severity = '2';

          break;
        case 6:
          error.inverterID = inverterId;
          error.alarmName = ' License Expired';
          error.alarmID = '2066';
          error.severity = '1';

          break;
        case 7:
          error.inverterID = inverterId;
          error.alarmName = 'Faulty Monitoring Unit';
          error.alarmID = '61440';
          error.severity = '2';

          break;
        case 8:
          error.inverterID = inverterId;
          error.alarmName = 'Faulty Power Collector';
          error.alarmID = '2067';
          error.severity = '3';

          break;
        case 9:
          error.inverterID = inverterId;
          error.alarmName = 'Battery abnormal';
          error.alarmID = '2068';
          error.severity = '3';

          break;
        case 10:
          error.inverterID = inverterId;
          error.alarmName = 'Active Islanding';
          error.alarmID = '2070';
          error.severity = '3';

          break;
        case 11:
          error.inverterID = inverterId;
          error.alarmName = 'Passive Islanding';
          error.alarmID = '2071';
          error.severity = '3';

          break;
        case 12:
          error.inverterID = inverterId;
          error.alarmName = 'Transient AC Overvoltage';
          error.alarmID = '2072';
          error.severity = '3';

          break;
        case 13:
          error.inverterID = inverterId;
          error.alarmName = 'Peripheral port short circuit';
          error.alarmID = '2075';
          error.severity = '1';

          break;
        case 14:
          error.inverterID = inverterId;
          error.alarmName = 'Churn output overload';
          error.alarmID = '2077';
          error.severity = '3';

          break;
        case 15:
          error.inverterID = inverterId;
          error.alarmName = 'Abnormal PV module configuration';
          error.alarmID = '2080';
          error.severity = '3';

          break;
        default:
      }
    }
    // alarm 3
    if (type === 3) {
      switch (bit) {
        case 0:
          error.inverterID = inverterId;
          error.alarmName = 'Optimizer fault';
          error.alarmID = '2081';
          error.severity = '1';

          break;
        case 1:
          error.inverterID = inverterId;
          error.alarmName = 'Built-in PID operation abnormal';
          error.alarmID = '2085';
          error.severity = '2';

          break;
        case 2:
          error.inverterID = inverterId;
          error.alarmName = 'High input string voltage to ground';
          error.alarmID = '2014';
          error.severity = '3';

          break;
        case 3:
          error.inverterID = inverterId;
          error.alarmName = 'External Fan Abnormal';
          error.alarmID = '2086';
          error.severity = '3';

          break;
        case 4:
          error.inverterID = inverterId;
          error.alarmName = 'Battery Reverse Connection';
          error.alarmID = '2069';
          error.severity = '3';

          break;
        case 5:
          error.inverterID = inverterId;
          error.alarmName = 'On-grid/Off-grid controller abnormal';
          error.alarmID = '2082';
          error.severity = '3';

          break;
        case 6:
          error.inverterID = inverterId;
          error.alarmName = 'PV String Loss';
          error.alarmID = '2015';
          error.severity = '1';

          break;
        case 7:
          error.inverterID = inverterId;
          error.alarmName = 'Internal Fan Abnormal';
          error.alarmID = '2087';
          error.severity = '3';

          break;
        case 8:
          error.inverterID = inverterId;
          error.alarmName = 'DC Protection Unit Abnormal';
          error.alarmID = '2088';
          error.severity = '3';

          break;
        default:
      }
    }
  }

  // old 60KTL inverter alarm list

  // if (['20KTL', '60KTL'].includes(model)) {
  //   // alarm 1
  //   if (type === 1) {
  //     switch (bit) {
  //       case 0:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' High String Voltage';
  //         error.alarmID = '2001';
  //         error.severity = '3';

  //         break;
  //       case 1:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' DC Arc Fault';
  //         error.alarmID = '2002';
  //         error.severity = '3';

  //         break;
  //       case 2:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' String reversed';
  //         error.alarmID = '2011';
  //         error.severity = '3';

  //         break;
  //       case 3:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' PV String Backfeed';
  //         error.alarmID = '2012';
  //         error.severity = '3';

  //         break;
  //       case 4:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Abnormal String';
  //         error.alarmID = '2013';
  //         error.severity = '3';

  //         break;
  //       case 5:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' AFCI Self-test Fault';
  //         error.alarmID = '2021';
  //         error.severity = '3';

  //         break;
  //       case 6:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Short circuit between phase to PE';
  //         error.alarmID = '2031';
  //         error.severity = '3';

  //         break;
  //       case 7:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Power Grid Failure';
  //         error.alarmID = '2032';
  //         error.severity = '3';

  //         break;
  //       case 8:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Grid Undervoltage';
  //         error.alarmID = '2033';
  //         error.severity = '3';

  //         break;
  //       case 9:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Grid Overvoltage';
  //         error.alarmID = '2034';
  //         error.severity = '3';

  //         break;
  //       case 10:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Unbalanced Grid Voltage';
  //         error.alarmID = '2035';
  //         error.severity = '3';

  //         break;
  //       case 11:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Grid Overfrequency';
  //         error.alarmID = '2036';
  //         error.severity = '3';

  //         break;
  //       case 12:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Grid Underfrequency';
  //         error.alarmID = '2037';
  //         error.severity = '3';

  //         break;
  //       case 13:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Grid Frequency Instability';
  //         error.alarmID = '2038';
  //         error.severity = '3';

  //         break;
  //       case 14:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Output Overcurrent';
  //         error.alarmID = '2039';
  //         error.severity = '3';

  //         break;
  //       case 15:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Large DC of Output current';
  //         error.alarmID = '2040';
  //         error.severity = '3';

  //         break;
  //     }
  //   }
  //   // alarm 2
  //   if (type === 2) {
  //     switch (bit) {
  //       case 0:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Abnormal Leakage Current';
  //         error.alarmID = '2051';
  //         error.severity = '3';

  //         break;
  //       case 1:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Abnormal Ground';
  //         error.alarmID = '2061';
  //         error.severity = '3';

  //         break;
  //       case 2:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Low Insulation Res';
  //         error.alarmID = '504';
  //         error.severity = '2';

  //         break;
  //       case 3:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' High Temperature';
  //         error.alarmID = '2063';
  //         error.severity = '3';

  //         break;
  //       case 4:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Abnormal Equipment';
  //         error.alarmID = '2064';
  //         error.severity = '3';

  //         break;
  //       case 5:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Upgrade Failed';
  //         error.alarmID = '2065';
  //         error.severity = '2';

  //         break;
  //       case 6:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' License Expired';
  //         error.alarmID = '2066';
  //         error.severity = '1';

  //         break;
  //       case 7:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Abnormal Monitor Unit';
  //         error.alarmID = '61440';
  //         error.severity = '2';

  //         break;
  //       default:
  //     }
  //   }
  //   // alarm 3
  //   if (type === 3) {
  //     switch (bit) {
  //       case 1:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' High String Voltage to Ground';
  //         error.alarmID = '2014';
  //         error.severity = '3';

  //         break;
  //       case 2:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Built-in PID operation abnormal';
  //         error.alarmID = '2085';
  //         error.severity = '3';

  //         break;
  //       default:
  //     }
  //   }
  // }
  // if (!localCache.errorInverter) {
  //   localCache.errorInverter = {};
  // }
  // if(!!error.inverterID){

  // }
  // localCache.errorInverter = []
  // localCache.errorInverter.push(error);
  localCache.errorInverter[`${inverterId}`] = error;
};

module.exports = { alarm };
