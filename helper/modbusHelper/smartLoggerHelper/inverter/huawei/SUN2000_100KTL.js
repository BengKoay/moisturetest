const Base = require('../base');
const piConfig = require('../../../../../piConfig').piConfig;
let localCache = require('../../../../localCache');
const alarmInverter = require('./alarmInverter');
const moment = require('moment-timezone');

var energyHour = 0;

const invalidNumberRange = (key, value) => {
  if (['voltageA', 'voltageB'].includes(key)) {
    if (value > 922 || value < 0) {
      return 0;
    }
  }
  if (['currentA', 'currentB'].includes(key)) {
    if (value > 300 || value < 0) {
      return 0;
    }
  }

  return value;
};

class SUN2000_100KTL extends Base {
  constructor(modbusHelper, args, smartLoggerIndex) {
    super(modbusHelper, args, smartLoggerIndex);
  }

  async readData() {
    try {
      let { id } = this.args;
      let payload = {
        ref: id,
      };
      await this.modbusHelper.setID(id);
      let inverter_data = await this.modbusHelper.readHoldingRegisters(
        32064,
        100
      );
      let sNumber = await this.modbusHelper.readHoldingRegisters(30015, 10);
      let serialNumber = sNumber.buffer.toString('utf8');
      let refSerialNumber = serialNumber.substring(
        serialNumber.length - 6,
        serialNumber.length
      );

      payload.refSn = refSerialNumber;
      if (
        [12340, 12342, 12344, 12337, 12338, 12339, 12336].includes(
          sNumber.data[3]
        )
      ) {
        let refSerialNumber = serialNumber.substring(
          serialNumber.length - 14,
          serialNumber.length - 8
        );
        payload.refSn = refSerialNumber;
      }

      payload.voltageA = inverter_data.buffer.readUInt16BE(10) / 10; //32069
      payload.voltageB = inverter_data.buffer.readUInt16BE(12) / 10; //32070
      payload.voltageC = inverter_data.buffer.readUInt16BE(14) / 10; //32071
      payload.currentA = inverter_data.buffer.readInt32BE(16) / 1000; //32072
      payload.currentB = inverter_data.buffer.readInt32BE(20) / 1000; //32074
      payload.currentC = inverter_data.buffer.readInt32BE(24) / 1000; //32076
      payload.frequency = inverter_data.buffer.readUInt16BE(42) / 100; //32085
      payload.powerFactor = inverter_data.buffer.readInt16BE(40) / 1000; //32084
      payload.effInverter = inverter_data.buffer.readUInt16BE(44) / 100; //32086
      payload.temperature = inverter_data.buffer.readInt16BE(46) / 10; //32087

      // payload.status = inverter_data.buffer.readUInt16BE(50); //32089
      //  huaweiInverterStatus[inverter_data.buffer.readUInt16BE(20)];

      payload.peakPower = inverter_data.buffer.readInt32BE(28) / 1000; //32078
      payload.acPower = inverter_data.buffer.readInt32BE(32) / 1000; //32080
      payload.reactivePower = inverter_data.buffer.readInt32BE(36) / 1000; //32082
      payload.dcPower = inverter_data.buffer.readInt32BE(0) / 1000; // 32064
      payload.energyDaily = inverter_data.buffer.readUInt32BE(100) / 100; //32114

      let databeforeFiftyNineMinutes = moment().minute() < 59;

      if (moment().minute() === 59) {
        energyHour = 0;
      }
      if (databeforeFiftyNineMinutes) {
        energyHour += payload.acPower * (5 / 60) * 0.0167;
        payload.energyHourly = energyHour;
      }

      payload.energyMonthly = inverter_data.buffer.readUInt32BE(104) / 100; //32116
      payload.energyTotal = inverter_data.buffer.readUInt32BE(84) / 100; //32106
      payload.startup = inverter_data.buffer.readUInt32BE(54); //32091
      payload.shutdown = inverter_data.buffer.readUInt32BE(58); //32093
      payload.serialNumber = serialNumber;
      payload.panelEfficiency = piConfig.panelEfficiency;
      payload.arrayArea = piConfig.arrayArea;
      payload.dcCapacity = 60;
      let alarmNotification = await this.modbusHelper.readHoldingRegisters(
        32008,
        6
      );
      payload.alarmNotification1 = await alarmInverter.alarm(
        1,
        id,
        '100KTL',
        alarmNotification.buffer.readUInt16BE(0)
      );
      payload.alarmNotification2 = await alarmInverter.alarm(
        2,
        id,
        '100KTL',
        alarmNotification.buffer.readUInt16BE(2)
      );
      payload.alarmNotification3 = await alarmInverter.alarm(
        3,
        id,
        '100KTL',
        alarmNotification.buffer.readUInt16BE(4)
      );
      let alarmPriority = await this.modbusHelper.readHoldingRegisters(
        32090,
        1
      );
      payload.alarmPriority = alarmPriority.buffer.readUInt16BE(0);

      let mppt_values = await this.modbusHelper.readHoldingRegisters(32016, 24);
      let pvStringParameter = await this.modbusHelper.readHoldingRegisters(
        32016,
        40
      );

      const voltage = [
        'voltage',
        invalidNumberRange('voltageA', pvStringParameter.data[8]),
        invalidNumberRange('voltageB', pvStringParameter.data[10]),
      ];
      const current = [
        'current',
        invalidNumberRange('currentA', pvStringParameter.data[21] / 100),
        invalidNumberRange('currentB', pvStringParameter.data[3] / 100),
      ];
      let slConfig = piConfig.smartloggerConfig.filter((obj) => {
        return obj.id === this.smartLoggerIndex;
      })[0];

      let inverterObj = slConfig.inverterConfig.filter((obj) => {
        return obj.id === id;
      })[0];
      let noOfMppt0 = 0;
      let noOfMppt1 = 0;
      let noOfMppt2 = 0;
      let noOfMppt3 = 0;
      let noOfMppt4 = 0;
      let noOfMppt5 = 0;
      let noOfMppt6 = 0;
      let noOfMppt7 = 0;
      let noOfMppt8 = 0;
      let noOfMppt9 = 0;

      if (!!inverterObj && !!inverterObj.noOfMppt) {
        noOfMppt0 = inverterObj.noOfMppt[0] || 0;
        noOfMppt1 = inverterObj.noOfMppt[1] || 0;
        noOfMppt2 = inverterObj.noOfMppt[2] || 0;
        noOfMppt3 = inverterObj.noOfMppt[3] || 0;
        noOfMppt4 = inverterObj.noOfMppt[4] || 0;
        noOfMppt5 = inverterObj.noOfMppt[5] || 0;
        noOfMppt6 = inverterObj.noOfMppt[6] || 0;
        noOfMppt7 = inverterObj.noOfMppt[7] || 0;
        noOfMppt8 = inverterObj.noOfMppt[8] || 0;
        noOfMppt9 = inverterObj.noOfMppt[9] || 0;
      }

      payload.mpptPower = [
        {
          ref: 1,
          noOfMppt: noOfMppt0,
          power:
            ((invalidNumberRange('voltageA', pvStringParameter.data[0] / 10) +
              invalidNumberRange('voltageB', pvStringParameter.data[2] / 10)) *
              (invalidNumberRange('currentA', pvStringParameter.data[1] / 100) +
                invalidNumberRange(
                  'currentB',
                  pvStringParameter.data[3] / 100
                ))) /
            2000,
          //invalidNumberRange(
          // 'currentA',
          // pvACSupply.buffer.readUInt32BE(48) / 1000
          voltage: {
            1: invalidNumberRange('voltageA', pvStringParameter.data[0] / 10),
            2: invalidNumberRange('voltageB', pvStringParameter.data[2] / 10),
          },
          current: {
            1: invalidNumberRange('currentA', pvStringParameter.data[1] / 100),
            2: invalidNumberRange('currentB', pvStringParameter.data[3] / 100),
          },
        },
        {
          ref: 2,
          noOfMppt: noOfMppt1,
          power:
            ((invalidNumberRange('voltageA', pvStringParameter.data[4] / 10) +
              invalidNumberRange('voltageB', pvStringParameter.data[6] / 10)) *
              (invalidNumberRange('currentA', pvStringParameter.data[5] / 100) +
                invalidNumberRange(
                  'currentB',
                  pvStringParameter.data[7] / 100
                ))) /
            2000,
          voltage: {
            1: invalidNumberRange('voltageA', pvStringParameter.data[4] / 10),
            2: invalidNumberRange('voltageB', pvStringParameter.data[6] / 10),
          },
          current: {
            1: invalidNumberRange('currentA', pvStringParameter.data[5] / 100),
            2: invalidNumberRange('currentB', pvStringParameter.data[7] / 100),
          },
        },
        {
          ref: 3,
          noOfMppt: noOfMppt2,
          power:
            ((invalidNumberRange('voltageA', pvStringParameter.data[8] / 10) +
              invalidNumberRange('voltageB', pvStringParameter.data[10] / 10)) *
              (invalidNumberRange('currentA', pvStringParameter.data[9] / 100) +
                invalidNumberRange(
                  'currentB',
                  pvStringParameter.data[11] / 100
                ))) /
            2000,

          voltage: {
            1: invalidNumberRange('voltageA', pvStringParameter.data[8] / 10),
            2: invalidNumberRange('voltageB', pvStringParameter.data[10] / 10),
          },
          current: {
            1: invalidNumberRange('currentA', pvStringParameter.data[9] / 100),
            2: invalidNumberRange('currentB', pvStringParameter.data[11] / 100),
          },
        },
        {
          ref: 4,
          noOfMppt: noOfMppt3,
          power:
            ((invalidNumberRange('voltageA', pvStringParameter.data[12] / 10) +
              invalidNumberRange('voltageB', pvStringParameter.data[14] / 10)) *
              (invalidNumberRange(
                'currentA',
                pvStringParameter.data[13] / 100
              ) +
                invalidNumberRange(
                  'currentB',
                  pvStringParameter.data[15] / 100
                ))) /
            2000,

          voltage: {
            1: invalidNumberRange('voltageA', pvStringParameter.data[12] / 10),
            2: invalidNumberRange('voltageB', pvStringParameter.data[14] / 10),
          },
          current: {
            1: invalidNumberRange('currentA', pvStringParameter.data[13] / 100),
            2: invalidNumberRange('currentB', pvStringParameter.data[15] / 100),
          },
        },
        {
          ref: 5,
          noOfMppt: noOfMppt4,
          power:
            ((invalidNumberRange('voltageA', pvStringParameter.data[16] / 10) +
              invalidNumberRange('voltageB', pvStringParameter.data[18] / 10)) *
              (invalidNumberRange(
                'currentA',
                pvStringParameter.data[17] / 100
              ) +
                invalidNumberRange(
                  'currentB',
                  pvStringParameter.data[19] / 100
                ))) /
            2000,

          voltage: {
            1: invalidNumberRange('voltageA', pvStringParameter.data[16] / 10),
            2: invalidNumberRange('voltageB', pvStringParameter.data[18] / 10),
          },
          current: {
            1: invalidNumberRange('currentA', pvStringParameter.data[17] / 100),
            2: invalidNumberRange('currentB', pvStringParameter.data[19] / 100),
          },
        },
        {
          ref: 6,
          noOfMppt: noOfMppt5,
          power:
            ((invalidNumberRange('voltageA', pvStringParameter.data[20] / 10) +
              invalidNumberRange('voltageB', pvStringParameter.data[22] / 10)) *
              (invalidNumberRange(
                'currentA',
                pvStringParameter.data[21] / 100
              ) +
                invalidNumberRange(
                  'currentB',
                  pvStringParameter.data[23] / 100
                ))) /
            2000,

          voltage: {
            1: invalidNumberRange('voltageA', pvStringParameter.data[20] / 10),
            2: invalidNumberRange('voltageB', pvStringParameter.data[22] / 10),
          },
          current: {
            1: invalidNumberRange('currentA', pvStringParameter.data[21] / 100),
            2: invalidNumberRange('currentB', pvStringParameter.data[23] / 100),
          },
        },
        {
          ref: 7,
          noOfMppt: noOfMppt6,
          power:
            ((invalidNumberRange('voltageA', pvStringParameter.data[24] / 10) +
              invalidNumberRange('voltageB', pvStringParameter.data[26] / 10)) *
              (invalidNumberRange(
                'currentA',
                pvStringParameter.data[25] / 100
              ) +
                invalidNumberRange(
                  'currentB',
                  pvStringParameter.data[27] / 100
                ))) /
            2000,

          voltage: {
            1: invalidNumberRange('voltageA', pvStringParameter.data[24] / 10),
            2: invalidNumberRange('voltageB', pvStringParameter.data[26] / 10),
          },
          current: {
            1: invalidNumberRange('currentA', pvStringParameter.data[25] / 100),
            2: invalidNumberRange('currentB', pvStringParameter.data[27] / 100),
          },
        },
        {
          ref: 8,
          noOfMppt: noOfMppt7,
          power:
            ((invalidNumberRange('voltageA', pvStringParameter.data[28] / 10) +
              invalidNumberRange('voltageB', pvStringParameter.data[30] / 10)) *
              (invalidNumberRange(
                'currentA',
                pvStringParameter.data[29] / 100
              ) +
                invalidNumberRange(
                  'currentB',
                  pvStringParameter.data[31] / 100
                ))) /
            2000,

          voltage: {
            1: invalidNumberRange('voltageA', pvStringParameter.data[28] / 10),
            2: invalidNumberRange('voltageB', pvStringParameter.data[30] / 10),
          },
          current: {
            1: invalidNumberRange('currentA', pvStringParameter.data[29] / 100),
            2: invalidNumberRange('currentB', pvStringParameter.data[31] / 100),
          },
        },
        {
          ref: 9,
          noOfMppt: noOfMppt8,
          power:
            ((invalidNumberRange('voltageA', pvStringParameter.data[32] / 10) +
              invalidNumberRange('voltageB', pvStringParameter.data[34] / 10)) *
              (invalidNumberRange(
                'currentA',
                pvStringParameter.data[33] / 100
              ) +
                invalidNumberRange(
                  'currentB',
                  pvStringParameter.data[35] / 100
                ))) /
            2000,

          voltage: {
            1: invalidNumberRange('voltageA', pvStringParameter.data[32] / 10),
            2: invalidNumberRange('voltageB', pvStringParameter.data[34] / 10),
          },
          current: {
            1: invalidNumberRange('currentA', pvStringParameter.data[33] / 100),
            2: invalidNumberRange('currentB', pvStringParameter.data[35] / 100),
          },
        },
        {
          ref: 10,
          noOfMppt: noOfMppt9,
          power:
            ((invalidNumberRange('voltageA', pvStringParameter.data[36] / 10) +
              invalidNumberRange('voltageB', pvStringParameter.data[38] / 10)) *
              (invalidNumberRange(
                'currentA',
                pvStringParameter.data[37] / 100
              ) +
                invalidNumberRange(
                  'currentB',
                  pvStringParameter.data[39] / 100
                ))) /
            2000,

          voltage: {
            1: invalidNumberRange('voltageA', pvStringParameter.data[36] / 10),
            2: invalidNumberRange('voltageB', pvStringParameter.data[38] / 10),
          },
          current: {
            1: invalidNumberRange('currentA', pvStringParameter.data[37] / 100),
            2: invalidNumberRange('currentB', pvStringParameter.data[39] / 100),
          },
        },
      ];

      // payload.pcVoltage = {
      //   1: pvStringParameter.data[0] / 10,
      //   2: pvStringParameter.data[2] / 10,
      //   3: pvStringParameter.data[4] / 10,
      //   4: pvStringParameter.data[6] / 10,
      //   5: pvStringParameter.data[8] / 10,
      //   6: pvStringParameter.data[10] / 10,
      //   7: pvStringParameter.data[12] / 10,
      //   8: pvStringParameter.data[14] / 10,
      //   9: pvStringParameter.data[16] / 10,
      //   10: pvStringParameter.data[18] / 10,
      //   11: pvStringParameter.data[20] / 10,
      //   12: pvStringParameter.data[22] / 10
      // };
      // payload.pvCurrent = {
      //   1: pvStringParameter.data[1] / 100,
      //   2: pvStringParameter.data[3] / 100,
      //   3: pvStringParameter.data[5] / 100,
      //   4: pvStringParameter.data[7] / 100,
      //   5: pvStringParameter.data[9] / 100,
      //   6: pvStringParameter.data[11] / 100,
      //   7: pvStringParameter.data[13] / 100,
      //   8: pvStringParameter.data[15] / 100,
      //   9: pvStringParameter.data[17] / 100,
      //   10: pvStringParameter.data[19] / 100,
      //   11: pvStringParameter.data[21] / 100,
      //   12: pvStringParameter.data[23] / 100
      // };
      payload.lastUpdate = this.moment().unix();

      if (!localCache.inverterReading) {
        localCache.inverterReading = {};
      }

      localCache.inverterReading[`${id}`] = payload;

      // await this.writeDataToLocal(
      //   `${this.appProperties.localFileStorageLocation.ensureDirectories.inverterReadingDataPath}/${id}.json`,
      //   payload
      // );
    } catch (e) {
      this.logger.error(
        `SUN2000_60KTL Error ====> get inverter data error on inverter number of ${this.args.id}`,
        e
      );
      localCache.errorSmartlogger = e;
      console.log('localCache.errorSmartlogger', localCache.errorSmartlogger);
    }
  }
}

module.exports = SUN2000_100KTL;
