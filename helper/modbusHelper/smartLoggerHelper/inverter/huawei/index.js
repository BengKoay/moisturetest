const SUN2000_12KTL = require('./SUN2000_12KTL');
const SUN2000_12KTL_2 = require('./SUN2000_12KTL_2');
const SUN2000_17KTL = require('./SUN2000_17KTL');
const SUN2000_20KTL = require('./SUN2000_20KTL');
const SUN2000_20KTL_2 = require('./SUN2000_20KTL_2');
const SUN2000_36KTL = require('./SUN2000_36KTL');
const SUN2000_60KTL = require('./SUN2000_60KTL');
const SUN2000_100KTL = require('./SUN2000_100KTL');

module.exports = {
  SUN2000_12KTL,
  SUN2000_12KTL_2,
  SUN2000_17KTL,
  SUN2000_20KTL,
  SUN2000_20KTL_2,
  SUN2000_36KTL,
  SUN2000_60KTL,
  SUN2000_100KTL,
};
