const Base = require('../base');
const piConfig = require('../../../../../piConfig').piConfig;
let localCache = require('../../../../localCache');
const alarmInverter = require('./alarmInverter');

class SUN2000_12KTL extends Base {
  constructor(modbusHelper, args, smartLoggerIndex) {
    super(modbusHelper, args, smartLoggerIndex);
  }

  async readData() {
    try {
      let { id } = this.args;
      let payload = {
        ref: id
      };
      await this.modbusHelper.setID(id);

      let sNumber = await this.modbusHelper.readHoldingRegisters(32003, 10);
      let serialNumber = sNumber.buffer.toString('utf8'); 
      var str = serialNumber;
      str = str.substring(6, str.length-8);
      let refSerialNumber = str;

      let inverter_data = await this.modbusHelper.readHoldingRegisters(
        32277,
        100
      );
      payload.refSn = refSerialNumber;
      payload.voltageA = inverter_data.buffer.readUInt16BE(0) / 100;
      payload.voltageB = inverter_data.buffer.readUInt16BE(2) / 100;
      payload.voltageC = inverter_data.buffer.readUInt16BE(4) / 100;
      payload.currentA = inverter_data.buffer.readUInt16BE(6) / 100;
      payload.currentB = inverter_data.buffer.readUInt16BE(8) / 100;
      payload.currentC = inverter_data.buffer.readUInt16BE(10) / 100;
      payload.frequency = inverter_data.buffer.readUInt16BE(12) / 100;
      payload.powerFactor = inverter_data.buffer.readInt16BE(14) / 1000;
      payload.effInverter = inverter_data.buffer.readUInt16BE(16) / 100;
      payload.temperature = inverter_data.buffer.readInt16BE(18) / 10;
      payload.status = inverter_data.buffer.readUInt16BE(20);
      payload.peakPower = inverter_data.buffer.readInt32BE(22) / 1000;
      payload.acPower = inverter_data.buffer.readInt32BE(26) / 1000;
      payload.reactivePower = inverter_data.buffer.readInt32BE(30) / 1000;
      payload.dcPower = inverter_data.buffer.readUInt32BE(34) / 1000;
      payload.energyHourly = inverter_data.buffer.readUInt32BE(42) / 100;
      payload.energyDaily = inverter_data.buffer.readUInt32BE(46) / 100;
      payload.energyMonthly = inverter_data.buffer.readUInt32BE(50) / 100;
      payload.energyTotal = inverter_data.buffer.readUInt32BE(58) / 100;
      payload.startup = inverter_data.buffer.readUInt32BE(96);
      payload.shutdown = inverter_data.buffer.readUInt32BE(100);
      payload.serialNumber = serialNumber;
      payload.panelEfficiency = piConfig.panelEfficiency;
      payload.arrayArea = piConfig.arrayArea;
      payload.dcCapacity = 12;
      let alarmNotification = await this.modbusHelper.readHoldingRegisters(
        50000,
        17
      );
      payload.alarmNotification1 = await alarmInverter.alarm(
        1,
        id,
        '12KTL',
        alarmNotification.buffer.readUInt16BE(0)
      );
      payload.alarmNotification2 = await alarmInverter.alarm(
        2,
        id,
        '12KTL',
        alarmNotification.buffer.readUInt16BE(2)
      );
      payload.alarmNotification3 = await alarmInverter.alarm(
        3,
        id,
        '12KTL',
        alarmNotification.buffer.readUInt16BE(4)
      );
      payload.alarmNotification4 = await alarmInverter.alarm(
        4,
        id,
        '12KTL',
        alarmNotification.buffer.readUInt16BE(6)
      );
      payload.alarmNotification5 = await alarmInverter.alarm(
        5,
        id,
        '12KTL',
        alarmNotification.buffer.readUInt16BE(8)
      );
      payload.alarmNotification6 = await alarmInverter.alarm(
        6,
        id,
        '12KTL',
        alarmNotification.buffer.readUInt16BE(10)
      );
      payload.alarmNotification7 = await alarmInverter.alarm(
        7,
        id,
        '12KTL',
        alarmNotification.buffer.readUInt16BE(12)
      );
      payload.alarmNotification8 = await alarmInverter.alarm(
        8,
        id,
        '12KTL',
        alarmNotification.buffer.readUInt16BE(14)
      );
      payload.alarmNotification9 = await alarmInverter.alarm(
        9,
        id,
        '12KTL',
        alarmNotification.buffer.readUInt16BE(16)
      );
      payload.alarmNotification16 = await alarmInverter.alarm(
        15,
        id,
        '12KTL',
        alarmNotification.buffer.readUInt16BE(30)
      );

      let mppt_values = await this.modbusHelper.readHoldingRegisters(33022, 6);
      payload.mpptPower = {
        1: mppt_values.buffer.readUInt32BE(0) / 1000,
        2: mppt_values.buffer.readUInt32BE(4) / 1000,
        3: mppt_values.buffer.readUInt32BE(8) / 1000
      };
      let pvStringParameter = await this.modbusHelper.readHoldingRegisters(
        32262,
        12
      );

      let slConfig = piConfig.smartloggerConfig.filter((obj) => {
        return obj.id === this.smartLoggerIndex;
      })[0];

      let inverterObj = slConfig.inverterConfig.filter((obj) => {
        return obj.id === id;
      })[0];

      let noOfMppt0 = 0;
      let noOfMppt1 = 0;
      let noOfMppt2 = 0;

      if (!!inverterObj && !!inverterObj.noOfMppt) {
        noOfMppt0 = inverterObj.noOfMppt[0] || 0;
        noOfMppt1 = inverterObj.noOfMppt[1] || 0;
        noOfMppt2 = inverterObj.noOfMppt[2] || 0;
      }

      payload.mpptPower = [
        {
          ref: 1,
          noOfMppt: noOfMppt0,
          power: mppt_values.buffer.readUInt32BE(0) / 1000,
          voltage: {
            1: pvStringParameter.data[0] / 10,
            2: pvStringParameter.data[2] / 10
          },
          current: {
            1: pvStringParameter.data[1] / 10,
            2: pvStringParameter.data[3] / 10
          }
        },
        {
          ref: 2,
          noOfMppt: noOfMppt1,
          power: mppt_values.buffer.readUInt32BE(4) / 1000,
          voltage: {
            1: pvStringParameter.data[4] / 10,
            2: pvStringParameter.data[6] / 10
          },
          current: {
            1: pvStringParameter.data[5] / 10,
            2: pvStringParameter.data[7] / 10
          }
        },
        {
          ref: 3,
          noOfMppt: noOfMppt2,
          power: mppt_values.buffer.readUInt32BE(8) / 1000,
          voltage: {
            1: pvStringParameter.data[8] / 10,
            2: pvStringParameter.data[10] / 10
          },
          current: {
            1: pvStringParameter.data[9] / 10,
            2: pvStringParameter.data[11] / 10
          }
        }
      ];

      // payload.pcVoltage = {
      //   1: pvStringParameter.data[0] / 10,
      //   2: pvStringParameter.data[2] / 10,
      //   3: pvStringParameter.data[4] / 10,
      //   4: pvStringParameter.data[6] / 10,
      //   5: pvStringParameter.data[8] / 10,
      //   6: pvStringParameter.data[10] / 10
      // };
      // payload.pvCurrent = {
      //   1: pvStringParameter.data[1] / 10,
      //   2: pvStringParameter.data[3] / 10,
      //   3: pvStringParameter.data[5] / 10,
      //   4: pvStringParameter.data[7] / 10,
      //   5: pvStringParameter.data[9] / 10,
      //   6: pvStringParameter.data[11] / 10
      // };
      payload.lastUpdate = this.moment().unix();

      if (!localCache.inverterReading) {
        localCache.inverterReading = {};
      }

      localCache.inverterReading[`${id}`] = payload;
      // await this.writeDataToLocal(
      //   `${this.appProperties.localFileStorageLocation.ensureDirectories.inverterReadingDataPath}/${id}.json`,
      //   payload
      // );
    } catch (e) {
      this.logger.error(
        `SUN2000_12KTL Error ====> get inverter data error on inverter number of ${this.args.id}`,
        e
      );
    }
  }
}

module.exports = SUN2000_12KTL;
