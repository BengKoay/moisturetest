const Base = require('../base');
const piConfig = require('../../../../../piConfig').piConfig;
let localCache = require('../../../../localCache');
const logger = require('../../../../loggerHelper').getLogger('log');
const moment = require('moment-timezone');

class SUN2000_20KTL_2 extends Base {
  constructor(modbusHelper, args, smartLoggerIndex) {
    super(modbusHelper, args, smartLoggerIndex);
  }

  async readData() {
    try {
      let { id } = this.args;
      let payload = {
        ref: id,
      };
      await this.modbusHelper.setID(id);

      let sNumber = await this.modbusHelper.readHoldingRegisters(40713, 10);
      let serialNumber = sNumber.buffer.toString('utf8');
      let refSerialNumber = serialNumber.substring(
        serialNumber.length - 6,
        serialNumber.length
      );
      let inverter_data = await this.modbusHelper.readHoldingRegisters(
        40525,
        10
      );

      payload.refSn = refSerialNumber;
      if ([12337, 12338, 12340, 12341].includes(sNumber.data[3])) {
        let refSerialNumber = serialNumber.substring(
          serialNumber.length - 14,
          serialNumber.length - 8
        );
        payload.refSn = refSerialNumber;
      }

      // payload.voltageA = inverter_data.buffer.readUInt16BE(0) / 100;
      // payload.voltageB = inverter_data.buffer.readUInt16BE(2) / 100;
      // payload.voltageC = inverter_data.buffer.readUInt16BE(4) / 100;
      // payload.currentA = inverter_data.buffer.readUInt16BE(6) / 100;
      // payload.currentB = inverter_data.buffer.readUInt16BE(8) / 100;
      // payload.currentC = inverter_data.buffer.readUInt16BE(10) / 100;
      // payload.frequency = inverter_data.buffer.readUInt16BE(12) / 100;
      // payload.powerFactor = inverter_data.buffer.readInt16BE(14) / 1000;
      payload.effInverter =
        (
          await this.modbusHelper.readHoldingRegisters(40685, 1)
        ).buffer.readUInt16BE() / 100;
      // payload.temperature = inverter_data.buffer.readInt16BE(18) / 10;
      // payload.status = inverter_data.buffer.readUInt16BE(20);
      // payload.peakPower = inverter_data.buffer.readInt32BE(22) / 1000;
      payload.acPower = inverter_data.buffer.readInt32BE(0) / 1000;
      // payload.reactivePower = inverter_data.buffer.readInt32BE(30) / 1000;
      payload.dcPower =
        (
          await this.modbusHelper.readHoldingRegisters(40692, 2)
        ).buffer.readUInt32BE() / 100;

      let databeforeFiftyNineMinutes = moment().minute() < 59;
      let energyHour = 0;

      if (moment().minute() === 59) {
        energyHour = 0;
      }
      if (databeforeFiftyNineMinutes) {
        energyHour += payload.acPower * (5 / 60) * 0.0167;
        payload.energyHourly = energyHour;
      }
      payload.energyDaily =
        (
          await this.modbusHelper.readHoldingRegisters(40562, 2)
        ).buffer.readUInt32BE() / 100;
      if (!payload.energyMonthly) payload.energyMonthly = 0;
      let endOfMonth = moment().endOf('month');
      if (moment() != endOfMonth) {
        payload.energyMonthly += payload.energyDaily;
      } else {
        payload.energyMonthly = 0;
      }
      payload.energyTotal = (await this.modbusHelper.readHoldingRegisters(
      40560,2)
      ).buffer.readUInt32BE() / 100;
      // payload.startup = inverter_data.buffer.readUInt32BE(96);
      // payload.shutdown = inverter_data.buffer.readUInt32BE(100);
      payload.serialNumber = serialNumber;
      payload.panelEfficiency = piConfig.panelEfficiency;
      payload.arrayArea = piConfig.arrayArea;

      let mppt_values = await this.modbusHelper.readHoldingRegisters(40686, 6);
      let pvStringParameter = await this.modbusHelper.readHoldingRegisters(
        40500,
        12
      );

      let slConfig = piConfig.smartloggerConfig.filter((obj) => {
        return obj.id === this.smartLoggerIndex;
      })[0];

      let inverterObj = slConfig.inverterConfig.filter((obj) => {
        return obj.id === id;
      })[0];

      let noOfMppt0 = 0;
      let noOfMppt1 = 0;
      let noOfMppt2 = 0;

      if (!!inverterObj && !!inverterObj.noOfMppt) {
        noOfMppt0 = inverterObj.noOfMppt[0] || 0;
        noOfMppt1 = inverterObj.noOfMppt[1] || 0;
        noOfMppt2 = inverterObj.noOfMppt[2] || 0;
      }

      payload.mpptPower = [
        {
          ref: 1,
          noOfMppt: noOfMppt0,
          power: mppt_values.buffer.readUInt32BE(0) / 1000,
          voltage: {
            1: pvStringParameter.data[0] / 10,
            2: pvStringParameter.data[1] / 10,
          },
          current: {
            1: pvStringParameter.data[6] / 100,
            2: pvStringParameter.data[7] / 100,
          },
        },
        {
          ref: 2,
          noOfMppt: noOfMppt1,
          power: mppt_values.buffer.readUInt32BE(4) / 1000,
          voltage: {
            1: pvStringParameter.data[2] / 10,
            2: pvStringParameter.data[3] / 10,
          },
          current: {
            1: pvStringParameter.data[8] / 100,
            2: pvStringParameter.data[9] / 100,
          },
        },
        {
          ref: 3,
          noOfMppt: noOfMppt2,
          power: mppt_values.buffer.readUInt32BE(8) / 1000,
          voltage: {
            1: pvStringParameter.data[4] / 10,
            2: pvStringParameter.data[5] / 10,
          },
          current: {
            1: pvStringParameter.data[10] / 100,
            2: pvStringParameter.data[11] / 100,
          },
        },
      ];

      // payload.pcVoltage = {
      //   1: pvStringParameter.data[0] / 10,
      //   2: pvStringParameter.data[2] / 10,
      //   3: pvStringParameter.data[4] / 10,
      //   4: pvStringParameter.data[6] / 10,
      //   5: pvStringParameter.data[8] / 10,
      //   6: pvStringParameter.data[10] / 10
      // };
      // payload.pvCurrent = {
      //   1: pvStringParameter.data[1] / 10,
      //   2: pvStringParameter.data[3] / 10,
      //   3: pvStringParameter.data[5] / 10,
      //   4: pvStringParameter.data[7] / 10,
      //   5: pvStringParameter.data[9] / 10,
      //   6: pvStringParameter.data[11] / 10
      // };
      payload.lastUpdate = this.moment().unix();

      if (!localCache.inverterReading) {
        localCache.inverterReading = {};
      }

      localCache.inverterReading[`${id}`] = payload;

      // await this.writeDataToLocal(
      //   `${this.appProperties.localFileStorageLocation.ensureDirectories.inverterReadingDataPath}/${id}.json`,
      //   payload
      // );
    } catch (e) {
      this.logger.error(
        `SUN2000_20KTL_2 Error ====> get inverter data error on inverter number of ${this.args.id}`,
        e
      );
    }
  }
}

module.exports = SUN2000_20KTL_2;
