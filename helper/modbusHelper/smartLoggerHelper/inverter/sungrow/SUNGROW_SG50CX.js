const Base = require('../base');
const piConfig = require('../../../../../piConfig').piConfig;
let localCache = require('../../../../localCache');
const alarmInverter = require('./alarmInverter');
const logger = require('../../../../loggerHelper').getLogger('log');
//const alarmInverter = require('./alarmInverter');
const moment = require('moment-timezone');



let energyHour = 0;
let energyMonthly = 0;

class SUNGROW_SG50CX extends Base {
  constructor(modbusHelper, args, smartLoggerIndex) {
    super(modbusHelper, args, smartLoggerIndex);
  }

  async readData() {
    try {
      let { id } = this.args;
      let payload = {
        ref: id,
      };
      await this.modbusHelper.setID(id);

      let sNumber = await this.modbusHelper.readInputRegisters(4990-1, 10);
      var buf = sNumber.buffer.slice(0,11)
      let serialNumber = buf.toString('utf8');
      let refSerialNumber = serialNumber.substring(
        serialNumber.length - 6,
        serialNumber.length
      );
      let inverter_data = await this.modbusHelper.readInputRegisters(
        5019-1,
        30
      );
      let inverter_data2 = await this.modbusHelper.readInputRegisters(
        5004-1,
        10
      );
      let inverter_data3 = await this.modbusHelper.readInputRegisters(
        5003-1,
        10
      );
      let inverter_data4 = await this.modbusHelper.readInputRegisters(
        5128-1,
        4
      );
      let invertertemp_data = await this.modbusHelper.readInputRegisters(
        5008-1,
        2
      );
      let inverterdcpower_data = await this.modbusHelper.readInputRegisters(
        5017-1,
        4
      );
      let inverter_nominalpower_data = await this.modbusHelper.readInputRegisters(
        5001-1,
        2
      );

      payload.refSn = refSerialNumber;
      payload.energyDaily = inverter_data3.buffer.readUInt16BE(0) / 10; //5003
      payload.voltageA = inverter_data.buffer.readUInt16BE(0) / 10; //5019
      payload.voltageB = inverter_data.buffer.readUInt16BE(2) / 10; //5020
      payload.voltageC = inverter_data.buffer.readUInt16BE(4) / 10; //5021
      payload.currentA = inverter_data.buffer.readUInt16BE(6) / 10; //5022
      payload.currentB = inverter_data.buffer.readUInt16BE(8) / 10; //5023
      payload.currentC = inverter_data.buffer.readUInt16BE(10) / 10; //5024
      payload.frequency = inverter_data.buffer.readUInt16BE(34) / 10; //=(5036-5019)*2
      payload.powerFactor = inverter_data.buffer.readInt16BE(32) / 100; // 5035
      //payload.effInverter = inverter_data.buffer.readUInt16BE(16) / 100;
      payload.temperature = invertertemp_data.buffer.readInt16BE(0) / 10; //5008
      //payload.status = inverter_data.buffer.readUInt16BE(20);
      //payload.peakPower = inverter_data.buffer.readInt32BE(22) / 1000;
      var buf = inverter_data.buffer.slice(24,28).swap16();
      payload.acPower = inverter_data.buffer.readInt32LE(24) / 1000; //5031
      var buf = inverter_data.buffer.slice(28,32).swap16();
      payload.reactivePower = inverter_data.buffer.readInt32LE(28) / 1000; //5033
      var buf = inverterdcpower_data.buffer.slice(0,4).swap16();
      payload.dcPower = inverterdcpower_data.buffer.readUInt32LE(0) / 1000; //5017
      let efficiency = (((inverter_data.buffer.readInt32LE(24) / 1000)/(inverterdcpower_data.buffer.readUInt32LE(0) / 1000))*100).toFixed(2);

     if (efficiency >= 0)
     {
       payload.effInverter = (((inverter_data.buffer.readInt32LE(24) / 1000)/(inverterdcpower_data.buffer.readUInt32LE(0) / 1000))*100).toFixed(2);
      }
      if(isNaN(efficiency))
      {
        payload.effInverter = 0;
      }

      let databeforeFiftyNineMinutes = moment().minute() < 59;
     

      if (moment().minute() === 59) {
        energyHour = 0;
      }

      if (databeforeFiftyNineMinutes) {
        energyHour += payload.acPower * (5 / 60) * 0.0167;
        payload.energyHourly = energyHour;
      }

      //let databeforeTomorrow = moment().hour() !== '00';
      
/*
      if (moment().hour() === '00' && moment().minute() === 1) {
        energyDaily = 0;
        
      }
      if (databeforeFiftyNineMinutes && databeforeTomorrow) {
        energyDaily += payload.acPower * (5 / 60) * 0.0167;
        payload.energyDaily = energyDaily;
      }
*/
/*
      let thisMonth = moment().month();
      let yesterdayMonth = moment().subtract(1, 'days').month();
      let countMonth = thisMonth - yesterdayMonth;
      

      if (countMonth !== 0){
      energyMonthly = 0;
      }
      if (countMonth === 0){
        energyMonthly += payload.acPower * (5 / 60) * 0.0167;
        payload.energyMonthly = energyMonthly;
      }
*/    

      //payload.energyHourly = inverter_data.buffer.readUInt32BE(42) / 100;
      //payload.energyDaily = inverter_data.buffer.readUInt32BE(46) / 100;

      // payload.energyMonthly = inverter_data.buffer.readUInt32BE(50) / 100;
      var buf = inverter_data4.buffer.slice(0,4).swap16();
      payload.energyMonthly = inverter_data4.buffer.readUInt32LE(0)/10 ; //5004
      var buf = inverter_data2.buffer.slice(0,4).swap16();
      payload.energyTotal = inverter_data2.buffer.readUInt32LE(0) ; //5004
      //payload.startup = inverter_data.buffer.readUInt32BE(96);
      //payload.shutdown = inverter_data.buffer.readUInt32BE(100);
      payload.serialNumber = serialNumber;
      payload.panelEfficiency = piConfig.panelEfficiency;
      payload.arrayArea = piConfig.arrayArea;
      payload.dcCapacity =
        inverter_nominalpower_data.buffer.readUInt16BE(0) / 10; //5001

        let alarmNotification = await this.modbusHelper.readInputRegisters(
          5045-1,
          6
        );

        payload.alarmNotification1 = await alarmInverter.alarm(
          1,
          id,
          '5KTL',
          alarmNotification.buffer.readUInt16BE(0)
        );

      /*
      let alarmNotification = await this.modbusHelper.readHoldingRegisters(
        50000,
        17
      );
      payload.alarmNotification1 = await alarmInverter.alarm(
        1,
        id,
        '36KTL',
        alarmNotification.buffer.readUInt16BE(0)
      );
      payload.alarmNotification2 = await alarmInverter.alarm(
        2,
        id,
        '36KTL',
        alarmNotification.buffer.readUInt16BE(2)
      );
      payload.alarmNotification3 = await alarmInverter.alarm(
        3,
        id,
        '36KTL',
        alarmNotification.buffer.readUInt16BE(4)
      );
      payload.alarmNotification4 = await alarmInverter.alarm(
        4,
        id,
        '36KTL',
        alarmNotification.buffer.readUInt16BE(6)
      );
      payload.alarmNotification5 = await alarmInverter.alarm(
        5,
        id,
        '36KTL',
        alarmNotification.buffer.readUInt16BE(8)
      );
      payload.alarmNotification6 = await alarmInverter.alarm(
        6,
        id,
        '36KTL',
        alarmNotification.buffer.readUInt16BE(10)
      );
      payload.alarmNotification7 = await alarmInverter.alarm(
        7,
        id,
        '36KTL',
        alarmNotification.buffer.readUInt16BE(12)
      );
      payload.alarmNotification8 = await alarmInverter.alarm(
        8,
        id,
        '36KTL',
        alarmNotification.buffer.readUInt16BE(14)
      );
      payload.alarmNotification9 = await alarmInverter.alarm(
        9,
        id,
        '36KTL',
        alarmNotification.buffer.readUInt16BE(16)
      );
      payload.alarmNotification16 = await alarmInverter.alarm(
        15,
        id,
        '36KTL',
        alarmNotification.buffer.readUInt16BE(30)
      );
*/
      //let mppt_values = await this.modbusHelper.readHoldingRegisters(33022, 50);
      let pvStringParameter = await this.modbusHelper.readInputRegisters(
        5011-1,
        50
      );

      const filterMPPTVoltage0 = (key, value) => {
        if (['noOfMppt'].includes(key)) {
          if (value >= 21 || value <= -42) {
          
            return pvStringParameter.buffer.readUInt16BE(2) / 10;
          }
        }
        return 0;
      };

      const filterMPPTCurrent0 = (key, value) => {
        if (['noOfMppt'].includes(key)) {
          if (value >= 21 || value <= -42) {
          
            return pvStringParameter.buffer.readUInt16BE(2) / 10;
          }
        }
        return 0;
      };

      
     

      //let mppt_values2 = await this.modbusHelper.readHoldingRegisters(33022, 4);
      let pvStringParameter2 = await this.modbusHelper.readInputRegisters(
        5115-1,
        50
      );

      let slConfig = piConfig.smartloggerConfig.filter((obj) => {
        return obj.id === this.smartLoggerIndex;
      })[0];

      let inverterObj = slConfig.inverterConfig.filter((obj) => {
        return obj.id === id;
      })[0];

      let noOfMppt0 = 0;
      let noOfMppt1 = 0;
      let noOfMppt2 = 0;
      let noOfMppt3 = 0;

      if (!!inverterObj && !!inverterObj.noOfMppt) {
        noOfMppt0 = inverterObj.noOfMppt[0] || 0;
        noOfMppt1 = inverterObj.noOfMppt[1] || 0;
        noOfMppt2 = inverterObj.noOfMppt[2] || 0;
        noOfMppt3 = inverterObj.noOfMppt[3] || 0;
        noOfMppt4 = inverterObj.noOfMppt[4] || 0;
        noOfMppt5 = inverterObj.noOfMppt[5] || 0;
        noOfMppt6 = inverterObj.noOfMppt[6] || 0;
        noOfMppt7 = inverterObj.noOfMppt[7] || 0;
        noOfMppt8 = inverterObj.noOfMppt[8] || 0;
      }
//////////////////////////////////////////////////////////////

                   //MPPT SCRIPT 1- no string info//
 
//////////////////////////////////////////////////////////////


      let MPPT0 = noOfMppt0;
      payload.mpptPower = [
        
      
        {
          ref: 1,
          noOfMppt: noOfMppt0,
         
          voltage: {
            
            1: pvStringParameter.buffer.readUInt16BE(0) / 10,
            2: pvStringParameter.buffer.readUInt16BE(0) / 10,
          },
          current: {
            1: 0,
            2: pvStringParameter.buffer.readUInt16BE(2) / 10,
          },
          power:
           (pvStringParameter.buffer.readUInt16BE(0) /
            10 *
            (pvStringParameter.buffer.readUInt16BE(2) / 10))/1000,
        },
        {
          ref: 2,
          noOfMppt: noOfMppt1,
          voltage: {
            1: pvStringParameter.buffer.readUInt16BE(4) / 10,
            2: pvStringParameter.buffer.readUInt16BE(4) / 10,
          },
          current: {
            1: 0,
            2: pvStringParameter.buffer.readUInt16BE(6) / 10,
          },
          power:
            (pvStringParameter.buffer.readUInt16BE(4) /
            10 *
            (pvStringParameter.buffer.readUInt16BE(6) / 10))/1000,
        },
        
        {
          ref: 3,
          noOfMppt: noOfMppt2,
          voltage: {
            1: pvStringParameter.data.buffer.readUInt16BE(8) * 10,
            2: pvStringParameter.data.buffer.readUInt16BE(8) * 10,
          },
          current: {
            1: 0,
            2: pvStringParameter.data.buffer.readUInt16BE(10) * 10,
          },
          power:
          (pvStringParameter.buffer.readUInt16BE(8) /
          10 *
          (pvStringParameter.buffer.readUInt16BE(10) / 10))/1000,
        },
        {
          ref: 4,
          noOfMppt: noOfMppt3,
          voltage: {
            1: pvStringParameter2.data.buffer.readUInt16BE(0) * 10,
            2: pvStringParameter2.data.buffer.readUInt16BE(0) * 10,
          },
          current: {
            1: 0,
            2: pvStringParameter2.data.buffer.readUInt16BE(2) * 10,
          },
          power:
          (pvStringParameter.buffer.readUInt16BE(0) /
          10 *
          (pvStringParameter.buffer.readUInt16BE(2) / 10))/1000,
        },
        {
          ref: 5,
          noOfMppt: noOfMppt4,
          voltage: {
            1: pvStringParameter2.data.buffer.readUInt16BE(4) * 10,
            2: pvStringParameter2.data.buffer.readUInt16BE(4) * 10,
          },
          current: {
            1: 0,
            2: pvStringParameter2.data.buffer.readUInt16BE(6) * 10,
          },
          power:
          (pvStringParameter.buffer.readUInt16BE(4) /
          10 *
          (pvStringParameter.buffer.readUInt16BE(6) / 10))/1000,
        },
        {
          ref: 6,
          noOfMppt: noOfMppt5,
          voltage: {
            1: pvStringParameter2.data.buffer.readUInt16BE(8) * 10,
            2: pvStringParameter2.data.buffer.readUInt16BE(8) * 10,
          },
          current: {
            1: 0,
            2: pvStringParameter2.data.buffer.readUInt16BE(10) * 10,
          },
          power:
          (pvStringParameter.buffer.readUInt16BE(8) /
          10 *
          (pvStringParameter.buffer.readUInt16BE(10) / 10))/1000,
        },
        {
          ref: 7,
          noOfMppt: noOfMppt6,
          voltage: {
            1: pvStringParameter2.data.buffer.readUInt16BE(12) * 10,
            2: pvStringParameter2.data.buffer.readUInt16BE(12) * 10,
          },
          current: {
            1: 0,
            2: pvStringParameter2.data.buffer.readUInt16BE(14) * 10,
          },
          power:
          (pvStringParameter.buffer.readUInt16BE(12) /
          10 *
          (pvStringParameter.buffer.readUInt16BE(14) / 10))/1000,
        },
        {
          ref: 8,
          noOfMppt: noOfMppt7,
          voltage: {
            1: pvStringParameter2.data.buffer.readUInt16BE(16) * 10,
            2: pvStringParameter2.data.buffer.readUInt16BE(16) * 10,
          },
          current: {
            1: 0,
            2: pvStringParameter2.data.buffer.readUInt16BE(18) * 10,
          },
          power:
          (pvStringParameter.buffer.readUInt16BE(16) /
          10 *
          (pvStringParameter.buffer.readUInt16BE(18) / 10))/1000,
        },
        {
            ref: 9,
            noOfMppt: noOfMppt8,
            voltage: {
              1: pvStringParameter2.data.buffer.readUInt16BE(30) * 10,
              2: pvStringParameter2.data.buffer.readUInt16BE(30) * 10,
            },
            current: {
              1: 0,
              2: pvStringParameter2.data.buffer.readUInt16BE(32) * 10,
            },
            power:
            (pvStringParameter.buffer.readUInt16BE(30) /
            10 *
            (pvStringParameter.buffer.readUInt16BE(32) / 10))/1000,
          },
      ];


      // payload.pcVoltage = {
      //   1: pvStringParameter.data[0] / 10,
      //   2: pvStringParameter.data[2] / 10,
      //   3: pvStringParameter.data[4] / 10,
      //   4: pvStringParameter.data[6] / 10,
      //   5: pvStringParameter.data[8] / 10,
      //   6: pvStringParameter.data[10] / 10
      // };
      // payload.pvCurrent = {
      //   1: pvStringParameter.data[1] / 10,
      //   2: pvStringParameter.data[3] / 10,
      //   3: pvStringParameter.data[5] / 10,
      //   4: pvStringParameter.data[7] / 10,
      //   5: pvStringParameter.data[9] / 10,
      //   6: pvStringParameter.data[11] / 10
      // };
      /*if (
        sNumber.data[4] === 13104 &&
        sNumber.data[5] === 12851 &&
        !!sNumber.data[6] &&
        !!sNumber.data[7] &&
        !!sNumber.data[8] &&
        sNumber.data[9] != 0
      ) {
        let pvStringParameter2 = await this.modbusHelper.readHoldingRegisters(
          32314,
          4
        );

        payload.pcVoltage['7'] = pvStringParameter2.data[0] / 10;
        payload.pcVoltage['8'] = pvStringParameter2.data[2] / 10;

        payload.pvCurrent['7'] = pvStringParameter2.data[1] / 10;
        payload.pvCurrent['8'] = pvStringParameter2.data[3] / 10;
      }*/
      payload.lastUpdate = this.moment().unix();

      if (!localCache.inverterReading) {
        localCache.inverterReading = {};
      }

      localCache.inverterReading[`${id}`] = payload;

      // await this.writeDataToLocal(
      //   `${this.appProperties.localFileStorageLocation.ensureDirectories.inverterReadingDataPath}/${id}.json`,
      //   payload
      // );
    } catch (e) {
      this.logger.error(
        `SUNGROW Error ====> get inverter data error on inverter number of ${this.args.id}`,
        e
      );
    }
  }
  
}

module.exports = SUNGROW_SG50CX;
