const localCache = require('../../../../localCache');

const alarm = async (type, inverterId, model, number) => {
    if (number > 1) {
      await errorCheck(type, inverterId, model, number);
      return number;
    }
  
  return 0;
};

const errorCheck = async (type, inverterId, model, number) => {
  // severity value 1 = warning, 2 = minor, 3 = major
  let error = {
    inverterID: '',
    alarmName: '',
    alarmID: '',
    severity: '',
    code: '',
  };
  // inverter SG33CX, SG50CX, SG110CX
  if (['5KTL', 'SG33CX', 'SG50CX', 'SG110CX'].includes(model)) {
    // alarm 1
    if (type === 1) {
      switch (number) {
        case 2:
          error.inverterID = inverterId;
          error.alarmName = ' Grid overvoltage';
          error.alarmID = '002';
          error.severity = '3';

          break;
        case 3:
          error.inverterID = inverterId;
          error.alarmName = ' Grid transient overvoltage ';
          error.alarmID = '003';
          error.severity = '3';

          break;
        case 4:
          error.inverterID = inverterId;
          error.alarmName = ' Grid undervoltage';
          error.alarmID = '004';
          error.severity = '3';

          break;
        case 5:
          error.inverterID = inverterId;
          error.alarmName = ' Grid low voltage';
          error.alarmID = '005';
          error.severity = '3';

          break;
        case 7:
          error.inverterID = inverterId;
          error.alarmName = ' AC instantaneous overcurrent';
          error.alarmID = '007';
          error.severity = '3';

          break;
        case 8:
          error.inverterID = inverterId;
          error.alarmName = ' Grid overfrequency';
          error.alarmID = '008';
          error.severity = '3';

          break;
        case 9:
          error.inverterID = inverterId;
          error.alarmName = ' Grid underfrequency';
          error.alarmID = '009';
          error.severity = '3';

          break;
        case 10:
          error.inverterID = inverterId;
          error.alarmName = ' Grid power outage';
          error.alarmID = '010';
          error.severity = '3';

          break;
        case 11:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '011';
          error.severity = '3';

          break;
        case 12:
          error.inverterID = inverterId;
          error.alarmName = ' Excessive leakage current';
          error.alarmID = '012';
          error.severity = '3';

          break;

          case 13:
          error.inverterID = inverterId;
          error.alarmName = ' Grid abnormal';
          error.alarmID = '013';
          error.severity = '3';

          break;
          case 14:
          error.inverterID = inverterId;
          error.alarmName = ' 10-minute grid overvoltage';
          error.alarmID = '014';
          error.severity = '3';

          break;
          case 15:
          error.inverterID = inverterId;
          error.alarmName = ' Grid high voltage';
          error.alarmID = '015';
          error.severity = '3';

          break;
          case 16:
          error.inverterID = inverterId;
          error.alarmName = ' Output overload';
          error.alarmID = '016';
          error.severity = '3';

          break;
          case 17:
          error.inverterID = inverterId;
          error.alarmName = ' Grid voltage unbalance';
          error.alarmID = '017';
          error.severity = '3';

          break;
          case 19:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '019';
          error.severity = '3';

          break;
          case 20:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '020';
          error.severity = '3';

          break;
          case 21:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '021';
          error.severity = '3';

          break;
          case 22:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '022';
          error.severity = '3';

          break;
          case 23:
          error.inverterID = inverterId;
          error.alarmName = ' PV connection fault';
          error.alarmID = '023';
          error.severity = '3';

          break;
          case 24:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '024';
          error.severity = '3';

          break;
          case 25:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '025';
          error.severity = '3';

          break;
          case 30:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '030';
          error.severity = '3';

          break;
          case 31:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '031';
          error.severity = '3';

          break;
          case 32:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '032';
          error.severity = '3';

          break;
          case 33:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '033';
          error.severity = '3';

          break;
          case 34:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '034';
          error.severity = '3';

          break;
          case 36:
          error.inverterID = inverterId;
          error.alarmName = ' Excessively high module temperature';
          error.alarmID = '036';
          error.severity = '3';

          break;
          case 37:
          error.inverterID = inverterId;
          error.alarmName = ' Excessively high ambient temperature';
          error.alarmID = '037';
          error.severity = '3';

          break;
          case 38:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '038';
          error.severity = '3';

          break;
          case 39:
          error.inverterID = inverterId;
          error.alarmName = ' Low system insulation resistance';
          error.alarmID = '039';
          error.severity = '3';

          break;
          case 40:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '040';
          error.severity = '3';

          break;
          case 41:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '041';
          error.severity = '3';

          break;
          case 42:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '042';
          error.severity = '3';

          break;
          case 43:
          error.inverterID = inverterId;
          error.alarmName = ' Low ambient temperature';
          error.alarmID = '043';
          error.severity = '3';

          break;
          case 44:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '044';
          error.severity = '3';

          break;
          case 45:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '045';
          error.severity = '3';

          break;
          case 46:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '046';
          error.severity = '3';

          break;
          case 47:
          error.inverterID = inverterId;
          error.alarmName = ' PV input configuration abnormal';
          error.alarmID = '047';
          error.severity = '3';

          break;
          case 48:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '048';
          error.severity = '3';

          

          break;
          case 49:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '049';
          error.severity = '3';

          break;
          case 50:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '050';
          error.severity = '3';

          break;
          case 53:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '053';
          error.severity = '3';

          break;
          case 54:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '054';
          error.severity = '3';

          break;
          case 55:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '055';
          error.severity = '3';

          break;
          case 56:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '056';
          error.severity = '3';

          break;
          case 59:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '059';
          error.severity = '1';

          break;
          case 60:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '060';
          error.severity = '3';

          break;
          case 70:
          error.inverterID = inverterId;
          error.alarmName = ' Fan alarm';
          error.alarmID = '070';
          error.severity = '1';

          break;
          case 71:
          error.inverterID = inverterId;
          error.alarmName = ' AC-side SPD alarm';
          error.alarmID = '071';
          error.severity = '1';

          break;
          case 72:
          error.inverterID = inverterId;
          error.alarmName = ' DC-side SPD alarm';
          error.alarmID = '072';
          error.severity = '1';

          break;
          case 74:
          error.inverterID = inverterId;
          error.alarmName = ' Communication alarm';
          error.alarmID = '074';
          error.severity = '1';

          break;
          case 76:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '076';
          error.severity = '1';

          break;
          case 78:
          error.inverterID = inverterId;
          error.alarmName = ' PV1 string abnormal';
          error.alarmID = '078';
          error.severity = '1';

          break;
          case 79:
          error.inverterID = inverterId;
          error.alarmName = ' PV2 string abnormal';
          error.alarmID = '079';
          error.severity = '1';

          break;
          case 80:
          error.inverterID = inverterId;
          error.alarmName = ' PV3 string abnormal';
          error.alarmID = '080';
          error.severity = '1';

          break;

          case 81:
          error.inverterID = inverterId;
          error.alarmName = ' PV4 string abnormal';
          error.alarmID = '081';
          error.severity = '1';

          break;
          case 87:
          error.inverterID = inverterId;
          error.alarmName = ' Electric arc detection module abnormal';
          error.alarmID = '087';
          error.severity = '1';

          break;
          case 88:
          error.inverterID = inverterId;
          error.alarmName = ' Electric arc fault';
          error.alarmID = '088';
          error.severity = '3';

          break;
          case 89:
          error.inverterID = inverterId;
          error.alarmName = ' Electric arc detection disable';
          error.alarmID = '089';
          error.severity = '1';

          break;
          case 105:
          error.inverterID = inverterId;
          error.alarmName = ' Grid-side protection self-check failure';
          error.alarmID = '105';
          error.severity = '3';

          break;
          case 106:
          error.inverterID = inverterId;
          error.alarmName = ' Grounding cable fault';
          error.alarmID = '106';
          error.severity = '3';

          break;
          case 116:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '116';
          error.severity = '3';

          break;
          case 117:
          error.inverterID = inverterId;
          error.alarmName = ' Device abnormal';
          error.alarmID = '117';
          error.severity = '3';

          break;
          case 220:
          error.inverterID = inverterId;
          error.alarmName = ' PV5 abnormal';
          error.alarmID = '220';
          error.severity = '1';

          break;
          case 221:
          error.inverterID = inverterId;
          error.alarmName = ' PV6 abnormal';
          error.alarmID = '221';
          error.severity = '1';

          break;
          case 222:
          error.inverterID = inverterId;
          error.alarmName = ' PV7 abnormal';
          error.alarmID = '222';
          error.severity = '1';

          break;
          case 223:
          error.inverterID = inverterId;
          error.alarmName = ' PV8 abnormal';
          error.alarmID = '223';
          error.severity = '1';

          break;
          case 224:
          error.inverterID = inverterId;
          error.alarmName = ' PV9 abnormal';
          error.alarmID = '224';
          error.severity = '1';

          break;
          case 225:
          error.inverterID = inverterId;
          error.alarmName = ' PV10 abnormal';
          error.alarmID = '225';
          error.severity = '1';

          break;
          case 226:
          error.inverterID = inverterId;
          error.alarmName = ' PV11 abnormal';
          error.alarmID = '226';
          error.severity = '1';

          break;
          case 227:
          error.inverterID = inverterId;
          error.alarmName = ' PV12 abnormal';
          error.alarmID = '227';
          error.severity = '1';

          break;
          case 514:
          error.inverterID = inverterId;
          error.alarmName = ' Meter communication abnormal alarm ';
          error.alarmID = '514';
          error.severity = '1';

          break;
          case 532:
          error.inverterID = inverterId;
          error.alarmName = ' String 1 reverse connection alarm';
          error.alarmID = '532';
          error.severity = '1';

          break;
          case 533:
          error.inverterID = inverterId;
          error.alarmName = ' String 2 reverse connection alarm';
          error.alarmID = '533';
          error.severity = '1';

          break;
          case 534:
          error.inverterID = inverterId;
          error.alarmName = ' String 3 reverse connection alarm';
          error.alarmID = '534';
          error.severity = '1';

          break;
          case 535:
          error.inverterID = inverterId;
          error.alarmName = ' String 4 reverse connection alarm';
          error.alarmID = '535';
          error.severity = '1';

          break;
          case 536:
          error.inverterID = inverterId;
          error.alarmName = ' String 5 reverse connection alarm';
          error.alarmID = '536';
          error.severity = '1';

          break;
          case 537:
          error.inverterID = inverterId;
          error.alarmName = ' String 6 reverse connection alarm';
          error.alarmID = '537';
          error.severity = '1';

          break;
          case 538:
          error.inverterID = inverterId;
          error.alarmName = ' String 7 reverse connection alarm';
          error.alarmID = '538';
          error.severity = '1';

          break;
          case 539:
          error.inverterID = inverterId;
          error.alarmName = ' String 8 reverse connection alarm';
          error.alarmID = '539';
          error.severity = '1';

          break;
          case 540:
          error.inverterID = inverterId;
          error.alarmName = ' String 9 reverse connection alarm';
          error.alarmID = '540';
          error.severity = '1';

          break;
          case 541:
          error.inverterID = inverterId;
          error.alarmName = ' String 10 reverse connection alarm';
          error.alarmID = '541';
          error.severity = '1';

          break;
          case 542:
          error.inverterID = inverterId;
          error.alarmName = ' String 11 reverse connection alarm';
          error.alarmID = '542';
          error.severity = '1';

          break;
          case 543:
          error.inverterID = inverterId;
          error.alarmName = ' String 12 reverse connection alarm';
          error.alarmID = '543';
          error.severity = '1';

          break;
          case 544:
          error.inverterID = inverterId;
          error.alarmName = ' String 13 reverse connection alarm';
          error.alarmID = '544';
          error.severity = '1';

          break;
          case 545:
          error.inverterID = inverterId;
          error.alarmName = ' String 14 reverse connection alarm';
          error.alarmID = '545';
          error.severity = '1';

          break;
          case 546:
          error.inverterID = inverterId;
          error.alarmName = ' String 15 reverse connection alarm';
          error.alarmID = '546';
          error.severity = '1';

          break;
          case 547:
          error.inverterID = inverterId;
          error.alarmName = ' String 16 reverse connection alarm';
          error.alarmID = '547';
          error.severity = '1';

          break;
          case 564:
          error.inverterID = inverterId;
          error.alarmName = ' String 17 reverse connection alarm';
          error.alarmID = '564';
          error.severity = '1';

          break;
          case 565:
          error.inverterID = inverterId;
          error.alarmName = ' String 18 reverse connection alarm';
          error.alarmID = '565';
          error.severity = '1';

          break;
          case 566:
          error.inverterID = inverterId;
          error.alarmName = ' String 19 reverse connection alarm';
          error.alarmID = '566';
          error.severity = '1';

          break;
          case 567:
          error.inverterID = inverterId;
          error.alarmName = ' String 20 reverse connection alarm';
          error.alarmID = '567';
          error.severity = '1';

          break;
          case 568:
          error.inverterID = inverterId;
          error.alarmName = ' String 21 reverse connection alarm';
          error.alarmID = '568';
          error.severity = '1';

          break;
          case 569:
          error.inverterID = inverterId;
          error.alarmName = ' String 22 reverse connection alarm';
          error.alarmID = '569';
          error.severity = '1';

          break;
          case 570:
          error.inverterID = inverterId;
          error.alarmName = ' String 23 reverse connection alarm';
          error.alarmID = '570';
          error.severity = '1';

          break;
          case 571:
          error.inverterID = inverterId;
          error.alarmName = ' String 24 reverse connection alarm';
          error.alarmID = '571';
          error.severity = '1';

          break;
          case 548:
          error.inverterID = inverterId;
          error.alarmName = ' String 1 abnormal current alarm';
          error.alarmID = '548';
          error.severity = '1';

          break;
          case 549:
          error.inverterID = inverterId;
          error.alarmName = ' String 2 abnormal current alarm';
          error.alarmID = '549';
          error.severity = '1';

          break;
          case 550:
          error.inverterID = inverterId;
          error.alarmName = ' String 3 abnormal current alarm';
          error.alarmID = '550';
          error.severity = '1';

          break;
          case 551:
          error.inverterID = inverterId;
          error.alarmName = ' String 4 abnormal current alarm';
          error.alarmID = '551';
          error.severity = '1';

          break;
          case 552:
          error.inverterID = inverterId;
          error.alarmName = ' String 5 abnormal current alarm';
          error.alarmID = '552';
          error.severity = '1';

          break;
          case 553:
          error.inverterID = inverterId;
          error.alarmName = ' String 6 abnormal current alarm';
          error.alarmID = '553';
          error.severity = '1';

          break;
          case 554:
          error.inverterID = inverterId;
          error.alarmName = ' String 7 abnormal current alarm';
          error.alarmID = '554';
          error.severity = '1';

          break;
          case 555:
          error.inverterID = inverterId;
          error.alarmName = ' String 8 abnormal current alarm';
          error.alarmID = '555';
          error.severity = '1';

          break;
          case 556:
          error.inverterID = inverterId;
          error.alarmName = ' String 9 abnormal current alarm';
          error.alarmID = '556';
          error.severity = '1';

          break;
          case 557:
          error.inverterID = inverterId;
          error.alarmName = ' String 10 abnormal current alarm';
          error.alarmID = '557';
          error.severity = '1';

          break;
          case 558:
          error.inverterID = inverterId;
          error.alarmName = ' String 11 abnormal current alarm';
          error.alarmID = '558';
          error.severity = '1';

          break;
          case 559:
          error.inverterID = inverterId;
          error.alarmName = ' String 12 abnormal current alarm';
          error.alarmID = '559';
          error.severity = '1';

          break;
          case 560:
          error.inverterID = inverterId;
          error.alarmName = ' String 13 abnormal current alarm';
          error.alarmID = '560';
          error.severity = '1';

          break;
          case 561:
          error.inverterID = inverterId;
          error.alarmName = ' String 14 abnormal current alarm';
          error.alarmID = '561';
          error.severity = '1';

          break;
          case 562:
          error.inverterID = inverterId;
          error.alarmName = ' String 15 abnormal current alarm';
          error.alarmID = '562';
          error.severity = '1';

          break;
          case 563:
          error.inverterID = inverterId;
          error.alarmName = ' String 16 abnormal current alarm';
          error.alarmID = '563';
          error.severity = '1';

          break;
          case 580:
          error.inverterID = inverterId;
          error.alarmName = ' String 17 abnormal current alarm';
          error.alarmID = '580';
          error.severity = '1';

          break;
          case 581:
          error.inverterID = inverterId;
          error.alarmName = ' String 18 abnormal current alarm';
          error.alarmID = '581';
          error.severity = '1';

          break;
          case 582:
          error.inverterID = inverterId;
          error.alarmName = ' String 19 abnormal current alarm';
          error.alarmID = '582';
          error.severity = '1';

          break;
          case 583:
          error.inverterID = inverterId;
          error.alarmName = ' String 20 abnormal current alarm';
          error.alarmID = '583';
          error.severity = '1';

          break;
          case 584:
          error.inverterID = inverterId;
          error.alarmName = ' String 21 abnormal current alarm';
          error.alarmID = '584';
          error.severity = '1';

          break;
          case 585:
          error.inverterID = inverterId;
          error.alarmName = ' String 22 abnormal current alarm';
          error.alarmID = '585';
          error.severity = '1';

          break;
          case 586:
          error.inverterID = inverterId;
          error.alarmName = ' String 23 abnormal current alarm';
          error.alarmID = '586';
          error.severity = '1';

          break;
          case 587:
          error.inverterID = inverterId;
          error.alarmName = ' String 24 abnormal current alarm';
          error.alarmID = '587';
          error.severity = '1';

          break;
          case 448:
          error.inverterID = inverterId;
          error.alarmName = ' String 1 reverse connection fault';
          error.alarmID = '448';
          error.severity = '3';

          break;
          case 449:
          error.inverterID = inverterId;
          error.alarmName = ' String 2 reverse connection fault';
          error.alarmID = '449';
          error.severity = '3';

          break;
          case 450:
          error.inverterID = inverterId;
          error.alarmName = ' String 3 reverse connection fault';
          error.alarmID = '450';
          error.severity = '3';

          break;
          case 451:
          error.inverterID = inverterId;
          error.alarmName = ' String 4 reverse connection fault';
          error.alarmID = '451';
          error.severity = '3';

          break;
          case 452:
          error.inverterID = inverterId;
          error.alarmName = ' String 5 reverse connection fault';
          error.alarmID = '452';
          error.severity = '3';

          break;
          case 453:
          error.inverterID = inverterId;
          error.alarmName = ' String 6 reverse connection fault';
          error.alarmID = '453';
          error.severity = '3';

          break;
          case 454:
          error.inverterID = inverterId;
          error.alarmName = ' String 7 reverse connection fault';
          error.alarmID = '454';
          error.severity = '3';

          break;
          case 455:
          error.inverterID = inverterId;
          error.alarmName = ' String 8 reverse connection fault';
          error.alarmID = '455';
          error.severity = '3';

          break;
          case 456:
          error.inverterID = inverterId;
          error.alarmName = ' String 9 reverse connection fault';
          error.alarmID = '456';
          error.severity = '3';

          break;
          case 457:
          error.inverterID = inverterId;
          error.alarmName = ' String 10 reverse connection fault';
          error.alarmID = '457';
          error.severity = '3';

          break;
          case 458:
          error.inverterID = inverterId;
          error.alarmName = ' String 11 reverse connection fault';
          error.alarmID = '458';
          error.severity = '3';

          break;
          case 459:
          error.inverterID = inverterId;
          error.alarmName = ' String 12 reverse connection fault';
          error.alarmID = '459';
          error.severity = '3';

          break;
          case 460:
          error.inverterID = inverterId;
          error.alarmName = ' String 13 reverse connection fault';
          error.alarmID = '460';
          error.severity = '3';

          break;
          case 461:
          error.inverterID = inverterId;
          error.alarmName = ' String 14 reverse connection fault';
          error.alarmID = '461';
          error.severity = '3';

          break;
          case 462:
          error.inverterID = inverterId;
          error.alarmName = ' String 15 reverse connection fault';
          error.alarmID = '462';
          error.severity = '3';

          break;
          case 463:
          error.inverterID = inverterId;
          error.alarmName = ' String 16 reverse connection fault';
          error.alarmID = '463';
          error.severity = '3';

          break;
          case 464:
          error.inverterID = inverterId;
          error.alarmName = ' String 17 reverse connection fault';
          error.alarmID = '464';
          error.severity = '3';

          break;
          case 465:
          error.inverterID = inverterId;
          error.alarmName = ' String 18 reverse connection fault';
          error.alarmID = '465';
          error.severity = '3';

          break;
          case 466:
          error.inverterID = inverterId;
          error.alarmName = ' String 19 reverse connection fault';
          error.alarmID = '466';
          error.severity = '3';

          break;
          case 467:
          error.inverterID = inverterId;
          error.alarmName = ' String 20 reverse connection fault';
          error.alarmID = '467';
          error.severity = '3';

          break;
          case 468:
          error.inverterID = inverterId;
          error.alarmName = ' String 21 reverse connection fault';
          error.alarmID = '468';
          error.severity = '3';

          break;
          case 469:
          error.inverterID = inverterId;
          error.alarmName = ' String 22 reverse connection fault';
          error.alarmID = '469';
          error.severity = '3';

          break;
          case 470:
          error.inverterID = inverterId;
          error.alarmName = ' String 23 reverse connection fault';
          error.alarmID = '470';
          error.severity = '3';

          break;
          case 471:
          error.inverterID = inverterId;
          error.alarmName = ' String 24 reverse connection fault';
          error.alarmID = '471';
          error.severity = '3';

          break;
          

          

        default:
      }
    }
  }

  // old 60KTL inverter alarm list

  // if (['20KTL', '60KTL'].includes(model)) {
  //   // alarm 1
  //   if (type === 1) {
  //     switch (bit) {
  //       case 0:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' High String Voltage';
  //         error.alarmID = '2001';
  //         error.severity = '3';

  //         break;
  //       case 1:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' DC Arc Fault';
  //         error.alarmID = '2002';
  //         error.severity = '3';

  //         break;
  //       case 2:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' String reversed';
  //         error.alarmID = '2011';
  //         error.severity = '3';

  //         break;
  //       case 3:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' PV String Backfeed';
  //         error.alarmID = '2012';
  //         error.severity = '3';

  //         break;
  //       case 4:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Abnormal String';
  //         error.alarmID = '2013';
  //         error.severity = '3';

  //         break;
  //       case 5:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' AFCI Self-test Fault';
  //         error.alarmID = '2021';
  //         error.severity = '3';

  //         break;
  //       case 6:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Short circuit between phase to PE';
  //         error.alarmID = '2031';
  //         error.severity = '3';

  //         break;
  //       case 7:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Power Grid Failure';
  //         error.alarmID = '2032';
  //         error.severity = '3';

  //         break;
  //       case 8:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Grid Undervoltage';
  //         error.alarmID = '2033';
  //         error.severity = '3';

  //         break;
  //       case 9:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Grid Overvoltage';
  //         error.alarmID = '2034';
  //         error.severity = '3';

  //         break;
  //       case 10:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Unbalanced Grid Voltage';
  //         error.alarmID = '2035';
  //         error.severity = '3';

  //         break;
  //       case 11:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Grid Overfrequency';
  //         error.alarmID = '2036';
  //         error.severity = '3';

  //         break;
  //       case 12:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Grid Underfrequency';
  //         error.alarmID = '2037';
  //         error.severity = '3';

  //         break;
  //       case 13:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Grid Frequency Instability';
  //         error.alarmID = '2038';
  //         error.severity = '3';

  //         break;
  //       case 14:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Output Overcurrent';
  //         error.alarmID = '2039';
  //         error.severity = '3';

  //         break;
  //       case 15:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Large DC of Output current';
  //         error.alarmID = '2040';
  //         error.severity = '3';

  //         break;
  //     }
  //   }
  //   // alarm 2
  //   if (type === 2) {
  //     switch (bit) {
  //       case 0:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Abnormal Leakage Current';
  //         error.alarmID = '2051';
  //         error.severity = '3';

  //         break;
  //       case 1:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Abnormal Ground';
  //         error.alarmID = '2061';
  //         error.severity = '3';

  //         break;
  //       case 2:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Low Insulation Res';
  //         error.alarmID = '504';
  //         error.severity = '2';

  //         break;
  //       case 3:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' High Temperature';
  //         error.alarmID = '2063';
  //         error.severity = '3';

  //         break;
  //       case 4:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Abnormal Equipment';
  //         error.alarmID = '2064';
  //         error.severity = '3';

  //         break;
  //       case 5:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Upgrade Failed';
  //         error.alarmID = '2065';
  //         error.severity = '2';

  //         break;
  //       case 6:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' License Expired';
  //         error.alarmID = '2066';
  //         error.severity = '1';

  //         break;
  //       case 7:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Abnormal Monitor Unit';
  //         error.alarmID = '61440';
  //         error.severity = '2';

  //         break;
  //       default:
  //     }
  //   }
  //   // alarm 3
  //   if (type === 3) {
  //     switch (bit) {
  //       case 1:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' High String Voltage to Ground';
  //         error.alarmID = '2014';
  //         error.severity = '3';

  //         break;
  //       case 2:
  //         error.inverterID = inverterId;
  //         error.alarmName = ' Built-in PID operation abnormal';
  //         error.alarmID = '2085';
  //         error.severity = '3';

  //         break;
  //       default:
  //     }
  //   }
  // }
  // if (!localCache.errorInverter) {
  //   localCache.errorInverter = {};
  // }
  // if(!!error.inverterID){

  // }
  // localCache.errorInverter = []
  // localCache.errorInverter.push(error);
  localCache.errorInverter[`${inverterId}`] = error;
};

module.exports = { alarm };
