'use strict';
const fs = require('fs-extra');
const moment = require('moment');
const appProperties = require('../../../config/properties');
const logger = require('../../../loggerHelper').getLogger('log');

class Base {
  constructor(modbusHelper, args, smartLoggerIndex) {
    this.modbusHelper = modbusHelper;
    this.args = args;
    this.appProperties = appProperties;
    this.logger = logger;
    this.moment = moment;
    this.smartLoggerIndex = smartLoggerIndex;
    return this;
  }

  async writeDataToLocal(filePath, data) {
    await fs.writeFileSync(filePath, JSON.stringify(data));
  }
}

module.exports = Base;
