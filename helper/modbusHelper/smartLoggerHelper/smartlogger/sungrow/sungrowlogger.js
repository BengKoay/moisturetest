const Base = require('../base');
const piConfig = require('../../../../../piConfig').piConfig;
const fs = require('fs-extra');
const fixedVariable = 200000;
let localCache = require('../../../../localCache');
const logger = require('../../../../loggerHelper').getLogger('log');

const invalidNumberRangeA = (key, value) => {
  if (['pvACSupply'].includes(key)) {
    if (value >= 4294967 || value <= -4294967) {
      return 0;
    }
  }

  return value;
};

const invalidNumberRangeB = (key, value) => {
  if (['capping'].includes(key)) {
    if (value > 1 || value <= -4294967) {
      return 1;
    }

    if (value === 0 || value <= -4294967) {
      return 0;
    }
  }

  return value;
};

class SUNGROWLOGGER extends Base {
  constructor(modbusHelper, smartloggerConfig, smartLoggerIndex) {
    super(modbusHelper, smartloggerConfig, smartLoggerIndex);
  }

  async readData() {
    const { id, brand, weatherStationId, inverterConfig } =
      this.smartloggerConfig;
    let payload = {
      loggerBrand: brand,
      loggerId: id,
      weatherStationId,
      dcInputPower: 0,
      powerFactor: 0,
      reactivePower: 0,
      currentA: 0,
      currentB: 0,
      currentC: 0,
      activePower: 0,
      serialNumber: '',
      energyTotalToDate: 0,
      timeChecker: 0,
      loggerTimeNow: 0,
      lastUpdate: this.appProperties.smartLoggerErrorTimestamp,
    };

    try {
      await this.setId(id);
      // const dcInputPowerValue = await this.modbusHelper.readHoldingRegisters(
      //  40521,
      //  2
      // );
      // const powerFactorValue = await this.modbusHelper.readHoldingRegisters(
      //  40532,
      // 1
      // );

      const reactivePowerValue = await this.modbusHelper.readInputRegisters(
        8076 - 1,
        6
      );
      var bufA = reactivePowerValue.buffer.slice(0, 4).swap16();
      var bufB = reactivePowerValue.buffer.slice(4, 8).swap16();
      var A = bufA.readInt32LE(0);
      var B = bufB.readInt32LE(0);

      var FinalreactivePowerValue = A * 4294967296 + B;

      //console.log(FinalreactivePowerValue );
      //const currentValue = await this.modbusHelper.readHoldingRegisters(
      // 40572,
      // 3
      //  );

      // const serialNumber = await this.modbusHelper.readHoldingRegisters(
      //  40713,
      //  10
      //);

      let sunActivePower = await this.modbusHelper.readInputRegisters(8069, 4);

      var bufA = sunActivePower.buffer.slice(0, 4).swap16();
      var bufB = sunActivePower.buffer.slice(4, 8).swap16();
      var A = bufA.readUInt32LE(0);
      var B = bufB.readUInt32LE(0);

      var FinalpvACSupply = (A * 4294967296 + B) / 1000;

      const pvACSupply = invalidNumberRangeA('pvACSupply', FinalpvACSupply);
      // console.log("pvACSupply" + pvACSupply);
      const energyTotalToDate = await this.modbusHelper.readInputRegisters(
        8080 - 1,
        6
      );

      var bufA = energyTotalToDate.buffer.slice(0, 4).swap16();
      var bufB = energyTotalToDate.buffer.slice(4, 8).swap16();
      var A = bufA.readUInt32LE(0);
      var B = bufB.readUInt32LE(0);

      var FinalenergyTotalToDate = A * 4294967296 + B;

      // const invEfficiency =
      //   (
      //     await this.modbusHelper.readHoldingRegisters(40685, 10)
      //   ).buffer.readUInt16BE(0) / 100;

      // payload.dcInputPower = dcInputPowerValue.buffer.readUInt32BE(0) / 1000;

      for (const smartloggerconfig of piConfig.smartloggerConfig) {
        try {
        } catch (e) {}

        for (const inverterConfig of smartloggerconfig.inverterConfig) {
          try {
            payload.dcInputPower +=
              localCache.inverterReading[`${inverterConfig.id}`].dcPower;
          } catch (e) {}
        }
      }
      //payload.powerFactor = powerFactorValue.buffer.readInt16BE(0) / 1000;
      payload.reactivePower = FinalreactivePowerValue / 1000;
      //payload.currentA = currentValue.data[0] / 10;
      // payload.currentB = currentValue.data[1] / 10;
      // payload.currentC = currentValue.data[2] / 10;
      payload.activePower = pvACSupply;
      payload.energyTotalToDate = FinalenergyTotalToDate;
      //payload.invEfficiency = invEfficiency;
      /*
      if (
        !!serialNumber.data[6] &&
        !!serialNumber.data[7] &&
        !!serialNumber.data[8] &&
        serialNumber.data[9] != 0
      ) {
        payload.serialNumber = serialNumber.buffer.toString();
      } else {
        payload.serialNumber = serialNumber.buffer.toString().substring(0, 12);
      }

      const loggerTimeNow = (
        await this.modbusHelper.readHoldingRegisters(40000, 2)
      ).buffer.readUInt32BE(0);
      const timeChecker = Math.abs(this.moment().unix() - loggerTimeNow);
*/
      // payload.loggerTimeNow = loggerTimeNow;
      // payload.timeChecker = timeChecker;
      payload.lastUpdate = this.moment().unix();

      await this.writeSmartLoggerDataFile(id, payload);
    } catch (e) {
      this.logger.error('SmartLogger readData Error', e);

      let error = {
        loggerID: '',
        errorName: '',
      };

      error.loggerID = id;
      error.errorName = e.name;

      localCache.errorSmartlogger = error; // pass info to dataCombineHelper
      console.log('localCache.errorSmartlogger', localCache.errorSmartlogger);
    }
  }

  async readWeatherData() {
    const { id, weatherStationId } = this.smartloggerConfig;

    const payload = {
      windSpeed: 0,
      moduleTemp: 0,
      ambientTemp: 0,
      irrSensor: 0,
      lastUpdate: this.moment().unix(),
    };

    if (!!weatherStationId) {
      try {
        await this.setId(weatherStationId);
        const weatherData = await this.modbusHelper.readHoldingRegisters(
          '0067',
          30
        );

        payload.windSpeed = weatherData.buffer.readInt16BE(16);
        payload.moduleTemp = weatherData.buffer.readInt16BE(48) / 10;
        payload.ambientTemp = weatherData.buffer.readInt16BE(8) / 10;
        payload.irrSensor = weatherData.buffer.readUInt16BE(34);
      } catch (e) {
        this.logger.error('SmartLogger Get Weather Station Error', e);
      }
    } else {
      payload.windSpeed = 0;
      payload.moduleTemp = 0;
      payload.ambientTemp = 0;
      payload.irrSensor = 0;
    }
    payload.lastUpdate = this.moment().unix();

    await this.writeSmartLoggerWeatherDataFile(id, payload);
  }

  async readInverterdata() {
    const { inverterConfig } = this.smartloggerConfig;

    try {
      for (const inverter of inverterConfig) {
        await this.getInverterData(
          this.modbusHelper,
          inverter,
          this.smartLoggerIndex
        );
      }
    } catch (e) {
      this.logger.error('getInverterData Error', e);
    }
  }

  async controlSolarPower() {
    try {
      if (this.moment().hour() >= 6 && this.moment().hour() <= 19) {
        if (this.piConfig.typeOfService === 1) {
          await this.maxSolarOutput();
        } else if (this.piConfig.typeOfService === 2) {
          await this.controlSolarOutput();
        } else {
          this.logger.error('Unknowns Type Of service');
        }
      } else {
        await this.maxSolarOutput();
      }
    } catch (e) {
      this.logger.error('controlSolarPower Error', e);

      let error = {
        loggerID: '',
        errorName: '',
      };

      error.loggerID = this.id;
      error.errorName = e.name;

      localCache.errorSmartlogger = error; // pass info to dataCombineHelper
      console.log('localCache.errorSmartlogger', localCache.errorSmartlogger);
    }
  }

  async controlSolarOutput() {
    const { id } = this.smartloggerConfig;
    let controlPowerState = 2;
    let controlPowerValue = this.piConfig.maxPVoutput;
    try {
      const energyDataInfo = await this.readPowerMeterDataFile();
      const smartLoggerInfo = await this.readSmartLoggerDataFile(id);
      const { activePower } = smartLoggerInfo;

      let totalBuildingLoad = 0;
      let totalUtilityIntake = 0;
      energyDataInfo.map((obj) => {
        totalBuildingLoad += obj.utilitiesActivePower || 0;
        totalUtilityIntake += obj.utilitiesActivePower || 0;
      });

      totalBuildingLoad += activePower || 0;
      let deductedFixedVariable = false;
      if (totalBuildingLoad > fixedVariable) {
        deductedFixedVariable = true;
        totalBuildingLoad = totalBuildingLoad - fixedVariable;
      }

      if (
        totalBuildingLoad > this.appProperties.maxTnbPower ||
        totalBuildingLoad < 0 ||
        activePower < 0 ||
        totalUtilityIntake > this.appProperties.maxTnbPower ||
        totalUtilityIntake < 0
      ) {
        // State 1, give 0% to solar power
        controlPowerState = 1;
        controlPowerValue = 0;
      }

      if (activePower > 0) {
        // let ratio = activePower / totalBuildingLoad;
        // this.logger.info('ratio', ratio);
        // if (ratio >= 1) {
        //   // Utility Intake is < 0
        //   controlPowerState = 3;
        //   controlPowerValue =
        //     totalBuildingLoad * this.maxSolarCappingPercentage;
        // }

        // if (ratio >= this.maxSolarCappingPercentage && ratio < 1) {
        //   controlPowerState = 3;
        //   controlPowerValue = activePower * this.maxSolarCappingPercentage;
        // }

        // if (ratio < this.maxSolarCappingPercentage) {
        //   controlPowerState = 2;
        //   controlPowerValue = this.piConfig.maxPVoutput;
        // }

        let triggeredState3 = false;
        if (
          totalBuildingLoad <=
          this.piConfig.maxPVoutput * this.maxSolarCappingPercentage
        ) {
          triggeredState3 = true;
        }
        if (triggeredState3) {
          controlPowerState = 3;
          controlPowerValue =
            totalBuildingLoad * this.maxSolarCappingPercentage;
        }
      }
      if (controlPowerValue > totalBuildingLoad) {
        this.logger.info('Uncatchable Control Solar Output condition');
        controlPowerState = 3;
        controlPowerValue = totalBuildingLoad * this.maxSolarCappingPercentage;
      }

      if (controlPowerValue < 0) {
        controlPowerState = 1;
        controlPowerValue = 0;
      }

      // this.logReading({
      //   activePower,
      //   totalUtilityIntake,
      //   totalBuildingLoad: deductedFixedVariable
      //     ? totalBuildingLoad + fixedVariable
      //     : totalBuildingLoad,
      //   controlPowerState,
      //   controlPowerValue
      // });

      if (controlPowerState === 3) {
        const notification = {
          companyId: this.piConfig.companyId,
          locationId: this.piConfig.locationId,
          lotId: this.piConfig.lotId,
          deviceId: this.piConfig.deviceId,
          code: 110, //Zero Export Capping,
          createdAt: this.moment().toISOString(),
          updatedAt: this.moment().toISOString(),
        };
        // await fs.writeFileSync(
        //   `${
        //     this.appProperties.localFileStorageLocation.ensureDirectories
        //       .processedNotificationPoolPath
        //   }/${this.moment().unix()}.json`,
        //   JSON.stringify(notification)
        // );
      }

      var capping = invalidNumberRangeB(
        'capping',
        controlPowerValue / this.piConfig.maxPVoutput
      );
      var capping2 = capping.toFixed(2);
      var capping3 = capping2 * 1000;

      //console.log("controlpowervalue" + controlPowerValue)
      //console.log(controlPowerValue /this.piConfig.maxPVoutput)
      //console.log("capping", capping3)
      //console.log(controlPowerState)

      await this.writeControlSolarOutputFile(id, {
        controlPowerValue,
        controlPowerState,
      });

      await this.setId(id);
      await this.modbusHelper.writeRegisters(8004, [capping3, 0]);

      // console.log('mm')
      //await this.modbusHelper.writeRegisters(8003-1, [

      //5*10  , 0 ,0,0
      //]);
    } catch (e) {
      this.logger.error('controlSolarOutput Error', e);

      let error = {
        loggerID: '',
        errorName: '',
      };

      error.loggerID = id;
      error.errorName = e.name;

      localCache.errorSmartlogger = error; // pass info to dataCombineHelper
      console.log('localCache.errorSmartlogger', localCache.errorSmartlogger);
    }
  }

  async maxSolarOutput() {
    try {
      const { id } = this.smartloggerConfig;
      await this.setId(id);
      await this.modbusHelper.writeRegisters(8005 - 1, [
        (this.piConfig.maxPVoutput / this.piConfig.maxPVoutput) * 1000,
        0,
      ]);
      await this.writeControlSolarOutputFile(id, {
        controlPowerValue: this.piConfig.maxPVoutput,
        controlPowerState: 0,
      });
    } catch (e) {
      const { id } = this.smartloggerConfig;
      this.logger.error('maxSolarOutput Error', e);

      let error = {
        loggerID: '',
        errorName: '',
      };

      error.loggerID = id;
      error.errorName = e.name;

      localCache.errorSmartlogger = error; // pass info to dataCombineHelper
      console.log('localCache.errorSmartlogger', localCache.errorSmartlogger);
    }
  }
}

module.exports = SUNGROWLOGGER;
