const Base = require('../base');
const fs = require('fs-extra');
let localCache = require('../../../../localCache');

const invalidNumberRange = (key, value) => {
  if (['currentA', 'currentB', 'currentC'].includes(key)) {
    if (value > 4294967 || value < -4294967) {
      return 0;
    }
  }

  return value;
};

class SMA_CLUSTER_CONTROLLER extends Base {
  constructor(modbusHelper, smartloggerConfig, smartLoggerIndex) {
    super(modbusHelper, smartloggerConfig, smartLoggerIndex);
  }

  async readData() {
    const { brand, id, weatherStationId } = this.smartloggerConfig;
    let payload = {
      loggerBrand: brand,
      loggerId: id,
      weatherStationId,
      dcInputPower: 0,
      powerFactor: 0,
      reactivePower: 0,
      currentA: 0,
      currentB: 0,
      currentC: 0,
      activePower: 0,
      serialNumber: '',
      lastUpdate: this.appProperties.smartLoggerErrorTimestamp,
    };

    try {
      await this.setId(id);
      const serialNumber = await this.modbusHelper.readHoldingRegisters(
        30005,
        2
      );
      const pvACSupply = await this.modbusHelper.readHoldingRegisters(
        30773,
        50
      );

      payload.dcInputPower = pvACSupply.buffer.readInt32BE(0) / 1000;
      payload.powerFactor = pvACSupply.buffer.readUInt32BE(96) / 100;
      payload.reactivePower = pvACSupply.buffer.readInt32BE(64);
      payload.currentA = invalidNumberRange(
        'currentA',
        pvACSupply.buffer.readUInt32BE(48) / 1000
      );
      payload.currentB = invalidNumberRange(
        'currentB',
        pvACSupply.buffer.readUInt32BE(52) / 1000
      );
      payload.currentC = invalidNumberRange(
        'currentC',
        pvACSupply.buffer.readUInt32BE(56) / 1000
      );
      payload.serialNumber = String(serialNumber.buffer.readUInt32BE(0));
      payload.activePower = pvACSupply.buffer.readInt32BE(4) / 1000;
      payload.energyTotalToDate = null;
      payload.invEfficiency = null;

      payload.lastUpdate = this.moment().unix();

      await this.writeSmartLoggerDataFile(id, payload);
    } catch (e) {
      this.logger.error('SmartLogger readData Error', e);
      localCache.errorSmartlogger = e;
      console.log('localCache.errorSmartlogger', localCache.errorSmartlogger);
    }
  }

  async readWeatherData() {
    const { id, weatherStationId } = this.smartloggerConfig;

    const payload = {
      windSpeed: 0,
      moduleTemp: 0,
      ambientTemp: 0,
      irrSensor: 0,
    };

    if (!!weatherStationId) {
      try {
        await this.setId(weatherStationId);
        const weatherData1 = await this.modbusHelper.readHoldingRegisters(
          34621,
          2
        );
        const weatherData2 = await this.modbusHelper.readHoldingRegisters(
          34609,
          2
        );
        const weatherData3 = await this.modbusHelper.readHoldingRegisters(
          34623,
          2
        );
        const weatherData4 = await this.modbusHelper.readHoldingRegisters(
          34633,
          2
        );
        payload.windSpeed = weatherData4.buffer.readUInt32BE(0) / 10;
        payload.moduleTemp = weatherData1.buffer.readInt32BE(0) / 10;
        payload.ambientTemp = weatherData2.buffer.readInt32BE(0) / 10;
        payload.irrSensor = weatherData3.buffer.readInt32BE(0);
      } catch (e) {
        this.logger.error('SmartLogger Get Weather Station Error', e);
      }
    } else {
      payload.windSpeed = 0;
      payload.moduleTemp = 0;
      payload.ambientTemp = 0;
      payload.irrSensor = 0;
    }
    payload.lastUpdate = this.moment().unix();

    await this.writeSmartLoggerWeatherDataFile(id, payload);
  }

  async readInverterdata() {
    const { inverterConfig } = this.smartloggerConfig;

    try {
      for (const inverter of inverterConfig) {
        await this.getInverterData(this.modbusHelper, inverter);
      }
    } catch (e) {
      this.logger.error('getInverterData Error', e);
    }
  }

  async controlSolarPower() {
    try {
      if (this.moment().hour() >= 6 && this.moment().hour() <= 19) {
        if (this.piConfig.typeOfService === 1) {
          await this.maxSolarOutput();
        } else if (this.piConfig.typeOfService === 2) {
          await this.controlSolarOutput();
        } else {
          this.logger.error('Unknowns Type Of service');
        }
      } else {
        await this.maxSolarOutput();
      }
    } catch (e) {
      this.logger.error('controlSolarPower Error', e);
      localCache.errorSmartlogger = e;
      console.log('localCache.errorSmartlogger', localCache.errorSmartlogger);
    }
  }

  async controlSolarOutput() {
    const { id } = this.smartloggerConfig;
    let controlPowerState = 2;
    let controlPowerValue = this.piConfig.maxPVoutput;

    try {
      const energyDataInfo = await this.readPowerMeterDataFile();
      const smartLoggerInfo = await this.readSmartLoggerDataFile(id);
      const { activePower } = smartLoggerInfo;

      let totalBuildingLoad = 0;
      let totalUtilityIntake = 0;
      energyDataInfo.map((obj) => {
        totalBuildingLoad += obj.utilitiesActivePower || 0;
        totalUtilityIntake += obj.utilitiesActivePower || 0;
      });

      totalBuildingLoad += activePower;

      // State 1, give 0% to solar power
      if (
        totalBuildingLoad > this.appProperties.maxTnbPower ||
        totalBuildingLoad < 0 ||
        activePower < 0 ||
        totalUtilityIntake > this.appProperties.maxTnbPower ||
        totalUtilityIntake < 0
      ) {
        controlPowerState = 1;
        controlPowerValue = 0;
      }

      // State 3, control power
      if (activePower > 0) {
        let triggeredState3 = false;
        if (
          totalBuildingLoad <=
          this.piConfig.maxPVoutput * this.maxSolarCappingPercentage
        ) {
          triggeredState3 = true;
        }

        if (triggeredState3) {
          controlPowerState = 3;
          controlPowerValue =
            totalBuildingLoad * this.maxSolarCappingPercentage;
        }
      }

      if (controlPowerValue > totalBuildingLoad) {
        this.logger.info('Uncatchable Control Solar Output condition');
        controlPowerState = 3;
        controlPowerValue = totalBuildingLoad * this.maxSolarCappingPercentage;
      }

      // this.logReading({
      //   activePower,
      //   totalUtilityIntake,
      //   controlPowerState,
      //   controlPowerValue
      // });

      if (controlPowerState === 3) {
        const notification = {
          companyId: this.piConfig.companyId,
          locationId: this.piConfig.locationId,
          lotId: this.piConfig.lotId,
          deviceId: this.piConfig.deviceId,
          code: 110, //Zero Export Capping,
          createdAt: this.moment().toISOString(),
          updatedAt: this.moment().toISOString(),
        };
        // await fs.writeFileSync(
        //   `${
        //     this.appProperties.localFileStorageLocation.ensureDirectories
        //       .processedNotificationPoolPath
        //   }/${this.moment().unix()}.json`,
        //   JSON.stringify(notification)
        // );
      }

      await this.setId(id);
      await this.modbusHelper.writeRegister(
        40016,
        (controlPowerValue / this.piConfig.maxPVoutput) * 100
      );

      return await this.writeControlSolarOutputFile(id, {
        controlPowerValue,
        controlPowerState,
      });
    } catch (e) {
      await this.writeControlSolarOutputFile(id, {
        controlPowerValue,
        controlPowerState,
      });
      this.logger.error('controlSolarOutput Error', e);
      localCache.errorSmartlogger = e;
      console.log('localCache.errorSmartlogger', localCache.errorSmartlogger);
    }
  }

  async maxSolarOutput() {
    try {
      const { id } = this.smartloggerConfig;
      await this.setId(id);
      //await this.modbusHelper.writeRegister(40016, 100);
      await this.writeControlSolarOutputFile(id, {
        controlPowerValue: this.piConfig.maxPVoutput,
        controlPowerState: 0,
      });
    } catch (e) {
      //this.logger.error('maxSolarOutput Error', e);
      localCache.errorSmartlogger = e;
      console.log('localCache.errorSmartlogger', localCache.errorSmartlogger);
    }
  }
}

module.exports = SMA_CLUSTER_CONTROLLER;
