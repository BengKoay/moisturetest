'use strict';
const fs = require('fs-extra');
const moment = require('moment-timezone');
const InverterResources = require('../inverter');
const appProperties = require('../../../config/properties');
let localCache = require('../../../localCache');
const logger = require('../../../loggerHelper').getLogger('log');
const piConfig = require('../../../../piConfig').piConfig;

moment.tz.setDefault(appProperties.momentTimeZone);
class Base {
  constructor(modbusHelper, smartloggerConfig, smartLoggerIndex) {
    this.modbusHelper = modbusHelper;
    this.smartloggerConfig = smartloggerConfig;
    this.appProperties = appProperties;
    this.logger = logger;
    this.moment = moment;
    this.piConfig = piConfig;
    this.maxSolarCappingPercentage =
      piConfig.solarCappingPercentage || appProperties.solarCappingPercentage;
    this.smartLoggerIndex = smartLoggerIndex;
    return this;
  }

  async setId(smartLoggerId) {
    await this.modbusHelper.setID(smartLoggerId);
  }

  async readPowerMeterDataFile() {
    let data = [];
    try {
      data = localCache.energyMeterReading;
    } catch (e) {
      this.logger.error('readPowerMeterDataFile Error', e);
    }

    return data;
  }

  async readSmartLoggerDataFile(loggerId) {
    let memoryKey = `smartlogger_${loggerId}`;

    let data = {
      activePower: 0,
    };

    try {
      data = localCache.smartLoggerReading[memoryKey];
    } catch (e) {
      this.logger.error('readSmartLoggerDataFile Error', e);

      let error = {
        loggerID: '',
        errorName: '',
      };

      error.loggerID = loggerId;
      error.errorName = e.name;

      localCache.errorSmartlogger = error; // pass info to dataCombineHelper
    }
    return data;
  }

  async writeSmartLoggerDataFile(loggerId, data) {
    let memoryKey = `smartlogger_${loggerId}`;
    if (!localCache.smartLoggerReading) {
      localCache.smartLoggerReading = {};
    }
    localCache.smartLoggerReading[memoryKey] = data;
  }

  async writeSmartLoggerWeatherDataFile(loggerId, data) {
    let memoryKey = `smartlogger_weather_${loggerId}`;

    if (!localCache.weatherSensorReading) {
      localCache.weatherSensorReading = {};
    }
    localCache.weatherSensorReading[memoryKey] = data;
  }

  async writeControlSolarOutputFile(loggerId, data) {
    data.lastUpdated = this.moment().unix();

    if (!localCache.controlSolarOutputData) {
      localCache.controlSolarOutputData = {};
    }

    localCache.controlSolarOutputData[loggerId] = data;
  }

  async logReading(reading) {
    logger.info(JSON.stringify(reading));
  }

  async getInverterData(modbusHelper, inverter, smartLoggerIndex) {
    switch (inverter.model) {
      case this.appProperties.inverterModelList.huaweiSun200012ktl:
        await new InverterResources.HUAWEI_INVERTER.SUN2000_12KTL(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      case this.appProperties.inverterModelList.huaweiSun200012ktl2:
        await new InverterResources.HUAWEI_INVERTER.SUN2000_12KTL_2(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      case this.appProperties.inverterModelList.huaweiSun200017ktl:
        await new InverterResources.HUAWEI_INVERTER.SUN2000_17KTL(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      case this.appProperties.inverterModelList.huaweiSun200020ktl:
        await new InverterResources.HUAWEI_INVERTER.SUN2000_20KTL(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      case this.appProperties.inverterModelList.huaweiSun200020ktl2:
        await new InverterResources.HUAWEI_INVERTER.SUN2000_20KTL_2(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      case this.appProperties.inverterModelList.huaweiSun200036ktl:
        await new InverterResources.HUAWEI_INVERTER.SUN2000_36KTL(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      case this.appProperties.inverterModelList.huaweiSun200060ktl:
        await new InverterResources.HUAWEI_INVERTER.SUN2000_60KTL(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      case this.appProperties.inverterModelList.huaweiSun2000100ktl:
        await new InverterResources.HUAWEI_INVERTER.SUN2000_100KTL(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      // Inverter Manager
      case this.appProperties.inverterModelList.smaInverterManager:
        await new InverterResources.SMA_INVERTER.SMA_INVERTER_MANAGER(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      // Cluster Controller
      case this.appProperties.inverterModelList.smaClusterController:
        await new InverterResources.SMA_INVERTER.SMA_INVERTER_CLUSTER_CONTROLLER(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      //Sungrow
      case this.appProperties.inverterModelList.SUNGROW:
        await new InverterResources.SUNGROW_INVERTER.SUNGROW(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      case this.appProperties.inverterModelList.SUNGROW_SG110CX:
        await new InverterResources.SUNGROW_INVERTER.SUNGROW_SG110CX(
          modbusHelper,
          inverter,
          smartLoggerIndex
        ).readData();
        break;

      default:
        throw new Error('Not in inverter list!');
    }
  }
}

module.exports = Base;
