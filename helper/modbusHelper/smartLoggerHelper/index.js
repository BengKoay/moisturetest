'use strict';
const ModbusRTU = require('modbus-serial');
const fs = require('fs-extra');
const moment = require('moment');

const Base = require('../base');
const piConfig = require('../../../piConfig.js').piConfig;
const appProperties = require('../../config/properties');
const logger = require('../../loggerHelper').getLogger('log');

const smartLoggerResources = require('./smartlogger');

class SmartLoggerHelper extends Base {
  constructor() {
    super();

    let smartLoggerMeter = {
      successfulList: [],
      unsuccessfulList: [],
    };

    if (!!piConfig.smartloggerConfig && piConfig.smartloggerConfig.length > 0) {
      for (let smartlogger of piConfig.smartloggerConfig) {
        let modbusHelper = new ModbusRTU();
        modbusHelper.connectTCP(
          smartlogger.ip,
          { port: smartlogger.port },
          (error, data) => {
            if (error) {
              logger.info(
                `Smartlogger with IP of ${smartlogger.ip} is initialization unsuccessful`
              );
              smartLoggerMeter.unsuccessfulList.push({
                smartloggerconfig: smartlogger,
                modbusHelper,
              });
            } else {
              logger.info(
                `Smartlogger with IP of ${smartlogger.ip} is initialization successful`
              );
              smartLoggerMeter.successfulList.push({
                smartloggerconfig: smartlogger,
                modbusHelper,
              });
            }
          }
        );
        // modbusHelper.setTimeout(500); // Beng commented because got issue. To be tested
      }
    }

    this.smartLoggerMeter = smartLoggerMeter;
    return this;
  }

  async getConnectionStatus() {
    let connectionStatus = this.smartLoggerMeter.unsuccessfulList.length === 0;

    const pendingDataFileList = await fs.readdirSync(
      appProperties.localFileStorageLocation.ensureDirectories
        .resetSmartloggerConnectionPath
    );
    for (const pendingDataFile of pendingDataFileList) {
      if (
        !fs
          .statSync(
            `${appProperties.localFileStorageLocation.ensureDirectories.resetSmartloggerConnectionPath}/${pendingDataFile}`
          )
          .isDirectory()
      ) {
        await fs.unlinkSync(
          `${appProperties.localFileStorageLocation.ensureDirectories.resetSmartloggerConnectionPath}/${pendingDataFile}`
        );
        connectionStatus = false;
      }
    }

    return connectionStatus;
  }

  async readData() {
    if (this.smartLoggerMeter.successfulList.length > 0) {
      let smartLoggerIndex = piConfig.smartloggerConfig[0].id;
      // changed smartLoggerIndex from hard code "0", Huawei smartlogger uses ID 0, SMA inverter manager uses ID 125
      for (const smartlogger of this.smartLoggerMeter.successfulList) {
        const { modbusHelper, smartloggerconfig } = smartlogger;
        switch (smartloggerconfig.brand) {
          // Huawei
          case 1:
            await new smartLoggerResources.HUAWEI_SUN2000(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readData();
            await new smartLoggerResources.HUAWEI_SUN2000(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readWeatherData();
            await new smartLoggerResources.HUAWEI_SUN2000(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readInverterdata();
            break;

          // SMA Inverter Manager
          case 2:
            await new smartLoggerResources.SMA_INVERTER_MANAGER(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readData();
            await new smartLoggerResources.SMA_INVERTER_MANAGER(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readWeatherData();
            await new smartLoggerResources.SMA_INVERTER_MANAGER(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readInverterdata();
            break;

          // SMA Cluster Controller
          case 3:
            await new smartLoggerResources.SMA_CLUSTER_CONTROLLER(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readData();
            await new smartLoggerResources.SMA_CLUSTER_CONTROLLER(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readWeatherData();
            await new smartLoggerResources.SMA_CLUSTER_CONTROLLER(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readInverterdata();
            break;

          // sungrowlogger
          case 4:
            await new smartLoggerResources.SUNGROWLOGGER(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readData();
            await new smartLoggerResources.SUNGROWLOGGER(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readWeatherData();
            await new smartLoggerResources.SUNGROWLOGGER(
              modbusHelper,
              smartloggerconfig,
              smartLoggerIndex
            ).readInverterdata();
            break;

          default:
            throw new Error('Smartlogger not setup yet');
        }

        smartLoggerIndex++;
        await this.sleep(500);
      }
    }
  }
  async controlSolar() {
    if (this.smartLoggerMeter.successfulList.length > 0) {
      for (const smartlogger of this.smartLoggerMeter.successfulList) {
        const { modbusHelper, smartloggerconfig } = smartlogger;
        switch (smartloggerconfig.brand) {
          case 1:
            // Huawei
            await new smartLoggerResources.HUAWEI_SUN2000(
              modbusHelper,
              smartloggerconfig
            ).controlSolarPower();
            break;

          // SMA Inverter Manager
          case 2:
            await new smartLoggerResources.SMA_INVERTER_MANAGER(
              modbusHelper,
              smartloggerconfig
            ).controlSolarPower();
            break;

          // SMA Cluster Controller
          case 3:
            await new smartLoggerResources.SMA_CLUSTER_CONTROLLER(
              modbusHelper,
              smartloggerconfig
            ).controlSolarPower();
            break;

          // SUNGROWLOGGER
          case 4:
            await new smartLoggerResources.SUNGROWLOGGER(
              modbusHelper,
              smartloggerconfig
            ).controlSolarPower();
            break;

          default:
            throw new Error('Smartlogger not setup yet');
        }

        await this.sleep(500);
      }
    }
  }
}

module.exports = SmartLoggerHelper;
