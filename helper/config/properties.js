const localDataPath = 'localdata';
const localStoragePath = `${process.cwd()}/${localDataPath}`;
const dataPoolPath = `${localStoragePath}/dataPool`;
const modbusConnectionPath = `${localStoragePath}/modbus`;
const powerMeterReadingPath = `${localStoragePath}/reading/powerMeterReading`;

module.exports = {
	awsIoTHost: 'a3j1bzslspz95c-ats.iot.ap-southeast-1.amazonaws.com',
	awsApiGateway:
		// 'https://rah975q5kb.execute-api.ap-southeast-1.amazonaws.com/prod/',
		'https://9ry30zin92.execute-api.ap-southeast-1.amazonaws.com/public',
	solarCappingPercentage: 0.95,
	huaweiSmartLoggerMaxNumber: 4294967,
	maxTnbPower: 30000,
	powerMeterErrorTimestamp: 1546300800,
	smartLoggerErrorTimestamp: 1546300800,
	momentTimeZone: 'Asia/Kuala_Lumpur',
	mosca: {
		qos: 2,
		channel: {
			powerMeter: `PSChannelPowerMeter`,
			smartLogger: `PSChannelSmartLogger`,
		},
	},
	publishMethod: {
		mqtt: 'mqtt',
		rest: 'rest',
		none: 'none',
	},
	meterTypeReading: {
		powerMeter: 1,
		smartLogger: 2,
	},
	plusSolarIoTChannel: 'plus-solar-iot-channel',
	localStoragePath: localStoragePath,

	awsCredentials: {
		accessKeyId: 'AKIAVBSYQGXZKUHKAD46', // piuser
		secretAccessKey: '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',
		region: 'ap-southeast-1',
	},

	localFileStorageLocation: {
		ensureDirectories: {
			modbusConnectionPath: modbusConnectionPath,
			resetPowerMeterConnectionPath: `${modbusConnectionPath}/powermeter`,
			resetSmartloggerConnectionPath: `${modbusConnectionPath}/smartlogger`,
			processedNotificationPoolPath: `${dataPoolPath}/notification`,
			publishingNotificationPoolPath: `${dataPoolPath}/notification/publishing`,
			publishedNotificationPoolPath: `${dataPoolPath}/notification/published`,
			powerMeterReadingPath,
			smartLoggerReadingPath: `${localStoragePath}/reading/smartLoggerReading`,
			inverterReadingDataPath: `${localStoragePath}/reading/inverterReading`,
			controlSolarOutputPath: `${localStoragePath}/reading/controlSolarOutputReading`,
			processedDataPoolPath: `${dataPoolPath}/processedData`,
			publishingDataPoolPath: `${dataPoolPath}/processedData/publishing`,
			publishingErrorDataPoolPath: `${dataPoolPath}/processedData/publishingError`,
			publishedDataPoolPath: `${dataPoolPath}/processedData/published`,
		},
		metersReading: `${powerMeterReadingPath}/metersReading.json`,
		selfCalculateActiveEnergy: `${powerMeterReadingPath}/selfCalculateActiveEnergy.json`,
		lastPushNotificationTimestamp: `${localStoragePath}/lastPushNotificationTimestamp.json`,
		pubSubLatestReading: `${localStoragePath}/pubSubLatestReading.json`,
	},

	inverterModelList: {
		huaweiSun200012ktl: 'HUAWEI_SUN2000_12KTL',
		huaweiSun200012ktl2: 'HUAWEI_SUN2000_12KTL_2',
		huaweiSun200017ktl: 'HUAWEI_SUN2000_17KTL',
		huaweiSun200020ktl: 'HUAWEI_SUN2000_20KTL',
		huaweiSun200020ktl2: 'HUAWEI_SUN2000_20KTL_2',
		huaweiSun200036ktl: 'HUAWEI_SUN2000_36KTL',
		huaweiSun200060ktl: 'HUAWEI_SUN2000_60KTL',
		huaweiSun2000100ktl: 'HUAWEI_SUN2000_100KTL',

		smaInverterManager: 'SMA_INVERTER_MANAGER',
		smaClusterController: 'SMA_CLUSTER_CONTROLLER',
		SUNGROW: 'SUNGROW',
		SUNGROW_SG33CX: 'SUNGROW_SG33CX',
		SUNGROW_SG50CX: 'SUNGROW_SG50CX',
		SUNGROW_SG110CX: 'SUNGROW_SG110CX',
	},
};
