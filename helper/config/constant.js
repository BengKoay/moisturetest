const huaweiInverterStatus = {
  0: 'Idle: Inivitla In process',
  1: 'Idle: ISO Detecting',
  2: 'Idle: Irratiation Detecting',
  3: 'Idol: Gritch',
  256: 'Starting up',
  512: 'Interconnection',
  513: 'Restricted during interconnection',
  768: 'Shutdown: Abnormality',
  769: 'Shutdown: Forced',
  1025: 'Grid feed: cos-P curve',
  1026: 'Grid feed: QU curve',
  1280: 'Check complete',
  1281: 'Checking',
  1536: 'Checking',
  2560: 'Idle: Irradiation None'
};

module.exports = {
  huaweiInverterStatus
};
