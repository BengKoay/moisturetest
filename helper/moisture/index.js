'use strict';
const fs = require('fs-extra');
const moment = require('moment-timezone');
const axios = require('axios');

const Base = require('./base');
// const piConfig = require('../../../piConfig.js').piConfig;
const logger = require('../loggerHelper').getLogger('log');
const appProperties = require('../config/properties');
let localCache = require('../localCache');
moment.tz.setDefault(appProperties.momentTimeZone);
const pushDataMoisture = [];
const pushDataQuotient = [];
const pushDataTemperature = [];

const deviceIP = 'http://192.168.0.107';
class moistureHelper extends Base {
	constructor() {
		super();
		const axiosLocal = axios.create({
			baseURL: deviceIP,
			headers: {
				'Content-Length': 0,
				'Content-Type': 'text/plain',
			},
			responseType: 'text',
		});
		this.axiosLocal = axiosLocal;
		return this;
	}

	// return this;
	// console.log('axiosLocal', axiosLocal);
	// try {
	// 	readData();
	// 	console.log('localCache.moistureReading', localCache.moistureReading);
	// 	console.log('localCache.quotientReading', localCache.quotientReading);
	// 	console.log(
	// 		'localCache.temperatureReading',
	// 		localCache.temperatureReading
	// 	);
	// } catch (e) {
	// 	logger.info('Read error: ', e);
	// }
	// const modbusHelper = new ModbusRTU();
	// if (!!piConfig.meterConfig && piConfig.meterConfig.meters.length > 0) {
	// 	const { usbConnection, baudRate, parity } = piConfig.meterConfig;
	// 	let connectionType = '/dev/ttyAMA0';
	// 	if (usbConnection === true) {
	// 		connectionType = '/dev/ttyUSB0';
	// 	}
	// 	if (!!piConfig.meterConfig.ip)
	// 		modbusHelper.connectTCP(
	// 			piConfig.meterConfig.ip,
	// 			{ port: 502 },
	// 			(error, success) => {
	// 				if (error) {
	// 					logger.info(
	// 						`Power meter with baud rate of ${baudRate} is initialization unsuccessful`
	// 					);
	// 					this.meterConnectedStatus = false;
	// 					return;
	// 				}
	// 				this.meterConnectedStatus = true;
	// 				logger.info(
	// 					`Power meter with baud rate of ${baudRate} is initialization successful`
	// 				);
	// 			}
	// 		);
	// 	else
	// 		modbusHelper.connectRTUBuffered(
	// 			connectionType,
	// 			{ baudRate, parity: parity },
	// 			(error, success) => {
	// 				if (error) {
	// 					logger.info(
	// 						`Power meter with baud rate of ${baudRate} is initialization unsuccessful`
	// 					);
	// 					this.meterConnectedStatus = false;
	// 					return;
	// 				}
	// 				this.meterConnectedStatus = true;
	// 				logger.info(
	// 					`Power meter with baud rate of ${baudRate} is initialization successful`
	// 				);
	// 			}
	// 		);
	// }
	// modbusHelper.setTimeout(500);
	// this.modbusHelper = modbusHelper;
	// return this;

	async readData() {
		try {
			// if (!!axiosLocal) console.log('readData axiosLocal', axiosLocal);
			let moistureReading = localCache.moistureReading;
			let quotientReading = localCache.quotientReading;
			let temperatureReading = localCache.temperatureReading;
			const moisture = await this.axiosLocal.post(
				'/param01.html',
				'__SL_P_UL1=FMG'
			);
			const quotient = await this.axiosLocal.post(
				'/param01.html',
				'__SL_P_UL1=QUO2'
			);
			const temperature = await this.axiosLocal.post(
				'/param01.html',
				'__SL_P_UL1=TEMP'
			);
			moistureReading.push(moisture.data);
			quotientReading.push(quotient.data);
			temperatureReading.push(temperature.data);
			localCache.moistureReading = moistureReading;
			localCache.quotientReading = quotientReading;
			localCache.temperatureReading = temperatureReading;
			console.log('moistureReading', localCache.moistureReading);
			console.log('quotientReading', localCache.quotientReading);
			console.log('temperatureReading', localCache.temperatureReading);
		} catch (e) {
			console.log(e);
		}
	}
}

// const medianOfArray = async (arr) => {
// 	console.log('array value: ', arr);
// 	const middle = (arr.length + 1) / 2;
// 	console.log();

// 	// Avoid mutating when sorting
// 	const sorted = [...arr].sort((a, b) => {
// 		return a - b;
// 	});
// 	console.log(sorted);
// 	const isEven = sorted.length % 2 === 0;

// 	return isEven
// 		? (sorted[middle - 1.5] + sorted[middle - 0.5]) / 2
// 		: sorted[middle - 1];
// };

// const sample = async () => {
// 	const { uuid } = require('uuidv4');
// 	const uuidv4ID = uuid();
// 	const cron = require('node-cron');
// 	const moment = require('moment');
// 	const deviceIP = 'http://192.168.50.119';
// 	// const deviceID = 'b176cf1e-a537-46c9-8ae7-e7b27a3de5c7';
// 	const deviceID = 'b8a84ba8-f61b-403b-b5f5-d6ba89509347';
// 	const customerName = '';

// 	let pushDataMoisture = [];
// 	let pushDataQuotient = [];
// 	let pushDataTemperature = [];
// 	let pushDataCombined = [];

// 	// Gan's Mock API Gateway

// 	const axios = require('axios');
// 	const { aws4Interceptor } = require('aws4-axios');
// 	const axiosSolar = {
// 		baseURL:
// 			'https://9ry30zin92.execute-api.ap-southeast-1.amazonaws.com/public',
// 		apiKey: 'eJzWdiiQi18jzBIWxqFHV1TRAgfcIg8h8Lc0wgBB',
// 		region: 'ap-southeast-1',
// 		service: 'execute-api',
// 		accessKeyId: 'AKIAVBSYQGXZE6POVWVK',
// 		secretAccessKey: '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',
// 	};
// 	const axiosMainHelper = axios.create({
// 		baseURL: axiosSolar.baseURL,
// 		headers: { 'x-api-key': axiosSolar.apiKey },
// 	});
// 	const interceptor = aws4Interceptor(
// 		{
// 			region: axiosSolar.region,
// 			service: axiosSolar.service,
// 		},
// 		{
// 			accessKeyId: axiosSolar.accessKeyId,
// 			secretAccessKey: axiosSolar.secretAccessKey,
// 		}
// 	);
// 	axiosMainHelper.interceptors.request.use(interceptor);

// 	const axiosLocal = axios.create({
// 		baseURL: deviceIP,
// 		headers: {
// 			'Content-Length': 0,
// 			'Content-Type': 'text/plain',
// 		},
// 		responseType: 'text',
// 	});

// 	// error checking
// 	const returnError = (err) => {
// 		return !!err.body ? JSON.stringify(err.body) : JSON.stringify(err.message);
// 	};

// 	function medianOfArray(arr) {
// 		console.log('array value: ', arr);
// 		const middle = (arr.length + 1) / 2;
// 		console.log();

// 		// Avoid mutating when sorting
// 		const sorted = [...arr].sort((a, b) => {
// 			return a - b;
// 		});
// 		console.log(sorted);
// 		const isEven = sorted.length % 2 === 0;

// 		return isEven
// 			? (sorted[middle - 1.5] + sorted[middle - 0.5]) / 2
// 			: sorted[middle - 1];
// 	}

// 	const readData = async (time) => {
// 		console.log('start');
// 		readTask = cron.schedule(`*/${time} * * * * *`, async () => {
// 			if (pushDataMoisture.length <= 11) {
// 				let moisture = { data: 0 };
// 				let quotient = { data: 0 };
// 				let temperature = { data: 0 };

// 				try {
// 					moisture = axiosLocal.post('/param01.html', '__SL_P_UL1=FMG');
// 					quotient = axiosLocal.post('/param01.html', '__SL_P_UL1=QUO2');
// 					temperature = axiosLocal.post('/param01.html', '__SL_P_UL1=TEMP');

// 					let reading1 = parseFloat(moisture.data) || 0;
// 					let reading2 = parseFloat(quotient.data) || 0;
// 					let reading3 = parseFloat(temperature.data) || 0;
// 					pushDataMoisture.push(reading1);
// 					pushDataQuotient.push(reading2);
// 					pushDataTemperature.push(reading3);

// 					console.log(
// 						'pushDataMoisture',
// 						pushDataMoisture,
// 						pushDataQuotient,
// 						pushDataTemperature
// 					);
// 					console.log('pushDataMoisture.length', pushDataMoisture.length);
// 					console.log('pushDataQuotient.length', pushDataQuotient.length);
// 					console.log('pushDataTemperature.length', pushDataTemperature.length);
// 					// console.log(pushDataQuotient);
// 					// console.log(pushDataTemperature);
// 				} catch (err) {
// 					console.log(returnError(err));
// 				}

// 				if (
// 					pushDataMoisture.length == 11 &&
// 					pushDataQuotient.length == 11 &&
// 					pushDataTemperature.length == 11
// 				) {
// 					medianOfArray(pushDataMoisture);
// 					medianOfArray(pushDataQuotient);
// 					medianOfArray(pushDataTemperature);
// 					console.log('Moisture median :', medianOfArray(pushDataMoisture));
// 					console.log('Quotient median :', medianOfArray(pushDataQuotient));
// 					console.log(
// 						'Temperature median :',
// 						medianOfArray(pushDataTemperature)
// 					);

// 					const outputPushMoistureData = await medianOfArray(pushDataMoisture);
// 					const outputPushQuotientData = await medianOfArray(pushDataQuotient);
// 					const outputPushTemperatureData = await medianOfArray(
// 						pushDataTemperature
// 					);
// 					let outputData = {
// 						moisture: outputPushMoistureData,
// 						quotient: outputPushQuotientData,
// 						temperature: outputPushTemperatureData,
// 					};
// 					console.log('outputData', outputData);
// 					pushDataCombined.push(outputData);
// 					console.log(pushDataCombined);
// 				}
// 			} else {
// 				pushDataCombined.length = 0;
// 				pushDataMoisture.length = 0;
// 				pushDataQuotient.length = 0;
// 				pushDataTemperature.length = 0;
// 			}
// 		});
// 	};

// 	const pushToCloud = async () => {
// 		pushTask = cron.schedule(`* * * * * *`, async () => {
// 			// console.log('info', info);
// 			// if (pushDataCombined.length >= 0) {
// 			//   const info = pushDataCombined;
// 			if (pushDataCombined.length == 1) {
// 				// push at the 0 sec of every minute

// 				const output = {
// 					deviceID: deviceID,
// 					timestamp: moment().unix(),
// 					sensor: {},
// 				};
// 				output.sensor = pushDataCombined;

// 				const data = { id: uuidv4ID, data: JSON.stringify(output) };
// 				console.log('data', data);
// 				try {
// 					const response = await axiosMainHelper.post('/rawtmpdata', data);
// 					console.log(response.status);
// 					console.log(response);
// 					console.log('push success');
// 				} catch (err) {
// 					console.log(returnError(err));
// 				}
// 				// }
// 				pushDataCombined.length = 0;
// 			}
// 		});
// 	};
// 	readData(1); // read data every 5 seconds

// 	pushToCloud();
// };
module.exports = moistureHelper;
