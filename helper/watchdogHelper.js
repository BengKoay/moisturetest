'use strict';
const exec = require('child_process').exec;
const logger = require('./loggerHelper').getLogger('log');
const appProperties = require('./config/properties');
const moment = require('moment-timezone');

class watchdogHelper {
  constructor() {}

  async process() {
    exec(`sudo sh -c "echo '.' >> /dev/watchdog"`);

    exec(`uptime grep | awk '{print $3}'`, (error, stdout, stderr) => {
      if (error) logger.info(error);
      if (stdout) {
        if (parseInt(stdout) == '0') {
          if (moment().minute() === 0 && moment().hour() === 0) {
            // Daily reboot ignore Every day  00:00:01 reboot Pi
          } else {
            logger.info(stdout, 'Pi restarted');
          }
        }
      }
      if (stderr) logger.info(stderr);
    });
  }
}

module.exports = watchdogHelper;
