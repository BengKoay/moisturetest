'use strict';
const exec = require('child_process').exec;
const fs = require('fs-extra');
const moment = require('moment-timezone');
const appProperties = require('../config/properties');
const localCache = require('../localCache');
const logger = require('../loggerHelper').getLogger('log');
const piConfig = require('../../piConfig').piConfig;
const { processedDataPoolPath, publishedDataPoolPath, publishingDataPoolPath } =
	appProperties.localFileStorageLocation.ensureDirectories;
const pubOpts = { qos: 1 };
const moveFileOverride = { overwrite: true };
// const clientId = `${piConfig.lotId.replace(
//   /-/g,
//   ''
// )}_${piConfig.deviceId.replace(/-/g, '')}`;
moment.tz.setDefault(appProperties.momentTimeZone);

// const readingTopic = `publish/lot_reading`;
// // const readingTopic = `publish/${clientId}_reading`;
// // const notificationTopic = `publish/${clientId}_notification`;
// const notificationTopic = `publish/lot_notification`;
// const publishIoTChannel = `publish/${appProperties.plusSolarIoTChannel}`;
// const subscribeIoTChannel = `subscribe/${appProperties.plusSolarIoTChannel}`;
// const subscribeRequestReadingTopic = `subscribe/${clientId}_request_reading`;
// const publishFiveSecondsReading = `publish/${clientId}_reading_5seconds`;
// const publishFiveSecondsReading = `publish/${clientId}_five_seconds_reading`;

class Base {
	constructor() {
		// this.publishIoTChannel = publishIoTChannel;
		// this.subscribeIoTChannel = subscribeIoTChannel;
		// this.notificationTopic = notificationTopic;
		// this.readingTopic = readingTopic;
		// this.subscribeRequestReadingTopic = subscribeRequestReadingTopic;

		this.appProperties = appProperties;
		// this.clientId = clientId;
		this.clientConnection = false;
		this.logger = logger;
		this.piConfig = piConfig;
		this.moment = moment;
		this.fs = fs;
		return this;
	}

	async isConnectionReady() {
		return this.clientConnection;
	}

	async deviceReceiveMessage(clientHelper, topic, payload) {
		const data = JSON.parse(payload);
		let validDevice = true;

		if (data.deviceId === undefined) {
			return this.logger.info(`Unknowns command -> ${payload}`);
		}

		if (!!data.deviceId && data.deviceId !== piConfig.deviceId) {
			validDevice = false;
			return this.logger.info(
				`Calling device id of'${data.deviceId}', None of my business, bye! 🙂`
			);
		}

		if (topic === this.subscribeIoTChannel) {
			let params = {
				companyId: piConfig.companyId,
				locationId: piConfig.locationId,
				lotId: piConfig.lotId,
				deviceId: piConfig.deviceId,
				createdAt: moment().toISOString(),
			};

			if (validDevice) {
				if (!!data.restart && data.restart) {
					let msg = 'Someone to force me terminate my process! 😭';
					params.msg = msg;

					await publishInfo({
						clientHelper,
						topic: this.publishIoTChannel,
						data: params,
					});
					this.logger.info(msg);
					setTimeout(() => {
						return process.exit(0);
					}, 5000);
				}

				if (!!data.shCommand && data.shCommand) {
					exec(data.shCommand, (error, stdout, stderr) => {
						if (error) {
							let msg = `error on shCommand => ${error}`;
							params.msg = msg;
							publishInfo({
								clientHelper,
								topic: this.publishIoTChannel,
								data: params,
							});
							return this.logger.error(msg);
						}
						if (stdout) {
							let msg = `stdout => ${stdout}`;
							params.msg = msg;
							publishInfo({
								clientHelper,
								topic: this.publishIoTChannel,
								data: params,
							});
							this.logger.info(msg);

							if (data.shCommand.includes('git pull')) {
								exec(
									'sudo git rev-parse HEAD',
									(gitVersionError, gitVersionStOut, gitVersionStdErr) => {
										if (gitVersionError) {
											this.logger.error('gitVersion Error: ', gitVersionError);
										}
										exec(
											'sudo git rev-parse --abbrev-ref HEAD',
											(gitBranchError, gitBranchStOut, gitBranchStdErr) => {
												if (gitBranchError) {
													this.logger.error(
														'gitBranch Error: ',
														gitBranchError
													);
												}

												const notificationInfo = {
													locationId: piConfig.locationId,
													lotId: piConfig.lotId,
													companyId: piConfig.companyId,
													deviceId: piConfig.deviceId,
													gitInfo: {
														version: gitVersionStOut.split('\n')[0],
														branch: gitBranchStOut.split('\n')[0],
													},
													code: 701,
													techChannel: true,
													createdAt: moment().toISOString(),
												};

												publishInfo({
													clientHelper,
													topic: this.notificationTopic,
													data: notificationInfo,
												});

												if (!!data.restartAfterPull && data.restartAfterPull) {
													let msg = 'Command included restart process';
													params.msg = msg;
													publishInfo({
														clientHelper,
														topic: this.publishIoTChannel,
														data: params,
													});
													this.logger.info(msg);
													setTimeout(() => {
														return process.exit(0);
													}, 5000);
												}
											}
										);
									}
								);
							}
						}
					});
				}
			}
		}

		if (topic === this.subscribeRequestReadingTopic) {
			localCache.publishFiveSecondsReadingRemainingCount =
				data.dataCounter || 24;

			await this.publishPubSubLatestReading(clientHelper);
		}
	}

	async publishPubSubLatestReading(clientHelper) {
		let counter = localCache.publishFiveSecondsReadingRemainingCount;
		if (counter > 0) {
			if (!!localCache.latestReading) {
				localCache.publishFiveSecondsReadingRemainingCount -= 1;
				return await publishInfo({
					clientHelper,
					topic: publishFiveSecondsReading,
					data: localCache.latestReading,
				});
			}
		}
	}

	async publishPayloadProcessing(clientHelper, params) {
		if (!clientHelper) {
			this.logger.info('Client Helper not Found!');
			return;
		}

		const pendingDataFileList = await fs.readdirSync(params.processedDataPath);
		for (const pendingDataFile of pendingDataFileList) {
			if (
				!fs
					.statSync(`${params.processedDataPath}/${pendingDataFile}`)
					.isDirectory()
			) {
				await fs.moveSync(
					`${params.processedDataPath}/${pendingDataFile}`,
					`${params.publishingDataPath}/${pendingDataFile}`,
					moveFileOverride
				);
				let dataToPush = null;

				try {
					dataToPush = JSON.parse(
						await fs.readFileSync(
							`${params.publishingDataPath}/${pendingDataFile}`,
							'utf8'
						)
					);
				} catch (e) {
					this.logger.info(
						'publishPayloadProcessing Read pendingDataFile Error',
						e
					);
					this.logger.info('Moving ', pendingDataFile);

					await fs.moveSync(
						`${params.publishingDataPath}/${pendingDataFile}`,
						`${params.publishingErrorDataPath}/${pendingDataFile}`,
						moveFileOverride
					);
					this.logger.info('Moved ', pendingDataFile);
				}

				if (params.topic === this.readingTopic) {
					console.log('readingTopic');
					if (!!dataToPush) {
						console.log('dataToPush', dataToPush);
						const logTitle = `Publishing:`;
						this.logger.info(
							`${logTitle.padEnd(40)} ${(pendingDataFile || '').padStart(30)}`
						);

						await publishInfo({
							clientHelper,
							data: dataToPush,
							fileName: pendingDataFile,
							...params,
						});
					}
				} else if (params.topic === this.notificationTopic) {
					if (await requirePushNotification(dataToPush.code)) {
						await publishInfo({
							clientHelper,
							data: dataToPush,
							fileName: pendingDataFile,
							...params,
						});
						await writeLocalNotificationTimeStamp(dataToPush.code);
					} else {
						await moveFile({
							data: dataToPush,
							fileName: pendingDataFile,
							...params,
						});
					}
				}
			}
		}
	}

	async removePublishedHistoricalData(path) {
		if (await fs.pathExistsSync(path)) {
			const publishedDataFileList = await fs.readdirSync(path);
			for (const publishedFile of publishedDataFileList) {
				if (fs.statSync(`${path}/${publishedFile}`).isDirectory()) {
					if (publishedFile.includes('-')) {
						const days = moment().diff(
							moment(publishedFile, 'YYYY-MM-DD'),
							'days',
							false
						);
						if (days > 90) {
							await fs.remove(`${path}/${publishedFile}`);
						}
					}
				}
			}
		}
	}

	async moveUnpublishedReadingBackFileToReadingPath() {
		if (await fs.pathExistsSync(publishingDataPoolPath)) {
			const pendingDataFileList = await fs.readdirSync(publishingDataPoolPath);
			for (const pendingDataFile of pendingDataFileList) {
				if (
					!fs
						.statSync(`${publishingDataPoolPath}/${pendingDataFile}`)
						.isDirectory()
				) {
					const fileTime = parseInt(pendingDataFile.split('.')[0], 10);
					if (
						moment
							.duration(moment().diff(moment(fileTime * 1000)))
							.asMinutes() >= 10
					) {
						await fs.moveSync(
							`${publishingDataPoolPath}/${pendingDataFile}`,
							`${processedDataPoolPath}/${pendingDataFile}`,
							moveFileOverride
						);
						const logTitle = `Re-publishing:`;
						this.logger.info(
							`${logTitle.padEnd(40)} ${pendingDataFile.padStart(30)}`
						);
					}
				}
			}
		}
	}

	async reportDeviceOnline(clientHelper) {
		const code = 288;
		try {
			const params = {
				clientHelper,
				topic: this.notificationTopic,
				data: {
					companyId: piConfig.companyId,
					locationId: piConfig.locationId,
					lotId: piConfig.lotId,
					deviceId: piConfig.deviceId,
					code,
					createdAt: moment().toISOString(),
					updatedAt: moment().toISOString(),
				},
			};

			await publishInfo(params);
			await writeLocalNotificationTimeStamp(code);
		} catch (e) {
			this.logger.info(`reportDeviceOnline Error`, e);
		}
	}
}

const requirePushNotification = async (code) => {
	const lastNotification = await getLastNotificationTimeStamp(code);
	let errorHour = 12;

	if ([302, 303].includes(code)) {
		errorHour = 4;
	}
	if ([2000].includes(code)) {
		errorHour = 6;
	}

	if (
		moment.duration(moment().diff(moment(lastNotification * 1000))).asHours() >
		errorHour
	) {
		return true;
	}

	return false;
};

const writeLocalNotificationTimeStamp = async (code) => {
	let lastPushNotification = {};
	if (
		await fs.existsSync(
			appProperties.localFileStorageLocation.lastPushNotificationTimestamp
		)
	) {
		lastPushNotification = await fs.readJSONSync(
			appProperties.localFileStorageLocation.lastPushNotificationTimestamp
		);
	}

	lastPushNotification[code] = moment().unix();
	await fs.writeJsonSync(
		appProperties.localFileStorageLocation.lastPushNotificationTimestamp,
		lastPushNotification
	);
};

const getLastNotificationTimeStamp = async (code) => {
	const lastNotification = 1546300800;
	if (
		await fs.existsSync(
			appProperties.localFileStorageLocation.lastPushNotificationTimestamp
		)
	) {
		const lastPushNotification = await fs.readJSONSync(
			appProperties.localFileStorageLocation.lastPushNotificationTimestamp
		);

		return lastPushNotification[code] || lastNotification;
	}
	return lastNotification;
};

const publishInfo = async (params) => {
	let { clientHelper, topic, data, fileName = null } = params;
	let dataPublished = false;
	console.log('publishInfo');

	switch (piConfig.publishDataMethod) {
		case appProperties.publishMethod.mqtt:
			dataPublished = await new Promise((resolve) => {
				clientHelper.publish(
					topic,
					JSON.stringify(data),
					pubOpts,
					(err, data) => {
						if (err === null) {
							return resolve(true);
						}
						return resolve(false);
					}
				);
			});
			break;
		case appProperties.publishMethod.rest:
			console.log('reading data', data);
			// console.log('clientHelper', clientHelper);
			let urlPath = '/rawtmpdata';
			// if (topic === notificationTopic) {
			// 	urlPath = 'notification';
			// } else if (topic === publishIoTChannel) {
			// 	urlPath = 'publishIoTChannel';
			// }

			try {
				const restResponse = await clientHelper.post(urlPath, data);
				console.log('restResponse', restResponse.data);
				if (restResponse.data.statusCode === '201') dataPublished = true;
				console.log('dataPublished', dataPublished);
			} catch (e) {
				console.log(e);
			}
			break;
		default:
			break;
	}

	if (!!fileName && dataPublished) {
		moveFile(params);
	}
};

const moveFile = async (params) => {
	const { topic, fileName, data, publishingDataPath, publishedPoolPath } =
		params;
	if (await fs.existsSync(`${publishingDataPath}/${fileName}`)) {
		let errorCode = ``;
		// if (topic === readingTopic) {
		const logTitle = `Published:`;
		logger.info(`${logTitle.padEnd(40)} ${(fileName || '').padStart(30)}`);
		// } else {
		// 	errorCode = `-${data.code}`;
		// }

		const datePath = `${moment(
			parseInt(fileName.split('.')[0], 10) * 1000
		).format('YYYY-MM-DD')}${errorCode}`;
		await fs.ensureDirSync(`${publishedPoolPath}/${datePath}`);

		await fs.moveSync(
			`${publishingDataPath}/${fileName}`,
			`${publishedPoolPath}/${datePath}/${fileName}`,
			moveFileOverride
		);
	}
};

module.exports = Base;
