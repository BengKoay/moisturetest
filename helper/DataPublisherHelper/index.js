const AWS_IOT_HELPER = require('./awsIoTHelper');
const AWS_API_GATEWAY_HELPER = require('./awsAPIGatewayHelper');

module.exports = {
  AWS_IOT_HELPER,
  AWS_API_GATEWAY_HELPER
};
