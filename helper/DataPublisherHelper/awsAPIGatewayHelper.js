'use strict';
const axios = require('axios');
const { aws4Interceptor } = require('aws4-axios');
const Base = require('./base');

class AWSAPIGateway extends Base {
	constructor() {
		super();

		const device = axios.create({
			baseURL: this.appProperties.awsApiGateway,
			timeout: 30000,
			headers: {
				// 'content-type': 'application/json',
				'x-api-key': 'eJzWdiiQi18jzBIWxqFHV1TRAgfcIg8h8Lc0wgBB',
			},
		});

		//  Gan API method start

		const interceptor = aws4Interceptor(
			{
				region: 'ap-southeast-1',
				service: 'execute-api',
			},
			{
				accessKeyId: 'AKIAVBSYQGXZE6POVWVK',
				secretAccessKey: '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',
			}
		);
		device.interceptors.request.use(interceptor);
		//  Gan API method end

		this.device = device;
		super.clientConnection = true;
		return this;
	}

	async processReportDeviceOnline() {
		this.reportDeviceOnline(this.device);
	}

	async process() {
		// Reading
		try {
			const params = {
				topic: this.readingTopic,
				processedDataPath:
					this.appProperties.localFileStorageLocation.ensureDirectories
						.processedDataPoolPath,
				publishingDataPath:
					this.appProperties.localFileStorageLocation.ensureDirectories
						.publishingDataPoolPath,
				publishedPoolPath:
					this.appProperties.localFileStorageLocation.ensureDirectories
						.publishedDataPoolPath,
			};
			await this.publishPayloadProcessing(this.device, params);
		} catch (e) {
			this.logger.error('AWS AWSAPIGateway Reading Process Error', e);
		}

		// Notification
		try {
			const params = {
				topic: this.notificationTopic,
				processedDataPath:
					this.appProperties.localFileStorageLocation.ensureDirectories
						.processedNotificationPoolPath,
				publishingDataPath:
					this.appProperties.localFileStorageLocation.ensureDirectories
						.publishingNotificationPoolPath,
				publishedPoolPath:
					this.appProperties.localFileStorageLocation.ensureDirectories
						.publishedNotificationPoolPath,
			};
			await this.publishPayloadProcessing(this.device, params);
		} catch (e) {
			this.logger.error('AWS AWSAPIGateway Notification Process Error', e);
		}
	}

	async getRemoteCommand() {
		let payload = {
			info: {
				companyId: this.piConfig.companyId,
				locationId: this.piConfig.locationId,
				lotId: this.piConfig.lotId,
				deviceId: this.piConfig.deviceId,
			},
		};

		const response = await this.device.post('remoteCommand', payload);
		if (
			!!response.data.d &&
			!!response.data.d.topic &&
			!!response.data.d.command
		) {
			this.deviceReceiveMessage(
				this.device,
				response.data.d.topic,
				response.data.d.command
			);
		}
	}
}

module.exports = AWSAPIGateway;
