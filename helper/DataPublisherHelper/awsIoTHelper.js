'use strict';
const awsIot = require('aws-iot-device-sdk');
let localCache = require('../localCache');
const Base = require('./base');

class AWSIoTHelper extends Base {
  constructor() {
    super();
    this.logger.info('AWSIoTHelper initialize');

    const device = awsIot.device({
      clientId: this.clientId,
      host: this.appProperties.awsIoTHost,
      offlineQueueing: false,
      clientCert: Buffer.from(this.piConfig.awsIoTCredentials.clientCert),
      privateKey: Buffer.from(this.piConfig.awsIoTCredentials.privateKey),
      caCert: Buffer.from(
        `-----BEGIN CERTIFICATE-----\nMIIDQTCCAimgAwIBAgITBmyfz5m/jAo54vB4ikPmljZbyjANBgkqhkiG9w0BAQsF\nADA5MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRkwFwYDVQQDExBBbWF6\nb24gUm9vdCBDQSAxMB4XDTE1MDUyNjAwMDAwMFoXDTM4MDExNzAwMDAwMFowOTEL\nMAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEZMBcGA1UEAxMQQW1hem9uIFJv\nb3QgQ0EgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJ4gHHKeNXj\nca9HgFB0fW7Y14h29Jlo91ghYPl0hAEvrAIthtOgQ3pOsqTQNroBvo3bSMgHFzZM\n9O6II8c+6zf1tRn4SWiw3te5djgdYZ6k/oI2peVKVuRF4fn9tBb6dNqcmzU5L/qw\nIFAGbHrQgLKm+a/sRxmPUDgH3KKHOVj4utWp+UhnMJbulHheb4mjUcAwhmahRWa6\nVOujw5H5SNz/0egwLX0tdHA114gk957EWW67c4cX8jJGKLhD+rcdqsq08p8kDi1L\n93FcXmn/6pUCyziKrlA4b9v7LWIbxcceVOF34GfID5yHI9Y/QCB/IIDEgEw+OyQm\njgSubJrIqg0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMC\nAYYwHQYDVR0OBBYEFIQYzIU07LwMlJQuCFmcx7IQTgoIMA0GCSqGSIb3DQEBCwUA\nA4IBAQCY8jdaQZChGsV2USggNiMOruYou6r4lK5IpDB/G/wkjUu0yKGX9rbxenDI\nU5PMCCjjmCXPI6T53iHTfIUJrU6adTrCC2qJeHZERxhlbI1Bjjt/msv0tadQ1wUs\nN+gDS63pYaACbvXy8MWy7Vu33PqUXHeeE6V/Uq2V8viTO96LXFvKWlJbYK8U90vv\no/ufQJVtMVT8QtPHRh8jrdkPSHCa2XV4cdFyQzR1bldZwgJcJmApzyMZFo6IQ6XU\n5MsI+yMRQ+hDKXJioaldXgjUkK642M4UwtBV8ob2xJNDd2ZhwLnoQdeXeGADbkpy\nrqXRfboQnoZsG4q5WTP468SQvvG5\n-----END CERTIFICATE-----`
      )
    });
    device.on('connect', () => {
      device.subscribe(this.subscribeIoTChannel);
      device.subscribe(this.subscribeRequestReadingTopic);
      super.clientConnection = true;
      this.logger.info(
        `AWSIoTHelper connected and subscribe to ${this.subscribeIoTChannel}`
      );
    });

    device.on('message', (topic, payload) => {
      this.deviceReceiveMessage(this.device, topic, payload.toString());
    });

    device.on('close', () => {
      super.clientConnection = false;
      this.logger.info('AWSIoTHelper disconnected');
    });

    device.on('reconnect', () => {
      this.logger.info('AWSIoTHelper reconnect...');
    });

    device.on('timeout', () => {
      this.logger.info('AWSIoTHelper timeout', arguments);
    });

    device.on('error', (error) => {
      this.logger.info('AWSIoTHelper error', error);
    });

    this.device = device;
    return this;
  }

  async processReportDeviceOnline() {
    this.reportDeviceOnline(this.device);
  }

  async process() {
    // Reading
    try {
      const params = {
        topic: this.readingTopic,
        processedDataPath: this.appProperties.localFileStorageLocation
          .ensureDirectories.processedDataPoolPath,
        publishingDataPath: this.appProperties.localFileStorageLocation
          .ensureDirectories.publishingDataPoolPath,
        publishingErrorDataPath: this.appProperties.localFileStorageLocation
          .ensureDirectories.publishingErrorDataPoolPath,
        publishedPoolPath: this.appProperties.localFileStorageLocation
          .ensureDirectories.publishedDataPoolPath
      };
      await this.publishPayloadProcessing(this.device, params);
    } catch (e) {
      this.logger.error('AWS IoTHelper Reading Process Error', e);
    }

    // Notification
    try {
      const params = {
        topic: this.notificationTopic,
        processedDataPath: this.appProperties.localFileStorageLocation
          .ensureDirectories.processedNotificationPoolPath,
        publishingDataPath: this.appProperties.localFileStorageLocation
          .ensureDirectories.publishingNotificationPoolPath,
        publishedPoolPath: this.appProperties.localFileStorageLocation
          .ensureDirectories.publishedNotificationPoolPath
      };
      await this.publishPayloadProcessing(this.device, params);
    } catch (e) {
      this.logger.error('AWS IoTHelper Notification Process Error', e);
    }
  }

  async processPubSubLatestReading() {
    await this.publishPubSubLatestReading(this.device);
    // try {
    // if (!!localCache.latestReading) {
    //   const reading = localCache.latestReading;
    //   await this.publishPubSubLatestReading(this.device, reading);
    // }
    // } catch (e) {
    //   this.logger.error(
    //     'AWS IoTHelper Reading processPubSubLatestReading Error',
    //     e
    //   );
    // }
  }
}

module.exports = AWSIoTHelper;
