let energyMeterReading = null;
let moistureReading = [];
let quotientReading = [];
let temperatureReading = [];
let smartLoggerReading = null;
let weatherSensorReading = null;
let inverterReading = null;
let controlSolarOutputData = null;
let latestReading = null;
let publishFiveSecondsReadingRemainingCount = 0;
let smartLoggerConnectionFailureCount = 0;
let DPMConnectionFailureCount = 0;
let AWSConnectionFailureCount = 0;
let errorInverter = null;
let errorSmartlogger = null;
let errorDPM = null;

module.exports = {
	energyMeterReading,
	smartLoggerReading,
	weatherSensorReading,
	inverterReading,
	controlSolarOutputData,
	latestReading,
	publishFiveSecondsReadingRemainingCount,
	smartLoggerConnectionFailureCount,
	DPMConnectionFailureCount,
	AWSConnectionFailureCount,
	errorInverter,
	errorSmartlogger,
	errorDPM,
	moistureReading,
	quotientReading,
	temperatureReading,
};
