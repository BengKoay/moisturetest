const fs = require('fs-extra');
const AWS = require('aws-sdk');
const properties = require('../config/properties');
const piConfig = require('../../piConfig.js').piConfig;
const logger = require('../loggerHelper').getLogger('log');

const logPath = process.cwd() + '/logs';

const awsConfig = {
  region: properties.awsCredentials.region,
  credentials: {
    accessKeyId: properties.awsCredentials.accessKeyId,
    secretAccessKey: properties.awsCredentials.secretAccessKey
  }
};

AWS.config.update(awsConfig);
const s3 = new AWS.S3();

const _main = async (logList) => {
  for (const log of logList) {
    if (await fs.existsSync(`${logPath}/${log}`)) {
      const fileData = await fs.readFileSync(`${logPath}/${log}`);
      const params = {
        Bucket: 'experiment-iot-core',
        Key: `pi-log/${piConfig.lotId}/${log}`,
        Body: fileData
      };
      new Promise(async (resolve, reject) => {
        s3.upload(params, (err, data) => {
          if (err) {
            logger.error(err);
            return resolve(false);
          }
          logger.info(data.Location);
          return resolve(true);
        });
      });
    } else {
      logger.error(`${logPath}/${log} is not found!`);
    }
  }
};

exports.uploadToS3 = _main;
