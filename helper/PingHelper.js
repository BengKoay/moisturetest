'use strict';
const exec = require('child_process').exec;
const awsLogger = require('./loggerHelper').getLogger('pingAWS');
// const smartLogger = require('./loggerHelper').getLogger('pingSmartLogger');
// const moscoLogger = require('./loggerHelper').getLogger('pingMoscoServer');
// const piConfig = require('../piConfig.js').piConfig;
const appProperties = require('./config/properties');

class PingHelper {
	constructor() {}

	async process() {
		// if (!!piConfig.moscoClientConfig && !!piConfig.moscoClientConfig.server) {
		//   exec(
		//     `ping -c 5 ${piConfig.moscoClientConfig.server}`,
		//     (error, stdout, stderr) => {
		//       if (error) {
		//         moscoLogger.error(error);
		//       } else {
		//         moscoLogger.info(stdout);
		//       }
		//     }
		//   );
		// }
		// if (!!piConfig.smartloggerConfig && piConfig.smartloggerConfig.length > 0) {
		//   piConfig.smartloggerConfig.map((obj) => {
		//     if (!!obj.ip) {
		//       exec(`ping -c 5 ${obj.ip}`, (error, stdout, stderr) => {
		//         if (error) {
		//           smartLogger.error(error);
		//         } else {
		//           smartLogger.info(stdout);
		//         }
		//       });
		//     }
		//   });
		// }

		exec(`ping -c 5 www.google.com`, (error, stdout, stderr) => {
			if (error) {
				awsLogger.error(error);
			} else {
				awsLogger.info(stdout);
			}
		});
	}
}

module.exports = PingHelper;
