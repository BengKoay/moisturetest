const mqtt = require('mqtt');
const piConfig = require('../piConfig.js').piConfig;
const appProperties = require('./config/properties');

class MQTTClientHelper {
  constructor() {
    const mqttClient = mqtt.connect({
      host: piConfig.moscoClientConfig.server,
      port: piConfig.moscoClientConfig.port,
      username: piConfig.moscoClientConfig.username,
      password: piConfig.moscoClientConfig.password
    });
    this.mqttClient = mqttClient;
    return this;
  }

  async sleep(ms) {
    new Promise((resolve) => setTimeout(resolve, ms));
  }

  async getConnectionStatus() {
    return this.mqttClient.connected;
  }

  async publishData(channel, data) {
    return new Promise((resolve, reject) => {
      this.mqttClient.publish(
        `${channel}/${piConfig.moscoClientConfig.username}`,
        JSON.stringify(data),
        {
          qos: appProperties.mosca.qos
        },
        (err, data) => {
          if (err) {
            return resolve({ c: -1, m: err, d: false });
          } else {
            return resolve({ c: 0, m: '', d: true });
          }
        }
      );
    });
  }
}

module.exports = MQTTClientHelper;
