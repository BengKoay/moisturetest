'use strict';
const log4js = require('log4js');
log4js.configure({
  appenders: {
    appDateFile: {
      type: 'dateFile',
      filename: './logs/log.log',
      pattern: '_yyyy-MM-dd',
      keepFileExt: true,
      daysToKeep: 90
    },
    console: {
      type: 'console'
    },
    pingMoscoServer: {
      type: 'dateFile',
      filename: './logs/pinglog/pingMoscoServer.log',
      pattern: '_yyyy-MM-dd',
      keepFileExt: true,
      daysToKeep: 30
    },
    pingSmartloggerDateFile: {
      type: 'dateFile',
      filename: './logs/pinglog/pingSmartlogger.log',
      pattern: '_yyyy-MM-dd',
      keepFileExt: true,
      daysToKeep: 30
    },
    pingAWSDateFile: {
      type: 'dateFile',
      filename: './logs/pinglog/pingAWS.log',
      pattern: '_yyyy-MM-dd',
      keepFileExt: true,
      daysToKeep: 30
    }
  },
  categories: {
    default: {
      // appenders: ['appDateFile', 'console'],
      appenders: ['appDateFile'],
      level: 'DEBUG'
    },
    pingSmartLogger: { appenders: ['pingSmartloggerDateFile'], level: 'DEBUG' },
    pingAWS: { appenders: ['pingAWSDateFile'], level: 'DEBUG' },
    pingMoscoServer: { appenders: ['pingMoscoServer'], level: 'DEBUG' }
  }
});

module.exports = log4js;
