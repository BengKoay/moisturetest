'use strict';
const fs = require('fs-extra');
const { uuid } = require('uuidv4');
const moment = require('moment-timezone');
const appProperties = require('../helper/config/properties');
let localCache = require('../helper/localCache');
// const piConfig = require('../piConfig').piConfig;
const logger = require('../helper/loggerHelper').getLogger('log');
const deviceID = 'b176cf1e-a537-46c9-8ae7-e7b27a3de5c7';

moment.tz.setDefault(appProperties.momentTimeZone);

class DataCombineHelper {
	constructor() {}

	async process() {
		let dataOnFiftyEightSeconds = moment().second() === 58;
		let moistureMedian = medianOfArray(localCache.moistureReading);
		let quotientMedian = medianOfArray(localCache.quotientReading);
		let temperatureMedian = medianOfArray(localCache.temperatureReading);

		// console.log('localCache.moistureReading', localCache.moistureReading);
		console.log('moistureMedian', moistureMedian);
		// console.log('localCache.quotientReading', localCache.quotientReading);
		// console.log('localCache.temperatureReading', localCache.temperatureReading);
		let output = {
			deviceID: deviceID,
			timestamp: moment().unix(),
			moisture: moistureMedian,
			quotient: quotientMedian,
			temperature: temperatureMedian,
		};

		if (dataOnFiftyEightSeconds) {
			logger.info(
				`${`output: `.padEnd(30)} 'moisture: ' ${output.moisture}'quotient: ' ${
					output.quotient
				}'temperature: ' ${output.temperature}`
			);
		}

		// const data = {
		// 	info: {
		// 		companyId: piConfig.companyId,
		// 		locationId: piConfig.locationId,
		// 		lotId: piConfig.lotId,
		// 		deviceId: piConfig.deviceId,
		// 		totalBuildingLoad,
		// 		createdAt: moment().toISOString(),
		// 		timeElapsed,
		// 	},
		// 	logger: smartLoggerData,
		// 	inverter: inverterData,
		// 	powerMeter: powerMeterData,
		// 	lastUpdate: moment().unix(),
		// };
		const dataToPush = { id: uuid(), data: JSON.stringify(output) };

		if (dataOnFiftyEightSeconds) {
			await fs.writeJsonSync(
				`${
					appProperties.localFileStorageLocation.ensureDirectories
						.processedDataPoolPath
				}/${moment().unix()}.json`,
				dataToPush
			);
			localCache.moistureReading.length = 0;
			localCache.quotientReading.length = 0;
			localCache.temperatureReading.length = 0;
		}
	}
}

const medianOfArray = (arr) => {
	// console.log('array value: ', arr);
	const middle = (arr.length + 1) / 2;
	// console.log();

	// Avoid mutating when sorting
	const sorted = [...arr].sort((a, b) => {
		return a - b;
	});
	// console.log(sorted);
	const isEven = sorted.length % 2 === 0;

	return isEven
		? (sorted[middle - 1.5] + sorted[middle - 0.5]) / 2
		: sorted[middle - 1];
};
module.exports = DataCombineHelper;
